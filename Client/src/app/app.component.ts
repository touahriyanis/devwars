import { Component } from '@angular/core';
import { User } from './_models/user.model';
import { AccountService } from './_services/account.service';

import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { SocketService } from './_services/socket.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'dev-wars';
  users: any;
  private stompClient:any = null;
  isConnected = false;

  constructor(private accountService: AccountService,
              public socketService: SocketService,
              ) {}

  async ngOnInit() {
    this.setCurrentUser();
  }

  setCurrentUser() {
    const user: User = JSON.parse(localStorage.getItem('user'));
    this.accountService.setCurrentUser(user);
  }

  async subscribe() {
    await this.socketService.stompClient.subscribe('/start/1', function (hello) {
      console.log("madafakaaa");
      console.log(hello);
      this.title="psst";
    });
  }

  sendMessage() {
    this.socketService.stompClient.send(
      '/current/fleet/1',
      {},
      JSON.stringify({"sup":"hey"})
    );
  }
}
