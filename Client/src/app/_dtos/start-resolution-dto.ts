export class StartResolutionDto {
  gameId: number;
  algorithmId: number;
  itemId: number;
  actionId: number;
}
