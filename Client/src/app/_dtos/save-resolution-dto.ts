import {LinterError} from '../_models/linter-error';

export class SaveResolutionDto {
  algorithmId: number;
  solved: boolean;
  linterErrors: LinterError[];
}
