import { User } from "./user.model";

export interface UsersByGameResponse {
    user: User;
    remainingHealthPoints: number;
    won: boolean;
}