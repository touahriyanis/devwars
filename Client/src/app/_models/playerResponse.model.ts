import { Game } from "./game.model";
import { User } from "./user.model";

export interface PlayerResponse {
    game: Game;
    user: User;
    remainingHealthPoints: number;
    won: boolean;
}