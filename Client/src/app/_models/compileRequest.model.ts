export interface CompileRequest {
    sourceCode: String ;
    language: Language ;
    algorithmId: number;
}

export enum Language {
    Java = "Java",
    Python = "Python",
    Cpp = "Cpp"
}