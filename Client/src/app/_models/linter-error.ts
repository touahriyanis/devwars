export class LinterError {
  errorNumber: number;
  errorMessage: string;
}
