export interface PlayerRequest {
    remainingHealthPoints: number;
    won: boolean;
}