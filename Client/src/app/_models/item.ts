export class Item {
  id: number;
  name: string;
  power: number;
  damageAmount: number;
  healAmount: number;
}
