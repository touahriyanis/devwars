import {LinterError} from './linter-error';

export interface RunResult {
  successfullyCompiled: boolean;
  cases: Case[];
  errorOutput: string;
  successful: boolean;
  linterErrors: LinterError[];
}

export interface Case {
  status: string;
  name: string;
  log: string;
  expectedOutput: string;
  successful: boolean;
}
