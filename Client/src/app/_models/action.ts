export class Action {
  id: number;
  name: string;
  power: number;
  itemId: number;
}
