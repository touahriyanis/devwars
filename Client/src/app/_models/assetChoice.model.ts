import { Action } from "./action";
import { Item } from "./item";

export interface AssetChoice {
  asset: Item | Action
  isItem: boolean
}