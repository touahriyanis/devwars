export interface ResolutionByGame {
  userId: number
  algoId: number
  itemId: number
  actionId: any
  resolutionTime: number
  solved: boolean
  linterErrors: number
}
