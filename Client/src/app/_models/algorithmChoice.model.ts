import { Action } from "./action";
import { Item } from "./item";

export interface AlgorithmChoice {
  algoId: number;
  actionId: number;
}