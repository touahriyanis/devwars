export interface AlgorithmData {
    id: number
    wording: string
    funcName: string
    description: string
    javaInitialCode: string
    pythonInitialCode: string
    cppInitialCode: string
    timeToSolve: number
    timeLimit: number
    complexity: number
    memoryLimit: number
  }
  