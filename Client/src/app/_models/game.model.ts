export interface Game {
    id: number;
    date: Date;
    over: boolean;
    nbOfPlayers: number;
}
