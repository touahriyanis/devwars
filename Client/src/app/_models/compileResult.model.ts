export interface CompileResult {
    output: String;
    status: Status;
    successfullyCompiled: boolean;
    date: Date;
}

enum Status {
    CompilationError,
    RuntimeError,
    OutOfMemory,
    TimeLimitExcess,
    Success
}