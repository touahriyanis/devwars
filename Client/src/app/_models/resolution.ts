import {User} from './user.model';
import {AlgorithmData} from './algorithm-data.model';
import {Game} from './game.model';
import {Item} from './item';
import {Action} from './action';
import {LinterError} from './linter-error';
import { PlayerResponse } from './playerResponse.model';

export class Resolution {
  user: User;
  algorithm: AlgorithmData;
  game: Game;
  item: Item;
  action: Action;
  resolutionTime: number;
  solved: boolean;
  linterErrors: LinterError[];
  startedTime: Date;
  player: PlayerResponse
}
