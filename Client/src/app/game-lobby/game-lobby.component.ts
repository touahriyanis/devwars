import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {PageChangedEvent} from 'ngx-bootstrap/pagination';
import {map} from 'rxjs/operators';
import {Game} from '../_models/game.model';
import {PlayerRequest} from '../_models/playerRequest.module';
import {UsersByGameResponse} from '../_models/usersByGameResponse.model';
import {AccountService} from '../_services/account.service';
import {GameService} from '../_services/game.service';
import {SocketService} from '../_services/socket.service';

@Component({
  selector: 'app-game-lobby',
  templateUrl: './game-lobby.component.html',
  styleUrls: ['./game-lobby.component.css']
})
export class GameLobbyComponent implements OnInit {

  games: Game[];
  displayedGames: Game[];
  nbPlayers: number[] = [];

  currentUserId: number;

  paginationStart = 0;
  paginationEnd = 5;

  constructor(private router: Router,
              private gameService: GameService,
              private accountService: AccountService,
              public socketService: SocketService) {
  }

  ngOnInit() {
    this.getGamesList();
    this.accountService.currentUser$.subscribe(currentUser => {
      if (currentUser != null) {
        this.currentUserId = currentUser.id;
      }
    });
  }

  joinGame(game: Game) {
    this.gameService.findAllUsersByGame(game.id).subscribe(usersInGame => {
      switch (usersInGame.length) {
        case 0:
          this.addPlayerToGame(game.id);
          this.subscribeToGameLaunch(game.id);
          this.getGamesList();
          break;
        case 1:
          if (!usersInGame.some(a => a.user.id == this.currentUserId)) {
            this.addPlayerToGame(game.id);
            this.sendGameLaunchNotification(game.id);
            this.router.navigate([`/game/${game.id}/assets`]);
          } else {
            console.log('You already joined this game');
          }
          break;
        default:
          console.log('Couldn\'t join game, 2 players are already playing');
          this.getGamesList();
      }
    });

  }

  private addPlayerToGame(gameId: number) {
    let playerRequest: PlayerRequest = {
      remainingHealthPoints: 100,
      won: false
    };
    this.gameService.addOrUpdateUserToGame(gameId, this.currentUserId, playerRequest).subscribe(playerResponse => {
    }, error => {
      console.log(error.error);
    });
  }

  private getGamesList() {
    this.gameService.findAllUnfinishedGames().subscribe(retrievedGames => {
      retrievedGames.map(game => {
        this.gameService.findAllUsersByGame(game.id).subscribe(usersInGame => {
          game.nbOfPlayers = usersInGame.length;
        });
      });
      this.games = retrievedGames;
      this.displayedGames = this.games.slice(this.paginationStart, this.paginationEnd);
    });
  }

  async subscribeToGameLaunch(gameId: number) {
    var _this = this;
    await this.socketService.stompClient.subscribe('/start/games/'+gameId, function (response) {
      _this.router.navigate([`/game/${gameId}/assets`]);
    });
  }

  sendGameLaunchNotification(gameId: number) {
    this.socketService.stompClient.send(
      '/current/games/' + gameId,
      {},
      JSON.stringify({'sup': 'hey'})
    );
  }

  pageChanged(event: PageChangedEvent): void {
    this.paginationStart = (event.page - 1) * event.itemsPerPage;
    this.paginationEnd = event.page * event.itemsPerPage;
    this.displayedGames = this.games.slice(this.paginationStart, this.paginationEnd);
  }
}
