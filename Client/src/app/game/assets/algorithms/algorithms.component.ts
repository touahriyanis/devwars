import {Component, Inject, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {AlgorithmData} from 'src/app/_models/algorithm-data.model';
import {RunResult} from 'src/app/_models/runResult.model';
import {AlgorithmService} from 'src/app/_services/algorithm.service';
import {AccountService} from '../../../_services/account.service';
import {Item} from '../../../_models/item';
import {Action} from '../../../_models/action';
import {ResolutionService} from '../../../_services/resolution.service';
import {Resolution} from '../../../_models/resolution';
import {StartResolutionDto} from '../../../_dtos/start-resolution-dto';
import { AssetChoice } from 'src/app/_models/assetChoice.model';
import { AssetChoiceComponent } from '../../asset-choice/asset-choice.component';
import { AlgorithmChoice } from 'src/app/_models/algorithmChoice.model';

@Component({
  selector: 'app-algorithms',
  templateUrl: './algorithms.component.html',
  styleUrls: ['./algorithms.component.css']
})
export class AlgorithmsComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: AssetChoice,
              public dialogRef: MatDialogRef<AlgorithmsComponent>,
              private algorithmService: AlgorithmService,
              private accountService: AccountService,
              private dialog: MatDialog,
              private resolutionService: ResolutionService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  algorithms: AlgorithmData[] = [];

  async ngOnInit() {
    await this.getAlgorithms();
  }

  async getAlgorithms() {
    await this.algorithmService.getAlgorithmsByPower(this.data.asset.power, 1)
      .subscribe(
        algos => {
          this.algorithms = algos;

        },
        error => {
          console.log(error);
        });
  }

  selectAlgorithm(id: any): void {
    if(this.data.isItem) {
      console.log('EHOH');
      const routeChildren = this.route.snapshot.children;
      const gameId = +routeChildren[0].params.idGame;
      // envoyer la requête ici :D
      // TODO Proposer les items de l'adversaire ou les siens quand on choisit une action
      const resolution = new StartResolutionDto();
      resolution.algorithmId = id;
      resolution.gameId = gameId;
  
      resolution.itemId = this.data.asset.id;
      this.resolutionService.startResolutionWithItem(resolution)
        .subscribe(success => {
            this.router.navigateByUrl(`/game/${gameId}/algorithm/${id}`);
            this.dialogRef.close();
          }
        );

    } else {
      const choice: AlgorithmChoice = {
        algoId: id,
        actionId: this.data.asset.id
      }
      this.dialogRef.close();
      this.dialog.open(AssetChoiceComponent, {
        data: choice,
        width: '800px',
        backdropClass: 'bdrop'
      });
    }
  }

}
