import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssetsComponent } from './assets.component';
import { AlgorithmsComponent } from './algorithms/algorithms.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { AssetsRoutingModule } from './assets-routing.module';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    AssetsComponent,
    AlgorithmsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    AssetsRoutingModule
  ]
})
export class AssetsModule { }
