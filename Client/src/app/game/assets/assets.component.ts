import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {AlgorithmsComponent} from './algorithms/algorithms.component';
import {ItemsService} from '../../_services/items.service';
import {Item} from '../../_models/item';
import {AccountService} from '../../_services/account.service';
import {Action} from '../../_models/action';
import {ActionsService} from '../../_services/actions.service';
import { AssetChoiceComponent } from '../asset-choice/asset-choice.component';
import { AssetChoice } from 'src/app/_models/assetChoice.model';

@Component({
  selector: 'app-assets',
  templateUrl: './assets.component.html',
  styleUrls: ['./assets.component.css']
})
export class AssetsComponent implements OnInit {

  items: Item[] = [];
  actions: Action[] = [];
  currentUserId: number;


  constructor(private dialog: MatDialog,
              private accountService: AccountService,
              private itemService: ItemsService,
              private actionService: ActionsService) {
  }

  ngOnInit(): void {
    this.accountService.currentUser$.subscribe(currentUser => {
      if (currentUser != null) {
        this.currentUserId = currentUser.id;
        this.loadItems();
        this.loadActions();
      }
    });
  }

  private loadItems(): void {
    this.itemService
      .getUserItems(this.currentUserId)
      .subscribe((items) => this.items = items);
  }

  private loadActions(): void {
    this.actionService
      .getUserActions(this.currentUserId)
      .subscribe((actions) => {
        this.actions = actions;
      });
  }

  public formatActionName(actionName: string): string {

    switch (actionName) {
      case 'DESTRUCTION':
        return 'Destruction';
      case 'RESURRECTION':
        return 'Resurrection';
      default:
        return 'Error';
    }
  }

  openChooseItemDialog(action: Action): void {
    if (action.name.toLowerCase() === 'destruction') {
      //open opponent items
    } else {
      //open my items
    }
  }


  openAlgorithmsDialog(tool: Item | Action, isItem: boolean): void {
    const choice: AssetChoice = {
      asset: tool,
      isItem: isItem
    }
    this.dialog.open(AlgorithmsComponent, {
      data: choice,
      width: '800px',
      backdropClass: 'bdrop'
    });
  }

  openAssetsChoiceDialog(assetId: number) {
    this.dialog.open(AssetChoiceComponent, {
      data: assetId,
      width: '800px',
      backdropClass: 'bdrop'
    });
  }

}
