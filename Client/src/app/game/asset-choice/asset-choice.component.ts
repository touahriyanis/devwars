import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { StartResolutionDto } from 'src/app/_dtos/start-resolution-dto';
import { AlgorithmChoice } from 'src/app/_models/algorithmChoice.model';
import { Item } from 'src/app/_models/item';
import { RunResult } from 'src/app/_models/runResult.model';
import { AccountService } from 'src/app/_services/account.service';
import { ItemsService } from 'src/app/_services/items.service';
import { ResolutionService } from 'src/app/_services/resolution.service';

@Component({
  selector: 'app-asset-choice',
  templateUrl: './asset-choice.component.html',
  styleUrls: [
    './asset-choice.component.css'
  ]
})
export class AssetChoiceComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: AlgorithmChoice,
              private resolutionService: ResolutionService,
              private route: ActivatedRoute,
              private router: Router,
              private accountService: AccountService,
              public dialogRef: MatDialogRef<AssetChoiceComponent>,
              private itemsService: ItemsService) {}

  items: Item[]
  currentUserId: number;
            
  ngOnInit(): void {
    switch(this.data.actionId) {
      case 1:
        console.log("destruction");
        this.getDestructionAssets();
        break;
      case 2:
        console.log("ressurection");
        this.getRessurectionAssets();
        break;
    }
    this.accountService.currentUser$.subscribe(currentUser => {
      if (currentUser != null) {
        this.currentUserId = currentUser.id;
      }
    });
  }

  getDestructionAssets() {
    this.itemsService.getUserItems(+localStorage.getItem('opponentId'))
    .subscribe(items => {
        this.items = items;
    }, error => console.log(error));
  }

  getRessurectionAssets() {
    this.itemsService.getUsedItemsByUser(this.currentUserId)
    .subscribe(items => {
        this.items = items;
    }, error => console.log(error));
  }

  selectItem(id:number) {
    const resolution = new StartResolutionDto();
    const routeChildren = this.route.snapshot.children;
    const gameId = +routeChildren[0].params.idGame;
    console.log(gameId);

    resolution.algorithmId = this.data.algoId;
    resolution.gameId = gameId;

    resolution.itemId = id;
    resolution.actionId = this.data.actionId;
    this.resolutionService.startResolutionWithAction(resolution)
    .subscribe(success => {
      this.router.navigateByUrl(`/game/${gameId}/algorithm/${this.data.algoId}`);
      this.dialogRef.close();
    }
  );
  }

}
