import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameInterafceComponent } from './game-interafce.component';
import { GameRoutingModule } from './game-routing.module';
import { SharedModule } from '../shared/shared.module';
import { CodeEditorComponent } from './code-editor/code-editor.component';
import { ExecutionResultComponent } from './execution-result/execution-result.component';
import { AssetsComponent } from './assets/assets.component';
import { AlgorithmsComponent } from './assets/algorithms/algorithms.component';
import { AssetsModule } from './assets/assets.module';
import { AlgorithmInterfaceModule } from './algorithm-interface/algorithm-interface.module';
import { AssetChoiceComponent } from './asset-choice/asset-choice.component';



@NgModule({
  declarations: [GameInterafceComponent, AssetChoiceComponent],
  imports: [
    CommonModule,
    SharedModule,
    GameRoutingModule,
    AssetsModule,
    AlgorithmInterfaceModule
  ],
  exports: [
    GameInterafceComponent
  ]
})
export class GameModule { }
