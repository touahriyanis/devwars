import {AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';

import * as ace from 'ace-builds';
import {CompileRequest, Language} from 'src/app/_models/compileRequest.model';
import {CompileResult} from 'src/app/_models/compileResult.model';
import {RunResult} from 'src/app/_models/runResult.model';
import {AccountService} from 'src/app/_services/account.service';
import {CompilerService} from 'src/app/_services/compiler.service';
import {SocketService} from 'src/app/_services/socket.service';
import {ExecutionResultComponent} from '../execution-result/execution-result.component';
import {SaveResolutionDto} from '../../_dtos/save-resolution-dto';
import {LinterError} from '../../_models/linter-error';
import {ActivatedRoute, Router} from '@angular/router';
import {ResolutionService} from '../../_services/resolution.service';
import {logger} from 'codelyzer/util/logger';


@Component({
  selector: 'app-code-editor',
  templateUrl: './code-editor.component.html',
  styleUrls: ['./code-editor.component.css']
})
export class CodeEditorComponent implements AfterViewInit {

  @ViewChild('editor') private editor: ElementRef<HTMLElement>;
  @Input() initialCode = '';
  @Input() algorithmId: number;
  compileRequest: CompileRequest;
  executionResult: RunResult;
  language: Language = Language.Cpp;
  theme = 'twilight';

  constructor(private compilerService: CompilerService,
              private dialog: MatDialog,
              private router: Router,
              private route: ActivatedRoute,
              private resolutionService: ResolutionService,
              private accountService: AccountService,
              public socketService: SocketService) {
  }

  ngAfterViewInit(): void {
    ace.config.set('fontSize', '14px');

    ace.config.set('basePath', 'https://unpkg.com/ace-builds@1.4.12/src-noconflict');

    const aceEditor = ace.edit(this.editor.nativeElement);
    aceEditor.session.setValue(this.initialCode ? this.initialCode : '');

    this.selectTheme();
    this.selectLanguage();
    this.subscribe();
  }

  async submit() {
    this.buildRequest();
    console.log(this.compileRequest);
    await this.compilerService.runCode(this.compileRequest)
      .subscribe(
        compileResponse => {
          this.executionResult = compileResponse;
          console.log(compileResponse);
          this.openExecutionDialog();
          if (this.executionResult.successful) {
            this.saveResolution(this.executionResult.linterErrors);
          }
        },
        error => {
          console.log('error');
        });
  }

  private saveResolution(linterErrors: LinterError[]): void {
    const resolution = new SaveResolutionDto();

    resolution.algorithmId = this.algorithmId;
    resolution.linterErrors = linterErrors;
    resolution.solved = true;

    const _this = this;
    this.resolutionService.saveResolution(resolution)
      .subscribe(resolution => {
        console.log(resolution);
        _this.route.parent.params.subscribe(
          (params) => {
            if(resolution.player.won) {
              this.router.navigate([`/game/${params.idGame}/game-over`]);
            } else {
              this.router.navigate([`/game/${params.idGame}/assets`]);
            }
          });

      }, error => console.log(error));
  }

  public getContent() {
    const aceEditor = ace.edit(this.editor.nativeElement);
    const code = aceEditor.getValue();
    return code;
  }

  buildRequest() {
    this.compileRequest = {
      sourceCode: this.getContent(),
      language: this.language,
      algorithmId: this.algorithmId
    };
  }

  openExecutionDialog() {
    this.dialog.open(ExecutionResultComponent, {
      data: this.executionResult,
      width: '800px',
      backdropClass: 'bdrop'
    });
  }

  selectLanguage() {
    const aceEditor = ace.edit(this.editor.nativeElement);
    switch (this.language as Language) {
      case Language.Java:
        aceEditor.session.setMode('ace/mode/java');
        break;
      case Language.Python:
        aceEditor.session.setMode('ace/mode/python');
        break;
      case Language.Cpp:
        aceEditor.session.setMode('ace/mode/c_cpp');
        break;
      default:
        console.log(this.language);
        break;
    }
  }

  selectTheme() {
    const aceEditor = ace.edit(this.editor.nativeElement);
    switch (this.theme) {
      case 'twilight':
        aceEditor.setTheme('ace/theme/twilight');
        break;
      case 'xcode':
        aceEditor.setTheme('ace/theme/xcode');
        break;
      case 'solarized':
        aceEditor.setTheme('ace/theme/solarized_dark');
        break;
      default:
        aceEditor.setTheme('ace/theme/twilight');
        break;
    }
  }

  subscribe() {
    const _this = this;
    this.accountService.currentUser$.subscribe(user => {
      _this.socketService.stompClient.subscribe('/start/' + user.id, function(hello) {
        console.log(hello);
      });
    });

  }
}
