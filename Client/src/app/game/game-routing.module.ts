import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AssetsComponent } from './assets/assets.component';
import { AssetsModule } from './assets/assets.module';
import { GameInterafceComponent } from './game-interafce.component';


const routes: Routes = [
  { path: '',
    component: GameInterafceComponent,
    children: [
      {
        path: 'assets',
        loadChildren: () =>
          import('./assets/assets.module').then((m) => m.AssetsModule)
      },
      {
        path: 'algorithm',
        loadChildren: () =>
          import('./algorithm-interface/algorithm-interface.module').then((m) => m.AlgorithmInterfaceModule)
      },
      {
        path: 'game-over',
        loadChildren: () =>
          import('./game-over/game-over.module').then((m) => m.GameOverModule)
      }
    ],
  },
  { path: '', redirectTo: '/assets', pathMatch: 'full' },
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports : [RouterModule]
})
export class GameRoutingModule { }
