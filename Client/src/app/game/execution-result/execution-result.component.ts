import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RunResult } from 'src/app/_models/runResult.model';

@Component({
  selector: 'app-execution-result',
  templateUrl: './execution-result.component.html',
  styleUrls: ['./execution-result.component.css']
})
export class ExecutionResultComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: RunResult) {}

  ngOnInit(): void {
  }

}
