import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Routes, RouterModule } from '@angular/router';
import { AlgorithmInterfaceComponent } from './algorithm-interface.component';


const routes: Routes = [
  {path: ':idAlgo', component: AlgorithmInterfaceComponent}
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports : [RouterModule]
})
export class AlgorithmInterfaceRoutingModule { }

