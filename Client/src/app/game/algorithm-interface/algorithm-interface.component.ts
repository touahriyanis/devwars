import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlgorithmData } from 'src/app/_models/algorithm-data.model';
import { AlgorithmService } from 'src/app/_services/algorithm.service';

@Component({
  selector: 'app-algorithm-interface',
  templateUrl: './algorithm-interface.component.html',
  styleUrls: ['./algorithm-interface.component.css']
})
export class AlgorithmInterfaceComponent implements OnInit {

  constructor(private route: ActivatedRoute,
    private router: Router,
    private algorithmService: AlgorithmService) { }

    algoId: number;
    algorithm: AlgorithmData;

    ngOnInit(): void {
      this.algoId = +this.route.snapshot.paramMap.get('idAlgo');
      if(this.algoId == null) {
        this.router.navigateByUrl('/');
      } else {
        this.getAlgorithm();
      }
    }

    async getAlgorithm() {
      await this.algorithmService.getAlgorithmById(this.algoId)
      .subscribe( algo => {
        this.algorithm = algo;
      },
      error => {
        console.log("error");
      });
    }

}
