import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlgorithmInterfaceComponent } from './algorithm-interface.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { CodeEditorComponent } from '../code-editor/code-editor.component';
import { ExecutionResultComponent } from '../execution-result/execution-result.component';
import { AlgorithmInterfaceRoutingModule } from './algorithm-interface-routing.module';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    AlgorithmInterfaceComponent,
    CodeEditorComponent,
    ExecutionResultComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    AlgorithmInterfaceRoutingModule
  ]
})
export class AlgorithmInterfaceModule { }
