import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlgorithmData } from '../_models/algorithm-data.model';
import { ResolutionByGame } from '../_models/resolutionByGame.model';
import { UsersByGameResponse } from '../_models/usersByGameResponse.model';
import { AccountService } from '../_services/account.service';
import { AlgorithmService } from '../_services/algorithm.service';
import { GameService } from '../_services/game.service';

@Component({
  selector: 'app-game-interafce',
  templateUrl: './game-interafce.component.html',
  styleUrls: ['./game-interafce.component.css']
})
export class GameInterafceComponent implements OnInit {

  gameId: number;
  player: UsersByGameResponse;
  opponent: UsersByGameResponse;
  historyTurns: ResolutionByGame[];  

  currentUserId: number;

  constructor(private gameService: GameService,
              private route: ActivatedRoute,
              private accountService: AccountService,
              private router: Router) { }

  ngOnInit(): void {
    this.gameId = +this.route.snapshot.paramMap.get('idGame');
    if(this.gameId == null) {
      this.router.navigateByUrl('/');
    } else {
      this.getGamePlayers();
      this.getResolutions();

      this.accountService.currentUser$.subscribe(currentUser => {
        if (currentUser != null) {
          this.currentUserId = currentUser.id;
        }
      });
    }
  }

  getGamePlayers() {
    this.gameService.findAllUsersByGame(this.gameId).subscribe(players => {
      this.player = players.find(player => player.user.id === this.currentUserId);
      this.opponent = players.find(player => player.user.id !== this.currentUserId);
      localStorage.setItem('opponentId', this.opponent.user.id.toString());
    });
  }

  getResolutions() {
    this.gameService.findAllResolutionsByGame(this.gameId).subscribe(resolutions => {
      this.historyTurns = resolutions;
    });
  }

}
