import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameOverComponent } from './game-over.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { GameOverRoutingModule } from './game-over-routing.module';




@NgModule({
  declarations: [GameOverComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    GameOverRoutingModule
  ]
})
export class GameOverModule { }
