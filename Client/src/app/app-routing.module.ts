import { NgModule } from "@angular/core";
import {ExtraOptions, RouterModule, Routes} from '@angular/router';
import { HomeComponent } from "./home/home.component";
import { AuthGuard } from "./_guards/auth.guard";



const routes : Routes = [
    {path: '', component: HomeComponent},
    {path: 'games', loadChildren: () => import('./game-lobby/game-lobby.module').then(mod => mod.GameLobbyModule), canActivate: [AuthGuard]},
    {path: 'game/:idGame', loadChildren: () => import('./game/game.module').then(mod => mod.GameModule), canActivate: [AuthGuard]},
    {path: 'app', loadChildren: () => import('./core/core.module').then(mod => mod.CoreModule)},
    {path: '**', redirectTo: '', pathMatch: 'full'}
];

export const routingConfiguration: ExtraOptions = {
  paramsInheritanceStrategy: 'always'
};

@NgModule({
    imports: [RouterModule.forRoot(routes, routingConfiguration)],
    exports: [RouterModule]
})
export class AppRoutingModule { }


