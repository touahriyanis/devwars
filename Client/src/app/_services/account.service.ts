import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {ReplaySubject} from 'rxjs/internal/ReplaySubject';
import {environment} from 'src/environments/environment';
import {User} from '../_models/user.model';
import {catchError, map} from 'rxjs/operators';
import {BehaviorSubject} from 'rxjs';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AccountService {


  private  currentUserSource = new ReplaySubject<User>(1);
  currentUser$ = this.currentUserSource.asObservable();

  constructor(private http: HttpClient, private router: Router) {
  }

  login(model: any) {
    return this.http.post(`/api/users/signin`, model)
      .pipe(map((response: User) => {
        const user = response;
        if (user) {
          this.setCurrentUser(user);
          return user;
        }
      }));
  }

  register(model: any) {
    return this.http.post(`/api/users`, model).pipe(
      map((user: User) => {
        if (user) {
          this.setCurrentUser(user);
        }
      })
    )
  }

  setCurrentUser(user: User) {
    localStorage.setItem('user', JSON.stringify(user));
    this.currentUserSource.next(user);
  }

  logout() {
    localStorage.removeItem('user');
    this.currentUserSource.next(null);
    this.router.navigate(['/']);
  }
}
