import { Injectable } from '@angular/core';

import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  stompClient:any = null;
  isConnected = false;
  baseUrl = environment.socketUrl;

  constructor() {
    this.connect();
  }

  async connect() {
    const socket = new SockJS(`${this.baseUrl}ws`);
    this.stompClient = Stomp.over(socket);
    const _this = this;
    await this.stompClient.connect({}, function (frame) {
      console.log('Socket connected: ' + frame);
      _this.isConnected = true;
    });
  }

}
