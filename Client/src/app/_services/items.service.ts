import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Item} from '../_models/item';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  constructor(private http: HttpClient) {
  }

  public getUserItems(userId: number): Observable<Item[]> {
    return this.http.get<Item[]>(`/api/users/${userId}/items`);
  }

  public getUsedItemsByUser(userId: number): Observable<Item[]> {
    return this.http.get<Item[]>(`/api/users/${userId}/items/used`);
  }

}
