import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Item} from '../_models/item';
import {Resolution} from '../_models/resolution';
import {StartResolutionDto} from '../_dtos/start-resolution-dto';
import {SaveResolutionDto} from '../_dtos/save-resolution-dto';

@Injectable({
  providedIn: 'root'
})
export class ResolutionService {

  constructor(private http: HttpClient) {
  }

  public startResolutionWithItem(resolution: StartResolutionDto): Observable<Resolution> {
    return this.http.post<Resolution>(`/api/resolutions/start/item`, resolution);
  }

  public startResolutionWithAction(resolution: StartResolutionDto): Observable<Resolution> {
    return this.http.post<Resolution>(`/api/resolutions/start/action`, resolution);
  }

  public saveResolution(resolution: SaveResolutionDto): Observable<Resolution> {
    return this.http.put<Resolution>(`/api/resolutions`, resolution);
  }

}
