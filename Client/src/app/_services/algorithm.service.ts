import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {environment} from 'src/environments/environment';
import {AlgorithmData} from '../_models/algorithm-data.model';

@Injectable({
  providedIn: 'root'
})
export class AlgorithmService {

  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {
  }

  getAlgorithmsByPower(power: number, userId: number) {
    return this.http.get<AlgorithmData[]>(`${this.baseUrl}algorithms/power/${power}/user/${userId}`);
  }

  getAlgorithmById(id: number) {
    return this.http.get<AlgorithmData>(`${this.baseUrl}algorithms/${id}`);
  }
}
