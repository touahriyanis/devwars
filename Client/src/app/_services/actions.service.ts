import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Action} from '../_models/action';

@Injectable({
  providedIn: 'root'
})
export class ActionsService {

  constructor(private http: HttpClient) {
  }

  public getUserActions(userId: number): Observable<Action[]> {
    return this.http.get<Action[]>(`/api/users/${userId}/actions`);
  }

}
