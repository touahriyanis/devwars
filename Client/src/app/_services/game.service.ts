import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Game } from '../_models/game.model';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { UsersByGameResponse } from '../_models/usersByGameResponse.model';
import { PlayerRequest } from '../_models/playerRequest.module';
import { PlayerResponse } from '../_models/playerResponse.model';
import { ResolutionByGame } from '../_models/resolutionByGame.model';


@Injectable({
  providedIn: 'root'
})
export class GameService {

  constructor(private http: HttpClient) {
  }

  public findAll(): Observable<Game[]>{
    return this.http.get<Game[]>("api/games");
  }

  public findAllUnfinishedGames(): Observable<Game[]>{
    return this.http.get<Game[]>("/api/games/unfinished");
  }

  public findAllUsersByGame(gameId: number): Observable<UsersByGameResponse[]> {
    return this.http.get<UsersByGameResponse[]>(`/api/games/${gameId}/users`);
  }

  public addOrUpdateUserToGame(gameId: number, userId: number, playerRequest: PlayerRequest) {
    return this.http.put<PlayerResponse>(`/api/games/${gameId}/users/${userId}`, playerRequest);
  }

  public addOrUpdateGameToUser(gameId: number, userId: number, playerRequest: PlayerRequest) {
    return this.http.put<PlayerResponse>(`/api/users/${userId}/games/${gameId}`, playerRequest);
  }

  public findAllResolutionsByGame(gameId: number): Observable<ResolutionByGame[]> {
    return this.http.get<ResolutionByGame[]>(`/api/games/${gameId}/algorithms`);
  }
}
