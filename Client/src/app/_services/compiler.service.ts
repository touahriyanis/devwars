import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { CompileRequest } from '../_models/compileRequest.model';
import { RunResult } from '../_models/runResult.model';

@Injectable({
  providedIn: 'root'
})
export class CompilerService {

  constructor(private http: HttpClient, private router: Router) { }

  runCode(compileRequest: CompileRequest) {
    return this.http.post<RunResult>(`/api/compiler`, compileRequest);
  }
}
