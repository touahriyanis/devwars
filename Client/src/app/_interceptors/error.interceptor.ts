import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { AccountService } from '../_services/account.service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/internal/operators/catchError';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(private router: Router,
              private accountService: AccountService
    ) { }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
    catchError(error => {
      if (error) {
        switch (error.status) {
          case 401:
            this.accountService.logout();
            break;
          default:
            console.log('Something unexpected went wrong');
            break;
        }
      return throwError(error);
      }
    })
    )
    }
}
