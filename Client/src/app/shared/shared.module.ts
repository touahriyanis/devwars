import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import {MatSelectModule} from '@angular/material/select';
import {MatDialogModule} from '@angular/material/dialog';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatCardModule} from '@angular/material/card';
import {MatIconModule} from '@angular/material/icon';
import {ScrollingModule} from '@angular/cdk/scrolling';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    BsDropdownModule.forRoot(),
    NgxSpinnerModule,
    ButtonsModule.forRoot(),
    MatSelectModule,
    MatDialogModule,
    MatExpansionModule,
    MatCardModule,
    MatIconModule,
    ScrollingModule,
    PaginationModule,
    ProgressbarModule.forRoot(),
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-center'
    })
  ],
  exports: [
    BsDropdownModule,
    NgxSpinnerModule,
    ButtonsModule,
    MatSelectModule,
    MatDialogModule,
    MatExpansionModule,
    MatCardModule,
    MatIconModule,
    ScrollingModule,
    PaginationModule,
    ProgressbarModule,
    ToastrModule
  ]
})
export class SharedModule { }
