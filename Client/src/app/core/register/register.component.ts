import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AccountService} from 'src/app/_services/account.service';
import {MustMatch} from 'src/app/_validators/must-match.validator';
import {first, take} from 'rxjs/operators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;
  error = '';

  constructor(private formBuilder: FormBuilder,
              private accountService: AccountService,
              private router: Router,
              private route: ActivatedRoute) {
                this.accountService.currentUser$.pipe(take(1)).subscribe(user => {
                  if (user) {
                    this.router.navigate(['/']);
                  }
                });
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required],
      acceptTerms: [false, Validators.requiredTrue]
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    this.error = '';
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    this.accountService.register(this.registerForm.value)
      .subscribe(
        data => {
          this.router.navigate(['../login'], {relativeTo: this.route});
        },
        error => {
          this.error = error.error;
        });
  }

  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }
}
