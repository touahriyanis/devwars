import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { GameLobbyModule } from './game-lobby/game-lobby.module';
import { GameModule } from './game/game.module';
import { HomeModule } from './home/home.module';
import { AppRoutingModule } from './app-routing.module';
import { JwtInterceptor } from './_interceptors/jwt.interceptor';
import { LoadingInterceptor } from './_interceptors/loading.interceptor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationConfig } from 'ngx-bootstrap/pagination';
import { ErrorInterceptor } from './_interceptors/error.interceptor';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    CoreModule,
    GameModule,
    GameLobbyModule,
    HomeModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true},
      {provide: HTTP_INTERCEPTORS,
        useClass: ErrorInterceptor,
        multi: true},
    {provide: HTTP_INTERCEPTORS,
      useClass: LoadingInterceptor,
      multi: true},
      PaginationConfig
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
