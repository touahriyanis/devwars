package fr.esgi.devwars.unit.compiler.domain.strategies;

import fr.esgi.devwars.compiler.domain.model.LinterError;
import fr.esgi.devwars.compiler.domain.strategies.AnalyzeCodePython;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static fr.esgi.devwars.compiler.domain.strategies.AnalyzeCodePython.PYTHON_MAX_METHOD_LINES;
import static org.junit.jupiter.api.Assertions.*;


class AnalyzeCodePythonTest {

    private AnalyzeCodePython sut;


    @BeforeEach
    public void setup() {
        sut = new AnalyzeCodePython();
    }

    @DisplayName("Find duplicated lines")
    @Nested
    class DuplicatedLines {

        @Test
        @DisplayName("With duplicated keywords")
        public void keywords_should_not_count_as_duplicated() {
            Set<LinterError> linterErrors = new HashSet<>();
            LinterError linterError1 = new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage("Line 15 and line 17 are duplicated");
            LinterError linterError2 = new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage("Line 16 and line 18 are duplicated");
            linterErrors.add(linterError1);
            linterErrors.add(linterError2);

            String sourceCode = "#!/usr/bin/env pipenv-shebang\n" +
                    "# -*- coding: utf-8 -*-\n" +
                    "\n" +
                    "'''\n" +
                    "@Author: AlainOUYANG\n" +
                    "@Date: 2019-10-16 16:52:17\n" +
                    "@LastEditors: AlainOUYANG\n" +
                    "@LastEditTime: 2019-10-17 20:25:34\n" +
                    "@Description: fibonacci\n" +
                    "'''\n" +
                    "\n" +
                    "def fibonacci_item(n):\n" +
                    "    if n <= 0:\n" +
                    "        return 0\n" +
                    "    elif n == 1:\n" +
                    "        return 1\n" +
                    "    elif n == 1:\n" +
                    "        return 1 \n" +
                    "    elif n == 5:\n" +
                    "        return   \n" +
                    "    elif n == 6:\n" +
                    "        return      \n" +
                    "    else:\n" +
                    "        return fibonacci_item(n - 1) + fibonacci_item(n - 2)\n" +
                    "\n" +
                    "\n" +
                    "def fibonacci_list(n):\n" +
                    "    lst = []\n" +
                    "    for i in range(1, n+1):\n" +
                    "        lst.append(fibonacci_item(i))\n" +
                    "    return lst\n" +
                    "\n" +
                    "\n" +
                    "def main():\n" +
                    "    print(fibonacci_list(20))\n" +
                    "\n" +
                    "\n" +
                    "if __name__ == \"__main__\":\n" +
                    "    main()";

            assertEquals(linterErrors, sut.findDuplicatedLines(sourceCode));
        }

        @Test
        @DisplayName("With duplicated keywords in duplicated fragment")
        public void keywords_should_count_when_fragment_is_duplicated() {
            Set<LinterError> linterErrors = new HashSet<>();
            LinterError linterError1 = new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage("Line 15 and line 17 are duplicated");
            LinterError linterError2 = new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage("Line 16 and line 18 are duplicated");
            LinterError linterError3 = new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage("Line 19 and line 21 are duplicated");
            LinterError linterError4 = new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage("Line 20 and line 22 are duplicated");
            linterErrors.add(linterError1);
            linterErrors.add(linterError2);
            linterErrors.add(linterError3);
            linterErrors.add(linterError4);

            String sourceCode = "#!/usr/bin/env pipenv-shebang\n" +
                    "# -*- coding: utf-8 -*-\n" +
                    "\n" +
                    "'''\n" +
                    "@Author: AlainOUYANG\n" +
                    "@Date: 2019-10-16 16:52:17\n" +
                    "@LastEditors: AlainOUYANG\n" +
                    "@LastEditTime: 2019-10-17 20:25:34\n" +
                    "@Description: fibonacci\n" +
                    "'''\n" +
                    "\n" +
                    "def fibonacci_item(n):\n" +
                    "    if n <= 0:\n" +
                    "        return 0\n" +
                    "    elif n == 1:\n" +
                    "        return 1\n" +
                    "    elif n == 1:\n" +
                    "        return 1 \n" +
                    "    elif n == 5:\n" +
                    "        return   \n" +
                    "    elif n == 5:\n" +
                    "        return      \n" +
                    "    else:\n" +
                    "        return fibonacci_item(n - 1) + fibonacci_item(n - 2)\n" +
                    "\n" +
                    "\n" +
                    "def fibonacci_list(n):\n" +
                    "    lst = []\n" +
                    "    for i in range(1, n+1):\n" +
                    "        lst.append(fibonacci_item(i))\n" +
                    "    return lst\n" +
                    "\n" +
                    "\n" +
                    "def main():\n" +
                    "    print(fibonacci_list(20))\n" +
                    "\n" +
                    "\n" +
                    "if __name__ == \"__main__\":\n" +
                    "    main()";

            assertEquals(linterErrors, sut.findDuplicatedLines(sourceCode));
        }

        @Test
        @DisplayName("With basic duplicated lines")
        public void should_return_duplicated_lines() {
            Set<LinterError> linterErrors = new HashSet<>();
            LinterError linterError1 = new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage("Line 1 and line 5 are duplicated");
            linterErrors.add(linterError1);

            String sourceCode = "def main():\n" +
                    "    \n" +
                    "    print(fibonacci_list(20))\n" +
                    "\n" +
                    "def main():\n" +
                    "\n" +
                    "    print(\"ehyo\")";

            assertEquals(linterErrors, sut.findDuplicatedLines(sourceCode));
        }

    }

    @DisplayName("Find too large methods")
    @Nested
    class TooLargeMethods {


        @DisplayName("With less than max lines")
        @Test
        public void should_not_return_method_with_less_than_20_lines() {
            String code = "def isfloat(num):\n" +
                    "    try:\n" +
                    "        float(num)\n" +
                    "        return True\n" +
                    "\n" +
                    "    except ValueError:\n" +
                    "        \n" +
                    "        return False\n" +
                    "\n" +
                    "def bonjour(str):\n" +
                    "\n" +
                    "\n" +
                    "\n" +
                    "print(\"hello\") ";

            assertEquals(new ArrayList<>(), sut.findTooLargeMethods(code));
        }

        @DisplayName("With more than max lines")
        @Test
        public void should_return_method_with_more_than_20_lines() {
            String code = "def isfloat(num):\n" +
                    "    try:\n" +
                    "        float(num)\n" +
                    "        return True\n" +
                    "\n" +
                    "    except ValueError:\n" +
                    "\n" +
                    "        return False\n" +
                    "\n" +
                    "def bonjour():\n" +
                    "    print(\"test\")\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "\n" +
                    "def bonjour():\n" +
                    "    print(\"test\")\n" +
                    "    print\n" +
                    "    print\n" +
                    "\n" +
                    "\n" +
                    "\n" +
                    "    print\n" +
                    "    \n" +
                    "    print\n" +
                    "    \n" +
                    "    print\n" +
                    "    \n" +
                    "    print\n" +
                    "    print\n" +
                    "    \n" +
                    "    print\n" +
                    "    \n" +
                    "    print\n" +
                    "    print\n" +
                    "    \n" +
                    "    print\n" +
                    "    print\n" +
                    "\n" +
                    "    print\n" +
                    "\n" +
                    "print \n" +
                    "\n" +
                    "print \n" +
                    "\n";
            List<LinterError> linterErrors = new ArrayList<>();

            LinterError linterError1 = new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage(
                            String.format("Method bonjour contains more than %s lines (%s)",
                                    PYTHON_MAX_METHOD_LINES, 24));
            linterErrors.add(linterError1);

            assertEquals(linterErrors, sut.findTooLargeMethods(code));
        }

        @DisplayName("With twice more than max lines")
        @Test
        public void should_return_method_error_number_2() {
            String code = "def isfloat(num):\n" +
                    "    try:\n" +
                    "        float(num)\n" +
                    "        return True\n" +
                    "\n" +
                    "    except ValueError:\n" +
                    "\n" +
                    "        return False\n" +
                    "\n" +
                    "def bonjour():\n" +
                    "    print(\"test\")\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "\n" +
                    "def bonjour():\n" +
                    "    print(\"test\")\n" +
                    "    print\n" +
                    "    print\n" +
                    "\n" +
                    "\n" +
                    "\n" +
                    "    print\n" +
                    "    \n" +
                    "    print\n" +
                    "    \n" +
                    "    print\n" +
                    "    \n" +
                    "    print\n" +
                    "    print\n" +
                    "    \n" +
                    "    print\n" +
                    "    \n" +
                    "    print\n" +
                    "    print\n" +
                    "    \n" +
                    "    print\n" +
                    "    print\n" +
                    "\n" +
                    "    print\n" +
                    "\n" +
                    "print \n" +
                    "\n" +
                    "print ";

            List<LinterError> linterErrors = new ArrayList<>();

            LinterError linterError1 = new LinterError()
                    .setErrorNumber(2)
                    .setErrorMessage(
                            String.format("Method bonjour contains more than %s lines (%s)",
                                    PYTHON_MAX_METHOD_LINES, 46));
            linterErrors.add(linterError1);

            assertEquals(linterErrors, sut.findTooLargeMethods(code));
        }

        @DisplayName("With many too long methods")
        @Test
        public void should_return_error_on_all_too_long_methods() {
            String code = "def isfloat(num):\n" +
                    "    try:\n" +
                    "        float(num)\n" +
                    "        return True\n" +
                    "\n" +
                    "    except ValueError:\n" +
                    "\n" +
                    "        return False\n" +
                    "\n" +
                    "def bonjour():\n" +
                    "    print(\"test\")\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "\n" +
                    "def aurevoir():\n" +
                    "    print(\"test\")\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "    print\n" +
                    "\n";

            List<LinterError> linterErrors = new ArrayList<>();

            LinterError linterError1 = new LinterError()
                    .setErrorNumber(2)
                    .setErrorMessage(
                            String.format("Method bonjour contains more than %s lines (%s)",
                                    PYTHON_MAX_METHOD_LINES, 46));
            LinterError linterError2 = new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage(
                            String.format("Method aurevoir contains more than %s lines (%s)",
                                    PYTHON_MAX_METHOD_LINES, 27));

            linterErrors.add(linterError1);
            linterErrors.add(linterError2);

            assertEquals(linterErrors, sut.findTooLargeMethods(code));
        }
    }

}
