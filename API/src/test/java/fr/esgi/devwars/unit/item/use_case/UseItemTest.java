package fr.esgi.devwars.unit.item.use_case;

import fr.esgi.devwars.item.domain.model.Item;
import fr.esgi.devwars.item.use_case.UseItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UseItemTest {

    private UseItem sut;

    @BeforeEach
    public void setup() {
        sut = new UseItem(null, null, null, null);
    }

    @Test
    @DisplayName("Calculate damage should take off 10% by linter error")
    public void calculate_damage_should_retire_ten_percent() {
        int errors = 3;
        Item item = new Item().setDamageAmount(5);
        assertEquals(4, sut.calculateDamageAmount(item, errors));
    }

    @Test
    @DisplayName("Calculate damage should return positive number")
    public void calculate_damage_should_return_positive_number() {
        int errors = 17;
        Item item = new Item().setDamageAmount(5);
        assertEquals(1, sut.calculateDamageAmount(item, errors));
    }

    @Test
    @DisplayName("Calculate heal should take off 10% by linter error")
    public void calculate_heal_should_retire_ten_percent() {
        int errors = 3;
        Item item = new Item().setHealAmount(5);
        assertEquals(4, sut.calculateHealAmount(item, errors));
    }

    @Test
    @DisplayName("Calculate heal should return positive number")
    public void calculate_heal_should_return_positive_number() {
        int errors = 17;
        Item item = new Item().setHealAmount(5);
        assertEquals(1, sut.calculateHealAmount(item, errors));
    }

}
