package fr.esgi.devwars.unit.compiler.domain.strategies;

import fr.esgi.devwars.compiler.domain.model.LinterError;
import fr.esgi.devwars.compiler.domain.strategies.AnalyzeCodeCpp;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static fr.esgi.devwars.compiler.domain.strategies.AnalyzeCodeCpp.CPP_MAX_LINE_CHARACTERS;
import static fr.esgi.devwars.compiler.domain.strategies.AnalyzeCodeCpp.CPP_MAX_METHOD_LINES;
import static org.junit.jupiter.api.Assertions.assertEquals;


class AnalyzeCodeCppTest {

    private AnalyzeCodeCpp sut;

    @BeforeEach
    public void setup() {
        sut = new AnalyzeCodeCpp();
    }

    @DisplayName("Find duplicated lines")
    @Nested
    class DuplicatedLines {

        @Test
        @DisplayName("With duplicated keywords")
        public void keywords_should_not_count_as_duplicated() {
            Set<LinterError> linterErrors = new HashSet<>();

            String sourceCode = "int main() {\n" +
                    "    if(0 == 0) {\n" +
                    "        return;\n" +
                    "    } else if (0 == 1) {\n" +
                    "        return;\n" +
                    "    }\n" +
                    "    \n" +
                    "}";


            assertEquals(linterErrors, sut.findDuplicatedLines(sourceCode));
        }

        @Test
        @DisplayName("With duplicated keywords in duplicated fragment")
        public void keywords_should_count_when_fragment_is_duplicated() {
            Set<LinterError> linterErrors = new HashSet<>();
            LinterError linterError1 = new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage("Line 2 and line 5 are duplicated");
            LinterError linterError2 = new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage("Line 3 and line 6 are duplicated");
            linterErrors.add(linterError1);
            linterErrors.add(linterError2);

            String sourceCode = "int main() {\n" +
                    "    if(0 == 0) {\n" +
                    "        return;\n" +
                    "    } \n" +
                    "        if(0 == 0) {\n" +
                    "        return;\n" +
                    "    } \n" +
                    "\n" +
                    "}";

            assertEquals(linterErrors, sut.findDuplicatedLines(sourceCode));
        }

        @Test
        @DisplayName("With basic duplicated lines")
        public void should_return_duplicated_lines() {
            Set<LinterError> linterErrors = new HashSet<>();
            LinterError linterError1 = new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage("Line 2 and line 10 are duplicated");
            linterErrors.add(linterError1);

            LinterError linterError2 = new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage("Line 3 and line 11 are duplicated");
            linterErrors.add(linterError2);

            LinterError linterError3 = new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage("Line 4 and line 12 are duplicated");
            linterErrors.add(linterError3);

            String sourceCode = "int foo() {\n" +
                    "    for (int i = 1; i <=  n2; ++i) {\n" +
                    "        if (n1 % i == 0 && n2 % i ==0) {\n" +
                    "            hcf = i;\n" +
                    "        }\n" +
                    "    }\n" +
                    "}\n" +
                    "\n" +
                    "int bar() {\n" +
                    "    for (int i = 1; i <=  n2; ++i) {\n" +
                    "        if (n1 % i == 0 && n2 % i ==0) {\n" +
                    "            hcf = i;\n" +
                    "        }\n" +
                    "    }\n" +
                    "}";


            assertEquals(linterErrors, sut.findDuplicatedLines(sourceCode));
        }

        @Test
        @DisplayName("With lines separated by ;")
        public void should_return_duplicated_lines_separated_by_comma() {
            Set<LinterError> linterErrors = new HashSet<>();
            LinterError linterError1 = new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage("Line 2 and line 10 are duplicated");
            linterErrors.add(linterError1);

            LinterError linterError2 = new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage("Line 3 and line 11 are duplicated");
            linterErrors.add(linterError2);

            LinterError linterError3 = new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage("Line 4 and line 12 are duplicated");
            linterErrors.add(linterError3);

            LinterError linterError4 = new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage("Line 4 contains duplicated code");
            linterErrors.add(linterError4);

            LinterError linterError5 = new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage("Line 12 contains duplicated code");
            linterErrors.add(linterError5);

            String sourceCode = "int foo() {\n" +
                    "    for (int i = 1; i <=  n2; ++i) {\n" +
                    "        if (n1 % i == 0 && n2 % i ==0) {\n" +
                    "            hcf = i; hcf = i;\n" +
                    "        }\n" +
                    "    }\n" +
                    "}\n" +
                    "\n" +
                    "int bar() {\n" +
                    "    for (int i = 1; i <=  n2; ++i) {\n" +
                    "        if (n1 % i == 0 && n2 % i ==0) {\n" +
                    "            hcf = i; hcf = i;\n" +
                    "        }\n" +
                    "    }\n" +
                    "}";


            assertEquals(linterErrors, sut.findDuplicatedLines(sourceCode));
        }

    }

    @DisplayName("Find too large methods")
    @Nested
    class TooLargeMethods {

        @DisplayName("With less than max lines")
        @Test
        public void should_not_return_method_with_less_than_20_lines() {
            String code = "int foor\n" +
                    "            (\n" +
                    "\n" +
                    "            ) {\n" +
                    "        return 1;\n" +
                    "    }\n" +
                    "\n" +
                    "    void main() {\n" +
                    "        final var bonjour = 5;\n" +
                    "        if(bonjour == 10) {\n" +
                    "            System.out.println(\"c bizar\");\n" +
                    "        }\n" +
                    "        final var bjr = 5; final var aurevoir = 7;\n" +
                    "    }\n" +
                    "\n" +
                    "    void foo(){}\n" +
                    "\n" +
                    "    void bar(){\n" +
                    "\n" +
                    "    }\n" +
                    "\n" +
                    "        int whileforelseif() {\n" +
                    "            if(10 - 8 == 3) {\n" +
                    "                return 0;\n" +
                    "            }else if(3 - 4 == 3) {\n" +
                    "                return 1;\n" +
                    "            }else {\n" +
                    "                return 3;\n" +
                    "            }\n" +
                    "\n" +
                    "        }";

            assertEquals(new ArrayList<>(), sut.findTooLargeMethods(code));
        }

        @DisplayName("With more than max lines")
        @Test
        public void should_return_method_with_more_than_20_lines() {
            String code = "void main() {\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "}";

            List<LinterError> linterErrors = new ArrayList<>();

            LinterError linterError1 = new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage(
                            String.format("Method main contains more than %s lines (%s)",
                                    CPP_MAX_METHOD_LINES, 21));
            linterErrors.add(linterError1);

            assertEquals(linterErrors, sut.findTooLargeMethods(code));
        }

        @DisplayName("With twice more than max lines")
        @Test
        public void should_return_method_error_number_2() {
            String code = "void bonjour() {\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "\n" +
                    "\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "\n" +
                    "\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "}\n" +
                    "\n" +
                    "int aurevoir() {\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    return 1;\n" +
                    "}";

            List<LinterError> linterErrors = new ArrayList<>();

            LinterError linterError1 = new LinterError()
                    .setErrorNumber(2)
                    .setErrorMessage(
                            String.format("Method aurevoir contains more than %s lines (%s)",
                                    CPP_MAX_METHOD_LINES, 43));
            linterErrors.add(linterError1);

            assertEquals(linterErrors, sut.findTooLargeMethods(code));
        }

        @DisplayName("With many too long methods")
        @Test
        public void should_return_error_on_all_too_long_methods() {
            String code = "void bonjour() {\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "\n" +
                    "\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "\n" +
                    "\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "}\n" +
                    "\n" +
                    "int aurevoir() {\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    cout << \"Hello\";\n" +
                    "    return 1;\n" +
                    "}";

            List<LinterError> linterErrors = new ArrayList<>();

            LinterError linterError1 = new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage(
                            String.format("Method bonjour contains more than %s lines (%s)",
                                    CPP_MAX_METHOD_LINES, 24));

            LinterError linterError2 = new LinterError()
                    .setErrorNumber(2)
                    .setErrorMessage(
                            String.format("Method aurevoir contains more than %s lines (%s)",
                                    CPP_MAX_METHOD_LINES, 43));

            linterErrors.add(linterError1);
            linterErrors.add(linterError2);


            assertEquals(linterErrors, sut.findTooLargeMethods(code));
        }
    }

    @Nested
    @DisplayName("Too long lines")
    class TooLongLines {

        @DisplayName("With only short lines")
        @Test
        public void should_return_void_if_lines_under_limit() {
            String code = "int main()\n" +
                    "{\n" +
                    "    cout<<\"Hello Worlddddddddddddddddddddddddddddddddddddddddddddddddddddddsssdddd\";\n" +
                    "\n" +
                    "    return 0;\n" +
                    "}";

            assertEquals(new HashSet<>(), sut.findTooLongLines(code));
        }

        @DisplayName("With too long lines")
        @Test
        public void should_return_errors_if_lines_above_limit() {
            String code = "int main()\n" +
                    "{\n" +
                    "    cout<<\"Hello Worldddddddddddddddddddddddddddddddddddddddddddddddddddddddsssdddd\";\n" +
                    "    if(1 - 3 - 4 - 5 - 9 - 9 == 3) cout<<\"yotiytytnk\" else if (1 - 3 - 4 - 5 - 9 - 9 == 2)cout<<\"Hey\";\n" +
                    "    return 0;\n" +
                    "}";
            Set<LinterError> linterErrors = new HashSet<>();

            linterErrors.add(new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage(String.format("Line %s contains more than %s characters (%s)",
                            3, CPP_MAX_LINE_CHARACTERS, 81)));

            linterErrors.add(new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage(String.format("Line %s contains more than %s characters (%s)",
                            4, CPP_MAX_LINE_CHARACTERS, 98)));

            assertEquals(linterErrors, sut.findTooLongLines(code));
        }

        @DisplayName("With twice too long lines")
        @Test
        public void should_add_errors_every_20_lines_above_limit() {
            String code = "int main()\n" +
                    "{\n" +
                    "    cout<<\"Hello Worldddddddddddddddddddddddddddddddddddddddddddddddddddddddsssdddd\";\n" +
                    "    \n" +
                    "    if(1 - 3 - 4 - 5 - 9 - 9 == 3) cout<<\"yotiytytnk\" else if (1 - 3 - 4 - 5 - 9 - 9 == 2)cout<<\"Hello world nnnnnnnnnnnnnn\";\n" +
                    "    \n" +
                    "    return 1;\n" +
                    "    \n" +
                    "}";
            Set<LinterError> linterErrors = new HashSet<>();

            linterErrors.add(new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage(String.format("Line %s contains more than %s characters (%s)",
                            3, CPP_MAX_LINE_CHARACTERS, 81)));

            linterErrors.add(new LinterError()
                    .setErrorNumber(3)
                    .setErrorMessage(String.format("Line %s contains more than %s characters (%s)",
                            5, CPP_MAX_LINE_CHARACTERS, 121)));

            assertEquals(linterErrors, sut.findTooLongLines(code));
        }
    }
}
