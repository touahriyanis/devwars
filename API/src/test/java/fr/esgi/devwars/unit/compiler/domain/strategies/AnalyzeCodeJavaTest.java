package fr.esgi.devwars.unit.compiler.domain.strategies;

import fr.esgi.devwars.compiler.domain.model.LinterError;
import fr.esgi.devwars.compiler.domain.strategies.AnalyzeCodeJava;
import net.minidev.json.JSONUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.web.socket.server.HandshakeHandler;

import java.util.*;
import java.util.stream.Collectors;

import static fr.esgi.devwars.compiler.domain.strategies.AnalyzeCodeJava.JAVA_MAX_LINE_CHARACTERS;
import static fr.esgi.devwars.compiler.domain.strategies.AnalyzeCodeJava.JAVA_MAX_METHOD_LINES;
import static org.junit.jupiter.api.Assertions.assertEquals;


class AnalyzeCodeJavaTest {

    private AnalyzeCodeJava sut;

    @BeforeEach
    public void setup() {
        sut = new AnalyzeCodeJava();
    }

    @DisplayName("Find duplicated lines")
    @Nested
    class DuplicatedLines {

        @Test
        @DisplayName("With duplicated keywords")
        public void keywords_should_not_count_as_duplicated() {
            Set<LinterError> linterErrors = new HashSet<>();

            String sourceCode = "private void main() {\n" +
                    "                if(main == 1) {\n" +
                    "                    return;\n" +
                    "                }\n" +
                    "                if(main == 2) {\n" +
                    "                    return;\n" +
                    "                }\n" +
                    "            }";


            assertEquals(linterErrors, sut.findDuplicatedLines(sourceCode));
        }

        @Test
        @DisplayName("With duplicated keywords in duplicated fragment")
        public void keywords_should_count_when_fragment_is_duplicated() {
            Set<LinterError> linterErrors = new HashSet<>();
            LinterError linterError1 = new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage("Line 2 and line 5 are duplicated");
            LinterError linterError2 = new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage("Line 3 and line 6 are duplicated");
            linterErrors.add(linterError1);
            linterErrors.add(linterError2);

            String sourceCode = "private void main() {\n" +
                    "            if(main == 1) {\n" +
                    "                return;\n" +
                    "            }\n" +
                    "            if(main == 1) {\n" +
                    "                return;\n" +
                    "            }\n" +
                    "        }";

            assertEquals(linterErrors, sut.findDuplicatedLines(sourceCode));
        }

        @Test
        @DisplayName("With basic duplicated lines")
        public void should_return_duplicated_lines() {
            Set<LinterError> linterErrors = new HashSet<>();
            LinterError linterError1 = new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage("Line 1 and line 5 are duplicated");
            linterErrors.add(linterError1);

            String sourceCode = "private Integer foo () {\n" +
                    "                System.out.println(\"hello\");\n" +
                    "            }\n" +
                    "\n" +
                    "            private Integer foo () {\n" +
                    "                System.out.println(\"hola\");\n" +
                    "                switch (4) {\n" +
                    "                    case 4: return;\n" +
                    "                    break;\n" +
                    "                    case 5: return;\n" +
                    "                    break;\n" +
                    "                }\n" +
                    "                return 3;\n" +
                    "            }";


            assertEquals(linterErrors, sut.findDuplicatedLines(sourceCode));
        }

        @Test
        @DisplayName("With lines separated by ;")
        public void should_return_duplicated_lines_separated_by_comma() {
            Set<LinterError> linterErrors = new HashSet<>();
            LinterError linterError1 = new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage("Line 2 contains duplicated code");
            linterErrors.add(linterError1);

            String sourceCode = "private void main() {\n" +
                    "                System.out.println(\"yo\"); System.out.println(\"yo\"); System.out.println(\"yo\");\n" +
                    "            }";


            assertEquals(linterErrors, sut.findDuplicatedLines(sourceCode));
        }

    }

    @DisplayName("Find too large methods")
    @Nested
    class TooLargeMethods {

        @DisplayName("With less than max lines")
        @Test
        public void should_not_return_method_with_less_than_20_lines() {
            String code = "Integer foor\n" +
                    "            (\n" +
                    "\n" +
                    "            ) {\n" +
                    "        return 1;\n" +
                    "    }\n" +
                    "\n" +
                    "    private void main() {\n" +
                    "        final var bonjour = 5;\n" +
                    "        if(bonjour == 10) {\n" +
                    "            System.out.println(\"c bizar\");\n" +
                    "        }\n" +
                    "        final var bjr = 5; final var aurevoir = 7;\n" +
                    "    }\n" +
                    "\n" +
                    "    private void foo(){}\n" +
                    "\n" +
                    "    private void bar(){\n" +
                    "\n" +
                    "    }\n" +
                    "\n" +
                    "        int whileforelseif() {\n" +
                    "            if(10 - 8 == 3) {\n" +
                    "                return 0;\n" +
                    "            }else if(3 - 4 == 3) {\n" +
                    "                return 1;\n" +
                    "            }else {\n" +
                    "                return 3;\n" +
                    "            }\n" +
                    "\n" +
                    "        }";

            assertEquals(new ArrayList<>(), sut.findTooLargeMethods(code));
        }

        @DisplayName("With more than max lines")
        @Test
        public void should_return_method_with_more_than_20_lines() {
            String code = "public void bonjour() {\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "        }";

            List<LinterError> linterErrors = new ArrayList<>();

            LinterError linterError1 = new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage(
                            String.format("Method bonjour contains more than %s lines (%s)",
                                    JAVA_MAX_METHOD_LINES, 22));
            linterErrors.add(linterError1);

            assertEquals(linterErrors, sut.findTooLargeMethods(code));
        }

        @DisplayName("With twice more than max lines")
        @Test
        public void should_return_method_error_number_2() {
            String code = "public void aurevoir() {\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "\n" +
                    "\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "\n" +
                    "\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "        }\n" +
                    "\n" +
                    "        public void bonjour() {\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "        }";

            List<LinterError> linterErrors = new ArrayList<>();

            LinterError linterError1 = new LinterError()
                    .setErrorNumber(2)
                    .setErrorMessage(
                            String.format("Method bonjour contains more than %s lines (%s)",
                                    JAVA_MAX_METHOD_LINES, 45));
            linterErrors.add(linterError1);

            assertEquals(linterErrors, sut.findTooLargeMethods(code));
        }

        @DisplayName("With many too long methods")
        @Test
        public void should_return_error_on_all_too_long_methods() {
            String code = "public void aurevoir() {\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "        }\n" +
                    "\n" +
                    "        public void bonjour() {\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "            System.out.println(\"yo\");\n" +
                    "        }";

            List<LinterError> linterErrors = new ArrayList<>();

            LinterError linterError1 = new LinterError()
                    .setErrorNumber(2)
                    .setErrorMessage(
                            String.format("Method bonjour contains more than %s lines (%s)",
                                    JAVA_MAX_METHOD_LINES, 45));

            LinterError linterError2 = new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage(
                            String.format("Method aurevoir contains more than %s lines (%s)",
                                    JAVA_MAX_METHOD_LINES, 25));

            linterErrors.add(linterError2);
            linterErrors.add(linterError1);

            assertEquals(linterErrors, sut.findTooLargeMethods(code));
        }
    }


    @Nested
    @DisplayName("Too long lines")
    class TooLongLines {

        @DisplayName("With only short lines")
        @Test
        public void should_return_void_if_lines_under_limit() {
            String code = "public void bonjour() {\n" +
                    "            if (0 == 1) {\n" +
                    "                System.out.println(\"C'est faux\");\n" +
                    "            } else {\n" +
                    "                System.out.println(\"C'est pas faux\");\n" +
                    "            }\n" +
                    "            System.out.println(\"bbbbbbbbbbbbbbbbbbbbbiuerqhbcismherkvldsiufvhesmggggggggg\");\n" +
                    "        }";

            assertEquals(new HashSet<>(), sut.findTooLongLines(code));
        }

        @DisplayName("With too long lines")
        @Test
        public void should_return_errors_if_lines_above_limit() {
            String code = "public void bonjour() {\n" +
                    "            if (0 == 1) {\n" +
                    "                System.out.println(\"C'est faux\");\n" +
                    "            } else {\n" +
                    "                System.out.println(\"C'est pas faux\");\n" +
                    "            }\n" +
                    "            List<String> list = new ArrayList<>();\n" +
                    "            list.stream().filter(li -> li.startsWith(\"bonjour\")).map(li -> \"yo\").collect(Collectors.toList());\n" +
                    "            list.stream().filter(li -> li.startsWith(\"bonjour\"))\n" +
                    "                    .map(li -> \"yo\").collect(Collectors.toList());\n" +
                    "\n" +
                    "\n" +
                    "            if(true) System.out.println(\"ok\"); else if (false) System.out.println(\"ko\"); else System.out.println(\"\");\n" +
                    "            System.out.println(\"bbbbbbbbbbbbbbbbbbbbbiuerqhbcismherkvldsiufvhesmggggggggg\");\n" +
                    "        }";
            Set<LinterError> linterErrors = new HashSet<>();

            linterErrors.add(new LinterError()
                    .setErrorNumber(1)
                    .setErrorMessage(String.format("Line %s contains more than %s characters (%s)",
                            8, JAVA_MAX_LINE_CHARACTERS, 98)));

            linterErrors.add(new LinterError()
                    .setErrorNumber(2)
                    .setErrorMessage(String.format("Line %s contains more than %s characters (%s)",
                            13, JAVA_MAX_LINE_CHARACTERS, 105)));

            assertEquals(linterErrors, sut.findTooLongLines(code));
        }

        @DisplayName("With twice too long lines")
        @Test
        public void should_add_errors_every_20_lines_above_limit() {
            String code = "public void bonjour() {\n" +
                    "            if (0 == 1) {\n" +
                    "                System.out.println(\"C'est faux\");\n" +
                    "            } else {\n" +
                    "                System.out.println(\"C'est pas faux\");\n" +
                    "            }\n" +
                    "            List<String> list = new ArrayList<>();\n" +
                    "            list.stream().filter(li -> li.startsWith(\"bonjour\")).map(li -> \"yo\").map(li -> \"ya\").map(li -> \"heo\").collect(Collectors.toList());\n" +
                    "            list.stream().filter(li -> li.startsWith(\"bonjour\"))\n" +
                    "                    .map(li -> \"yo\").collect(Collectors.toList());\n" +
                    "\n" +
                    "\n" +
                    "            if(true) System.out.println(\"ok\"); else if (false) System.out.println(\"ko\"); else System.out.println(\"\");\n" +
                    "            System.out.println(\"bbbbbbbbbbbbbbbbbbbbbiuerqhbcismherkvldsiufvhesmggggggggg\");\n" +
                    "        }";
            Set<LinterError> linterErrors = new HashSet<>();

            linterErrors.add(new LinterError()
                    .setErrorNumber(3)
                    .setErrorMessage(String.format("Line %s contains more than %s characters (%s)",
                            8, JAVA_MAX_LINE_CHARACTERS, 131)));

            linterErrors.add(new LinterError()
                    .setErrorNumber(2)
                    .setErrorMessage(String.format("Line %s contains more than %s characters (%s)",
                            13, JAVA_MAX_LINE_CHARACTERS, 105)));

            assertEquals(linterErrors, sut.findTooLongLines(code));
        }
    }
}
