package fr.esgi.devwars.algorithm_case.use_case;

import fr.esgi.devwars.algorithm_case.domain.dao.AlgorithmCaseDAO;
import fr.esgi.devwars.algorithm.domain.dao.AlgorithmDAO;
import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DeleteAlgorithmCaseById {

    private final AlgorithmCaseDAO algorithmCaseDAO;
    private final AlgorithmDAO algorithmDAO;

    public void execute(Long algoId, Long id) {
        checkIfAlgoExists(algoId);
        checkIfExists(id);
        algorithmCaseDAO.delete(id);
    }

    private void checkIfExists(Long id) {
        var algoCase = algorithmCaseDAO.findById(id);
        if (algoCase == null) {
            throw new NotFoundException(String.format("AlgoCase with id %s not found", id));
        }
    }

    private void checkIfAlgoExists(Long id) {
        Algorithm exist = algorithmDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("Algo with id %s not found", id));
        }
    }
}
