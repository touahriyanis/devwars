package fr.esgi.devwars.algorithm_case.use_case;

import fr.esgi.devwars.algorithm_case.domain.dao.AlgorithmCaseDAO;
import fr.esgi.devwars.algorithm_case.domain.model.AlgorithmCase;
import fr.esgi.devwars.algorithm.domain.dao.AlgorithmDAO;
import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FindAllAlgorithmCasesByAlgo {

    private final AlgorithmCaseDAO algorithmCaseDAO;
    private final AlgorithmDAO algorithmDAO;

    public List<AlgorithmCase> execute(Long algoId) {
        checkIfAlgoExists(algoId);

        return algorithmCaseDAO.findAllByAlgo(algoId);
    }

    private void checkIfAlgoExists(Long id) {
        Algorithm exist = algorithmDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("Algo with id %s not found", id));
        }
    }
}
