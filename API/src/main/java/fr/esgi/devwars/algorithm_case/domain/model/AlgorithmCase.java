package fr.esgi.devwars.algorithm_case.domain.model;

import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.case_input.domain.model.CaseInput;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;


@Accessors(chain = true)
@Data
public class AlgorithmCase {
    private Long id;
    private String name;
    private List<CaseInput> input;
    private String outputType;
    private String expectedOutput;
    private Algorithm algorithm;
}
