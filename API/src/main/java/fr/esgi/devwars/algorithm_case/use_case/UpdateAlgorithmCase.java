package fr.esgi.devwars.algorithm_case.use_case;

import fr.esgi.devwars.algorithm_case.domain.dao.AlgorithmCaseDAO;
import fr.esgi.devwars.algorithm_case.domain.model.AlgorithmCase;
import fr.esgi.devwars.algorithm_case.infrastructure.web.request.AlgorithmCaseRequest;
import fr.esgi.devwars.algorithm.domain.dao.AlgorithmDAO;
import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.common.domain.exception.AlreadyExistsException;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UpdateAlgorithmCase {

    private final AlgorithmCaseDAO algorithmCaseDAO;
    private final AlgorithmDAO algorithmDAO;

    public AlgorithmCase execute(Long algoId, Long id, AlgorithmCaseRequest request) {
        checkIfExists(id);
        Algorithm algorithm = checkIfAlgoExists(algoId);
        checkIfNameAlreadyExists(request.getName(), algoId, id);

        AlgorithmCase algorithmCase = new AlgorithmCase()
                .setId(id)
                .setName(request.getName())
                .setOutputType(request.getOutputType())
                .setExpectedOutput(request.getExpectedOutput())
                .setAlgorithm(algorithm);

        algorithmCase = algorithmCaseDAO.update(algorithmCase);
        algorithmCase.setAlgorithm(algorithm);
        return algorithmCase;
    }

    private void checkIfExists(Long id) {
        var algoCase = algorithmCaseDAO.findById(id);
        if (algoCase == null) {
            throw new NotFoundException(String.format("AlgoCase with id %s not found", id));
        }
    }

    private Algorithm checkIfAlgoExists(Long id) {
        Algorithm exist = algorithmDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("Algo with id %s not found", id));
        }
        return exist;
    }

    private void checkIfNameAlreadyExists(String name, Long algoId, Long id) {
        AlgorithmCase exist = algorithmCaseDAO.findByName(algoId, name);
        if (exist != null && !exist.getId().equals(id)) {
            throw new AlreadyExistsException(String.format("AlgoCase with name %s already exists for algo with id %s", name, algoId));
        }
    }
}
