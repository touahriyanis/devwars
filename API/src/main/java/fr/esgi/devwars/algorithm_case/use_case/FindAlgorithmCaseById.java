package fr.esgi.devwars.algorithm_case.use_case;

import fr.esgi.devwars.algorithm_case.domain.dao.AlgorithmCaseDAO;
import fr.esgi.devwars.algorithm_case.domain.model.AlgorithmCase;
import fr.esgi.devwars.algorithm.domain.dao.AlgorithmDAO;
import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FindAlgorithmCaseById {

    private final AlgorithmCaseDAO algorithmCaseDAO;
    private final AlgorithmDAO algorithmDAO;

    public AlgorithmCase execute(Long algoId, Long id) {
        Algorithm algorithm = checkIfAlgoExists(algoId);

        var algoCase = algorithmCaseDAO.findById(id);
        if (algoCase == null) {
            throw new NotFoundException(String.format("AlgoCase with id %s not found", id));
        }
        algoCase.setAlgorithm(algorithm);
        return algoCase;
    }

    private Algorithm checkIfAlgoExists(Long id) {
        Algorithm exist = algorithmDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("Algo with id %s not found", id));
        }
        return exist;
    }
}
