package fr.esgi.devwars.case_input.use_case;

import fr.esgi.devwars.algorithm_case.domain.dao.AlgorithmCaseDAO;
import fr.esgi.devwars.algorithm_case.domain.model.AlgorithmCase;
import fr.esgi.devwars.case_input.domain.dao.CaseInputDAO;
import fr.esgi.devwars.case_input.domain.model.CaseInput;
import fr.esgi.devwars.case_input.infrastructure.web.request.CaseInputRequest;
import fr.esgi.devwars.algorithm.domain.dao.AlgorithmDAO;
import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.common.domain.exception.AlreadyExistsException;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CreateCaseInput {

    private final CaseInputDAO caseInputDAO;
    private final AlgorithmCaseDAO algorithmCaseDAO;

    public CaseInput execute(Long caseId, CaseInputRequest request) {
        AlgorithmCase algorithmCase = checkIfCaseExists(caseId);

        CaseInput caseInput = new CaseInput()
                .setValue(request.getValue())
                .setAlgorithmCase(algorithmCase);

        caseInput = caseInputDAO.create(caseInput);
        caseInput.setAlgorithmCase(algorithmCase);
        return caseInput;
    }

    private AlgorithmCase checkIfCaseExists(Long id) {
        AlgorithmCase exist = algorithmCaseDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("Case with id %s not found", id));
        }
        return exist;
    }
}
