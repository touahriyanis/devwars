package fr.esgi.devwars.case_input.domain.dao;

import fr.esgi.devwars.case_input.domain.model.CaseInput;

import java.util.List;

public interface CaseInputDAO {
    CaseInput create(CaseInput caseInput);

    CaseInput findById(Long id);

    List<CaseInput> findAllByCase(Long caseId);

    CaseInput update(CaseInput caseInput);

    void delete(Long id);
}
