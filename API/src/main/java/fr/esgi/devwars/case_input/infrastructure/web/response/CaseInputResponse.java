package fr.esgi.devwars.case_input.infrastructure.web.response;

import fr.esgi.devwars.algorithm_case.domain.model.AlgorithmCase;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Accessors(chain = true)
@Data
@RequiredArgsConstructor
public class CaseInputResponse implements Serializable {
    private Long id;
    private String value;
    private AlgorithmCase algorithmCase;
}
