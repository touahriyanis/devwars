package fr.esgi.devwars.case_input.use_case;

import fr.esgi.devwars.algorithm_case.domain.dao.AlgorithmCaseDAO;
import fr.esgi.devwars.algorithm_case.domain.model.AlgorithmCase;
import fr.esgi.devwars.case_input.domain.dao.CaseInputDAO;
import fr.esgi.devwars.case_input.domain.model.CaseInput;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FindAllCaseInputsByCase {

    private final CaseInputDAO caseInputDAO;
    private final AlgorithmCaseDAO algorithmCaseDAO;

    public List<CaseInput> execute(Long caseId) {
        checkIfCaseExists(caseId);
        return caseInputDAO.findAllByCase(caseId);
    }

    private void checkIfCaseExists(Long id) {
        AlgorithmCase exist = algorithmCaseDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("Case with id %s not found", id));
        }
    }
}
