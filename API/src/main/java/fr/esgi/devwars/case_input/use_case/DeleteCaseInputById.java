package fr.esgi.devwars.case_input.use_case;

import fr.esgi.devwars.algorithm_case.domain.dao.AlgorithmCaseDAO;
import fr.esgi.devwars.algorithm_case.domain.model.AlgorithmCase;
import fr.esgi.devwars.case_input.domain.dao.CaseInputDAO;
import fr.esgi.devwars.algorithm.domain.dao.AlgorithmDAO;
import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DeleteCaseInputById {

    private final CaseInputDAO caseInputDAO;
    private final AlgorithmCaseDAO algorithmCaseDAO;

    public void execute(Long algoId, Long id) {
        checkIfCaseExists(algoId);
        checkIfExists(id);
        caseInputDAO.delete(id);
    }

    private void checkIfExists(Long id) {
        var caseInput = caseInputDAO.findById(id);
        if (caseInput == null) {
            throw new NotFoundException(String.format("Input with id %s not found", id));
        }
    }

    private void checkIfCaseExists(Long id) {
        AlgorithmCase exist = algorithmCaseDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("Case with id %s not found", id));
        }
    }
}
