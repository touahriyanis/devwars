package fr.esgi.devwars.case_input.infrastructure.dataprovider.repository;

import fr.esgi.devwars.case_input.infrastructure.dataprovider.entity.CaseInputEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CaseInputRepository extends JpaRepository<CaseInputEntity, Long> {
    List<CaseInputEntity> findAllByAlgorithmCaseId(Long algoId);
}
