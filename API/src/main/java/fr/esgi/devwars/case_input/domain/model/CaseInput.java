package fr.esgi.devwars.case_input.domain.model;

import fr.esgi.devwars.algorithm_case.domain.model.AlgorithmCase;
import lombok.Data;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@Data
public class CaseInput {
    private Long id;
    private String value;
    private AlgorithmCase algorithmCase;
}
