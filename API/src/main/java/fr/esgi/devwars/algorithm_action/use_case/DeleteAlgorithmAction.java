package fr.esgi.devwars.algorithm_action.use_case;

import fr.esgi.devwars.algorithm_action.domain.dao.AlgorithmActionDAO;
import fr.esgi.devwars.algorithm.domain.dao.AlgorithmDAO;
import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.algorithm_action.domain.model.AlgorithmAction;
import fr.esgi.devwars.common.domain.exception.AlreadyExistsException;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import fr.esgi.devwars.action.domain.dao.ActionDAO;
import fr.esgi.devwars.action.domain.model.Action;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DeleteAlgorithmAction {

    private final AlgorithmActionDAO algorithmActionDAO;
    private final AlgorithmDAO algorithmDAO;
    private final ActionDAO actionDAO;

    public void execute(Long algoId, Long actionId) {
        checkIfAlgoExists(algoId);
        checkIfActionExists(actionId);
        checkIfAlgoActionExists(algoId, actionId);
        algorithmActionDAO.delete(algoId, actionId);
    }

    private void checkIfAlgoExists(Long id) {
        Algorithm exist = algorithmDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("Algo with id %s not found", id));
        }
    }

    private void checkIfActionExists(Long id) {
        Action exist = actionDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("Action with id %s not found", id));
        }
    }

    private void checkIfAlgoActionExists(Long algoId, Long actionId) {
        AlgorithmAction exist = algorithmActionDAO.findByIds(algoId, actionId);
        if (exist == null) {
            throw new NotFoundException(String.format("No relation between algorithm %s and action %s", algoId, actionId));
        }
    }
}
