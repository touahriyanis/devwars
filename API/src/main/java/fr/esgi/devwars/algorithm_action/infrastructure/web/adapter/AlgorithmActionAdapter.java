package fr.esgi.devwars.algorithm_action.infrastructure.web.adapter;

import fr.esgi.devwars.algorithm_action.domain.model.AlgorithmAction;
import fr.esgi.devwars.algorithm_action.infrastructure.web.response.AlgorithmActionResponse;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class AlgorithmActionAdapter {
    public AlgorithmActionResponse toResponse(AlgorithmAction algorithmAction) {
        return new AlgorithmActionResponse()
                .setAlgorithm(algorithmAction.getAlgorithm())
                .setAction(algorithmAction.getAction());
    }

    public List<AlgorithmActionResponse> toResponses(List<AlgorithmAction> algorithmActions) {
        return algorithmActions
                .stream()
                .map(this::toResponse)
                .collect(Collectors.toList());
    }


}
