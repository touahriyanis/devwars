package fr.esgi.devwars.algorithm_action.domain.dao;

import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.algorithm_action.domain.model.AlgorithmAction;
import fr.esgi.devwars.action.domain.model.Action;

import java.util.List;

public interface AlgorithmActionDAO {
    void create(AlgorithmAction algorithmAction);

    void delete(Long algoId, Long actionId);

    AlgorithmAction findByIds(Long algoId, Long actionId);

    List<Action> findActionsByAlgo(Long algoId);

    List<Algorithm> findAlgosByAction(Long actionId);
}
