package fr.esgi.devwars.algorithm_action.domain.model;

import fr.esgi.devwars.action.domain.model.Action;
import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import lombok.Data;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@Data
public class AlgorithmAction {

    private Algorithm algorithm;
    private Action action;
}
