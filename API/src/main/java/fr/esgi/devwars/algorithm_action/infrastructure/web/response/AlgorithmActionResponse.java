package fr.esgi.devwars.algorithm_action.infrastructure.web.response;

import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.action.domain.model.Action;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Accessors(chain = true)
@Data
@RequiredArgsConstructor
public class AlgorithmActionResponse implements Serializable {
    private Algorithm algorithm;
    private Action action;
}
