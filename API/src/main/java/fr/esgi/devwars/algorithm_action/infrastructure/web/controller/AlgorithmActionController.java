package fr.esgi.devwars.algorithm_action.infrastructure.web.controller;

import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.algorithm.infrastructure.web.adapter.AlgorithmAdapter;
import fr.esgi.devwars.algorithm.infrastructure.web.response.AlgorithmResponse;
import fr.esgi.devwars.algorithm_action.domain.model.AlgorithmAction;
import fr.esgi.devwars.algorithm_action.infrastructure.web.adapter.AlgorithmActionAdapter;
import fr.esgi.devwars.algorithm_action.infrastructure.web.response.AlgorithmActionResponse;
import fr.esgi.devwars.algorithm_action.use_case.*;
import fr.esgi.devwars.action.domain.model.Action;
import fr.esgi.devwars.action.infrastructure.web.adapter.ActionAdapter;
import fr.esgi.devwars.action.infrastructure.web.response.ActionResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;

@RestController
@Validated
@RequestMapping("/api")
@RequiredArgsConstructor
public class AlgorithmActionController {

    private final AlgorithmActionAdapter algorithmActionAdapter;
    private final AlgorithmAdapter algorithmAdapter;
    private final ActionAdapter actionAdapter;
    private final AddAlgorithmAction addAlgorithmAction;
    private final DeleteAlgorithmAction deleteAlgorithmAction;
    private final FindAllAlgorithmsByAction findAllAlgorithmsByAction;
    private final FindAllActionsByAlgorithm findAllActionsByAlgorithm;

    @PutMapping("/algorithms/{algoId}/actions/{actionId}")
    public ResponseEntity<AlgorithmActionResponse> addActionToAlgo(
            @PathVariable("algoId") @NotNull @Positive Long algoId,
            @PathVariable("actionId") @NotNull @Positive Long actionId) {

        AlgorithmAction algorithmAction = addAlgorithmAction.execute(algoId, actionId);

        return ResponseEntity
                .ok()
                .body(algorithmActionAdapter.toResponse(algorithmAction));
    }

    @PutMapping("/actions/{actionId}/algorithms/{algoId}")
    public ResponseEntity<AlgorithmActionResponse> addAlgoToAction(
            @PathVariable("algoId") @NotNull @Positive Long algoId,
            @PathVariable("actionId") @NotNull @Positive Long actionId) {

        AlgorithmAction algorithmAction = addAlgorithmAction.execute(algoId, actionId);

        return ResponseEntity
                .ok()
                .body(algorithmActionAdapter.toResponse(algorithmAction));
    }

    @DeleteMapping("/algorithms/{algoId}/actions/{actionId}")
    public ResponseEntity<AlgorithmActionResponse> deleteActionFromAlgo(
            @PathVariable("algoId") @NotNull @Positive Long algoId,
            @PathVariable("actionId") @NotNull @Positive Long actionId) {

        deleteAlgorithmAction.execute(algoId, actionId);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/actions/{actionId}/algorithms/{algoId}")
    public ResponseEntity<AlgorithmActionResponse> deleteAlgoFromAction(
            @PathVariable("algoId") @NotNull @Positive Long algoId,
            @PathVariable("actionId") @NotNull @Positive Long actionId) {

        deleteAlgorithmAction.execute(algoId, actionId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/actions/{actionId}/algorithms")
    public ResponseEntity<List<AlgorithmResponse>> findAllAlgosByAction(
            @PathVariable("actionId") @NotNull @Positive Long actionId) {

        List<Algorithm> algorithms = findAllAlgorithmsByAction.execute(actionId);
        return ResponseEntity.ok(algorithmAdapter.toResponses(algorithms));
    }

    @GetMapping("/algorithms/{algoId}/actions")
    public ResponseEntity<List<ActionResponse>> findAllActionsByAlgo(
            @PathVariable("algoId") @NotNull @Positive Long algoId) {

        List<Action> actions = findAllActionsByAlgorithm.execute(algoId);
        return ResponseEntity.ok(actionAdapter.toResponses(actions));
    }


}
