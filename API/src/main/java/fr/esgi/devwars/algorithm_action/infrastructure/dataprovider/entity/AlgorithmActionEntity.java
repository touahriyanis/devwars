package fr.esgi.devwars.algorithm_action.infrastructure.dataprovider.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.io.Serializable;

@Entity(name = "algo_action")
@Accessors(chain = true)
@Data
@IdClass(AlgorithmActionId.class)
public class AlgorithmActionEntity implements Serializable {
    @Id
    @Column(name = "algo_id")
    private Long algorithmId;

    @Id
    @Column(name = "action_id")
    private Long actionId;
}
