package fr.esgi.devwars.algorithm_action.use_case;

import fr.esgi.devwars.algorithm.domain.dao.AlgorithmDAO;
import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.algorithm_action.domain.dao.AlgorithmActionDAO;
import fr.esgi.devwars.algorithm_action.domain.model.AlgorithmAction;
import fr.esgi.devwars.common.domain.exception.AlreadyExistsException;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import fr.esgi.devwars.action.domain.dao.ActionDAO;
import fr.esgi.devwars.action.domain.model.Action;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AddAlgorithmAction {

    private final AlgorithmActionDAO algorithmActionDAO;
    private final AlgorithmDAO algorithmDAO;
    private final ActionDAO actionDAO;

    public AlgorithmAction execute(Long algoId, Long actionId) {
        Algorithm algorithm = checkIfAlgoExists(algoId);
        Action action = checkIfActionExists(actionId);
        checkIfAlgoActionAlreadyExists(algoId, actionId);

        var algorithmAction = new AlgorithmAction()
                .setAlgorithm(algorithm)
                .setAction(action);

        algorithmActionDAO.create(algorithmAction);
        return algorithmAction;
    }

    private Algorithm checkIfAlgoExists(Long id) {
        Algorithm exist = algorithmDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("Algo with id %s not found", id));
        }
        return exist;
    }

    private Action checkIfActionExists(Long id) {
        Action exist = actionDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("Action with id %s not found", id));
        }
        return exist;
    }

    private void checkIfAlgoActionAlreadyExists(Long algoId, Long actionId) {
        AlgorithmAction exist = algorithmActionDAO.findByIds(algoId, actionId);
        if (exist != null) {
            throw new AlreadyExistsException(String.format("Relation already exists between algorithm %s and action %s", algoId, actionId));
        }
    }
}
