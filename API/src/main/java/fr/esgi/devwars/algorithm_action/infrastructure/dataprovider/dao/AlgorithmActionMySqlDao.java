package fr.esgi.devwars.algorithm_action.infrastructure.dataprovider.dao;


import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.algorithm.infrastructure.dataprovider.mapper.AlgorithmMapper;
import fr.esgi.devwars.algorithm.infrastructure.dataprovider.repository.AlgorithmRepository;
import fr.esgi.devwars.algorithm_action.domain.dao.AlgorithmActionDAO;
import fr.esgi.devwars.algorithm_action.domain.model.AlgorithmAction;
import fr.esgi.devwars.algorithm_action.infrastructure.dataprovider.entity.AlgorithmActionEntity;
import fr.esgi.devwars.algorithm_action.infrastructure.dataprovider.mapper.AlgorithmActionMapper;
import fr.esgi.devwars.algorithm_action.infrastructure.dataprovider.repository.AlgorithmActionRepository;
import fr.esgi.devwars.action.domain.model.Action;
import fr.esgi.devwars.action.infrastructure.dataprovider.mapper.ActionMapper;
import fr.esgi.devwars.action.infrastructure.dataprovider.repository.ActionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AlgorithmActionMySqlDao implements AlgorithmActionDAO {

    private final AlgorithmActionRepository algorithmActionRepository;
    private final AlgorithmRepository algorithmRepository;
    private final ActionRepository actionRepository;
    private final AlgorithmActionMapper algorithmActionMapper;
    private final AlgorithmMapper algorithmMapper;
    private final ActionMapper actionMapper;

    @Override
    public void create(AlgorithmAction algorithmAction) {
        AlgorithmActionEntity entity = algorithmActionMapper.toEntity(algorithmAction);
        algorithmActionRepository.save(entity);
    }

    @Override
    public void delete(Long algoId, Long actionId) {
        algorithmActionRepository.findByAlgorithmIdAndActionId(algoId, actionId)
                .ifPresent(algorithmActionRepository::delete);
    }

    @Override
    public AlgorithmAction findByIds(Long algoId, Long actionId) {
        return algorithmActionRepository.findByAlgorithmIdAndActionId(algoId, actionId)
                .map(algorithmActionMapper::toDomain)
                .orElse(null);
    }

    @Override
    public List<Action> findActionsByAlgo(Long algoId) {
        var actionIds = algorithmActionRepository.findByAlgorithmId(algoId)
                .stream().map(AlgorithmActionEntity::getActionId)
                .collect(Collectors.toList());
        return actionRepository.findAllById(actionIds)
                .stream()
                .map(actionMapper::toDomain)
                .collect(Collectors.toList());
    }

    @Override
    public List<Algorithm> findAlgosByAction(Long actionId) {
        var algoIds = algorithmActionRepository.findByActionId(actionId)
                .stream().map(AlgorithmActionEntity::getAlgorithmId)
                .collect(Collectors.toList());
        return algorithmRepository.findAllById(algoIds)
                .stream()
                .map(algorithmMapper::toDomain)
                .collect(Collectors.toList());
    }
}
