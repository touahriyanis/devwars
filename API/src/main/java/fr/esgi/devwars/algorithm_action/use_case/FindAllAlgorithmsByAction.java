package fr.esgi.devwars.algorithm_action.use_case;

import fr.esgi.devwars.algorithm_action.domain.dao.AlgorithmActionDAO;
import fr.esgi.devwars.algorithm_action.domain.model.AlgorithmAction;
import fr.esgi.devwars.algorithm.domain.dao.AlgorithmDAO;
import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import fr.esgi.devwars.action.domain.dao.ActionDAO;
import fr.esgi.devwars.action.domain.model.Action;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FindAllAlgorithmsByAction {

    private final AlgorithmActionDAO algorithmActionDAO;
    private final ActionDAO actionDAO;

    public List<Algorithm> execute(Long actionId) {
        checkIfActionExists(actionId);
        return algorithmActionDAO.findAlgosByAction(actionId);
    }

    private void checkIfActionExists(Long id) {
        Action exist = actionDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("Action with id %s not found", id));
        }
    }

}
