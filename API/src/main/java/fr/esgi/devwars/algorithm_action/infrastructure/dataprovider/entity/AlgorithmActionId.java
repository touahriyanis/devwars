package fr.esgi.devwars.algorithm_action.infrastructure.dataprovider.entity;

import java.io.Serializable;

public class AlgorithmActionId implements Serializable {
    private Long algorithmId;
    private Long actionId;
}
