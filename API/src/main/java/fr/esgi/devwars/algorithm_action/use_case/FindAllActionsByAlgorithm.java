package fr.esgi.devwars.algorithm_action.use_case;

import fr.esgi.devwars.algorithm.domain.dao.AlgorithmDAO;
import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.algorithm_action.domain.dao.AlgorithmActionDAO;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import fr.esgi.devwars.action.domain.model.Action;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FindAllActionsByAlgorithm {

    private final AlgorithmActionDAO algorithmActionDAO;
    private final AlgorithmDAO algorithmDAO;

    public List<Action> execute(Long algoId) {
        checkIfAlgoExists(algoId);
        return algorithmActionDAO.findActionsByAlgo(algoId);
    }

    private void checkIfAlgoExists(Long id) {
        Algorithm exist = algorithmDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("Algo with id %s not found", id));
        }
    }
}
