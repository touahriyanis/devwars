package fr.esgi.devwars.algorithm_action.infrastructure.dataprovider.mapper;

import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.algorithm_action.domain.model.AlgorithmAction;
import fr.esgi.devwars.algorithm_action.infrastructure.dataprovider.entity.AlgorithmActionEntity;
import fr.esgi.devwars.action.domain.model.Action;
import org.springframework.stereotype.Component;

@Component
public class AlgorithmActionMapper {
    public AlgorithmActionEntity toEntity(AlgorithmAction algorithmAction) {
        return new AlgorithmActionEntity()
                .setAlgorithmId(algorithmAction.getAlgorithm().getId())
                .setActionId(algorithmAction.getAction().getId());
    }

    public AlgorithmAction toDomain(AlgorithmActionEntity entity) {
        return new AlgorithmAction()
                .setAlgorithm(new Algorithm().setId(entity.getAlgorithmId()))
                .setAction(new Action().setId(entity.getActionId()));
    }
}
