package fr.esgi.devwars.algorithm_action.infrastructure.dataprovider.repository;


import fr.esgi.devwars.algorithm_action.infrastructure.dataprovider.entity.AlgorithmActionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AlgorithmActionRepository extends JpaRepository<AlgorithmActionEntity, Long> {
    Optional<AlgorithmActionEntity> findByAlgorithmIdAndActionId(Long algorithmId, Long actionId);

    List<AlgorithmActionEntity> findByActionId(Long actionId);

    List<AlgorithmActionEntity> findByAlgorithmId(Long algorithmId);
}
