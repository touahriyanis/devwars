package fr.esgi.devwars.compiler.domain.model;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class LinterError {
    private Integer errorNumber;
    private String errorMessage;
}
