package fr.esgi.devwars.compiler.use_case;

import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.compiler.domain.dao.CompilerDAO;
import fr.esgi.devwars.compiler.domain.model.ExecutionOutput;
import fr.esgi.devwars.compiler.domain.model.Language;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CompileCodeImpl implements CompileCode{

    private final CompilerDAO compilerDAO;

    @Override
    public ExecutionOutput execute(Long algoId, String sourceCode, Language language) throws IOException, InterruptedException {
        return compilerDAO.compile(algoId, sourceCode, language);
    }
}
