package fr.esgi.devwars.compiler.infrastructure.utils.interfaces;

import java.io.IOException;

public interface DockerClient {
    Process buildImage(String folder, String imageName) throws InterruptedException, IOException;
    Process runImage(String imageName, String fileName) throws IOException, InterruptedException;
}
