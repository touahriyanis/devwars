package fr.esgi.devwars.compiler.infrastructure.utils.interfaces;

import fr.esgi.devwars.compiler.domain.model.Language;

public interface ExecutableScript {
    void generateExecutionScript(Language language, int timeLimit, int memoryLimit);
}
