package fr.esgi.devwars.compiler.infrastructure.utils;

import fr.esgi.devwars.compiler.infrastructure.dataprovider.dao.CodeCompilerDAO;
import fr.esgi.devwars.compiler.infrastructure.utils.interfaces.DockerClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

@Service
public class DockerManager implements DockerClient {

    Logger logger = LogManager.getLogger(DockerManager.class);

    public Process buildImage(String folder, String imageName) throws InterruptedException, IOException {
        String[] dockerCommand = new String[] {"docker", "image", "build", folder, "-t", imageName};
        ProcessBuilder processbuilder = new ProcessBuilder(dockerCommand);
        Process process = processbuilder.start();
        var errorOutput = getOutputAsString(process.getErrorStream());
        logger.info(errorOutput);
        return process;
    }

    public Process runImage(String imageName, String fileName) throws IOException, InterruptedException {
        String[] dockerCommand = new String[] {"docker", "run", "--rm", imageName, fileName};
        ProcessBuilder processbuilder = new ProcessBuilder(dockerCommand);
        return processbuilder.start();
    }

    public String getOutputAsString(InputStream stream) throws IOException {
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(stream));
        StringBuilder builder = new StringBuilder();
        String line = null;
        while ( (line = reader.readLine()) != null) {
            builder.append(line);
            builder.append(System.getProperty("line.separator"));
        }
        String result = builder.toString();
        return result;
    }
}
