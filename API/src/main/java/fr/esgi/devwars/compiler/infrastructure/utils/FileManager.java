package fr.esgi.devwars.compiler.infrastructure.utils;

import fr.esgi.devwars.compiler.domain.model.Language;
import fr.esgi.devwars.compiler.infrastructure.utils.interfaces.ManageableFile;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;

@Service
public class FileManager implements ManageableFile {

    public void convertToFileAndUpload(String code, String name) throws IOException {
        byte[] bytes = code.getBytes();
        Path path = Paths.get(name);
        Files.write(path, bytes);
    }

    public boolean deleteFile(String folder, String file) {
        if(folder != null && file != null) {
            String fileName = folder + "/" + file;
            new File(fileName).delete();
            return true;
        }
        return false;
    }

    public String getFolderName(Language language) throws IOException {

        String folderName = "execution_environments/env";
        switch(language) {
            case Java:
                folderName += "_java";
                break;
            case Cpp:
                folderName += "_cpp";
                break;
            case Python:
                folderName += "_python";
                break;
            default:
                break;
        }
        /*String uuid = generateRandomUUID();
        folderName += "/"+uuid;

        if (!Files.exists(Paths.get(folderName))) {
            System.out.println("heeeyE");
            Files.createDirectories(Paths.get(folderName));
        }*/

        return folderName;
    }

    public String getFileName(Language language, int index) {
        String fileName = "main"+index;
        switch(language) {
            case Java:
                fileName += ".java";
                break;
            case Cpp:
                fileName += ".cpp";
                break;
            case Python:
                fileName += ".py";
                break;
            default:
                break;
        }
        return fileName;
    }

    public String getOutputAsString(InputStream stream) throws IOException {
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(stream));
        StringBuilder builder = new StringBuilder();
        String line = null;
        while ( (line = reader.readLine()) != null) {
            builder.append(line);
            builder.append(System.getProperty("line.separator"));
        }
        String result = builder.toString();
        return result;
    }

    public String generateRandomUUID() {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

        return generatedString;
    }

}
