package fr.esgi.devwars.compiler.use_case;


import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.compiler.domain.model.ExecutionOutput;
import fr.esgi.devwars.compiler.domain.model.Language;

import java.io.IOException;

public interface CompileCode {
    ExecutionOutput execute(Long algoId, String sourceCode, Language language) throws IOException, InterruptedException;
}
