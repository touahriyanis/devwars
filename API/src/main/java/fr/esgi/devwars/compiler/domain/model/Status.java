package fr.esgi.devwars.compiler.domain.model;

public enum Status {
    CompilationError,
    RuntimeError,
    OutOfMemory,
    TimeLimitExcess,
    Success
}