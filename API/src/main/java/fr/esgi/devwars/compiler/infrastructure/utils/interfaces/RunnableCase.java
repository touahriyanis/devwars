package fr.esgi.devwars.compiler.infrastructure.utils.interfaces;

import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.compiler.domain.exception.CompilationException;
import fr.esgi.devwars.compiler.domain.exception.ContainerExecutionException;
import fr.esgi.devwars.compiler.domain.exception.DockerBuildException;
import fr.esgi.devwars.algorithm_case.domain.model.AlgorithmCase;
import fr.esgi.devwars.compiler.domain.model.CaseExecutionOutput;
import fr.esgi.devwars.compiler.domain.model.Language;
import org.apache.tomcat.util.http.fileupload.FileUploadException;

import java.io.IOException;
import java.util.List;

public interface RunnableCase {
    List<CaseExecutionOutput> runAndVerifyCases(Algorithm algo, String sourceCode, Language language) throws IOException, CompilationException, DockerBuildException, ContainerExecutionException;
}
