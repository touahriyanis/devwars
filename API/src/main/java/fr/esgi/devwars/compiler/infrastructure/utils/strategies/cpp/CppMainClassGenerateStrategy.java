package fr.esgi.devwars.compiler.infrastructure.utils.strategies.cpp;

import fr.esgi.devwars.algorithm_case.domain.model.AlgorithmCase;
import fr.esgi.devwars.compiler.domain.strategies.MainClassGenerateStrategy;
import fr.esgi.devwars.compiler.infrastructure.utils.constants.MainClasses;

public class CppMainClassGenerateStrategy implements MainClassGenerateStrategy {

    @Override
    public String generateMainClass(AlgorithmCase runCase, String mainFileIndex, String funcName, String sourceCode) {
        return String.format(MainClasses.CPP, sourceCode, generateCasesFunctions(runCase, funcName));
    }

    private String generateCasesFunctions(AlgorithmCase runCase, String funcName) {
        String result = "";

        runCase.setOutputType(runCase.getOutputType().toLowerCase());

        StringBuilder line = new StringBuilder(runCase.getOutputType() + " runCase = " + funcName + "(");
        for (int i = 0; i < runCase.getInput().size(); i++) {
            if (i == 0) {
                line.append(runCase.getInput().get(i).getValue());
            } else {
                line.append(",").append(runCase.getInput().get(i).getValue());
            }
        }
        line.append(");\n\t");
        line.append("cout << runCase;\n\t");
        result += line.toString();
        return result;
    }
}
