package fr.esgi.devwars.compiler.use_case;

import fr.esgi.devwars.compiler.domain.exception.CompilationException;
import fr.esgi.devwars.compiler.domain.factory.AnalyzeCodeStrategyFactory;
import fr.esgi.devwars.compiler.domain.model.Language;
import fr.esgi.devwars.compiler.domain.model.LinterError;
import fr.esgi.devwars.compiler.domain.strategies.AnalyzeCodeStrategy;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AnalyzeCode {

    private final AnalyzeCodeStrategyFactory analyzeCodeStrategyFactory;

    public List<LinterError> execute(String sourceCode, Language language) {
        if (sourceCode == null || sourceCode.isBlank()) {
            throw new CompilationException("No source code");
        }
        AnalyzeCodeStrategy analyzeCodeStrategy = analyzeCodeStrategyFactory
                .getStrategy(language);

        List<LinterError> linterErrors = new ArrayList<>();

        linterErrors.addAll(analyzeCodeStrategy
                .findDuplicatedLines(sourceCode)
        );

        linterErrors.addAll(analyzeCodeStrategy
                .findTooLargeMethods(sourceCode)
        );

        linterErrors.addAll(analyzeCodeStrategy
                .findTooLongLines(sourceCode)
        );

        return linterErrors;
    }

}
