package fr.esgi.devwars.compiler.infrastructure.web.request;

import fr.esgi.devwars.compiler.domain.model.Language;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CompilerRequest {
    Long algorithmId;
    String sourceCode;
    Language language;
}


