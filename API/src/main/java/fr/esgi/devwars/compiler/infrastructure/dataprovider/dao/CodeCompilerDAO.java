package fr.esgi.devwars.compiler.infrastructure.dataprovider.dao;

import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.algorithm.use_case.FindAlgorithmById;
import fr.esgi.devwars.algorithm_case.use_case.FindAllAlgorithmCasesByAlgo;
import fr.esgi.devwars.case_input.use_case.FindAllCaseInputsByCase;
import fr.esgi.devwars.compiler.domain.dao.CompilerDAO;
import fr.esgi.devwars.compiler.domain.exception.CompilationException;
import fr.esgi.devwars.compiler.domain.exception.ContainerExecutionException;
import fr.esgi.devwars.compiler.domain.exception.DockerBuildException;
import fr.esgi.devwars.compiler.domain.model.*;
import fr.esgi.devwars.compiler.infrastructure.utils.*;
import fr.esgi.devwars.compiler.infrastructure.utils.interfaces.RunnableCase;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CodeCompilerDAO implements CompilerDAO {

    private final RunnableCase codeRunner;
    private final ExecutionScriptGenerateManager executionScriptGenerateManager;
    private final FindAlgorithmById findAlgorithmById;
    private final FindAllAlgorithmCasesByAlgo findAllAlgorithmCasesByAlgo;
    private final FindAllCaseInputsByCase findAllCaseInputsByCase;

    Logger logger = LogManager.getLogger(CodeCompilerDAO.class);

    @Override
    public ExecutionOutput compile(Long algoId, String sourceCode, Language language)  {
        var algo = findAlgorithm(algoId);
        return compileAndGetExecutionOutput(algo, sourceCode, language);
    }

    private Algorithm findAlgorithm(Long algoId) {
        var algo = findAlgorithmById.execute(algoId);
        var cases = findAllAlgorithmCasesByAlgo.execute(algoId);
        algo.setCases(cases);
        for (var a : cases) {
            var inputs = findAllCaseInputsByCase.execute(a.getId());
            a.setInput(inputs);
        }
        return algo;
    }

    private ExecutionOutput compileAndGetExecutionOutput(Algorithm algo, String sourceCode, Language language) {
        List<CaseExecutionOutput> caseExecutionOutputs = null;
        var compilationError = false;
        var compilationErrorStack = "";

        try {
            caseExecutionOutputs = codeRunner.runAndVerifyCases(algo, sourceCode, language);
        } catch (DockerBuildException | ContainerExecutionException | IOException e) {
            e.printStackTrace();
        } catch (CompilationException e) {
            compilationError = true;
            compilationErrorStack = e.getMessage();
        }
        var isOverallSuccessful = !compilationError && caseExecutionOutputs.stream().allMatch(CaseExecutionOutput::isSuccessful);

        return new ExecutionOutput()
                .setSuccessfullyCompiled(!compilationError)
                .setCases(caseExecutionOutputs)
                .setErrorOutput(compilationErrorStack)
                .setSuccessful(isOverallSuccessful);
    }

}
