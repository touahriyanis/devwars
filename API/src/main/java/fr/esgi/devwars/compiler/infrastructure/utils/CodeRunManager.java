package fr.esgi.devwars.compiler.infrastructure.utils;

import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.compiler.domain.exception.CompilationException;
import fr.esgi.devwars.compiler.domain.exception.ContainerExecutionException;
import fr.esgi.devwars.compiler.domain.exception.DockerBuildException;
import fr.esgi.devwars.algorithm_case.domain.model.AlgorithmCase;
import fr.esgi.devwars.compiler.domain.model.CaseExecutionOutput;
import fr.esgi.devwars.compiler.domain.model.Language;
import fr.esgi.devwars.compiler.infrastructure.utils.interfaces.ExecutableScript;
import fr.esgi.devwars.compiler.infrastructure.utils.interfaces.RunnableCase;
import fr.esgi.devwars.compiler.infrastructure.utils.interfaces.VerifiableOutput;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CodeRunManager implements RunnableCase, VerifiableOutput {
    private final MainClassGenerateManager mainClassGenerateManager;
    private final DockerManager dockerManager;
    private final FileManager fileManager;
    private final ExecutionScriptGenerateManager executionScriptGenerateManager;

    Logger logger = LogManager.getLogger(CodeRunManager.class);


    public List<CaseExecutionOutput> runAndVerifyCases(Algorithm algo, String sourceCode, Language language) throws IOException, CompilationException, DockerBuildException, ContainerExecutionException {
        executionScriptGenerateManager.generateExecutionScript(language, algo.getTimeLimit(), algo.getMemoryLimit());
        logger.info("Execution script has been uploaded");

        String folderName = fileManager.getFolderName(language);
        String imageName = language.name().toLowerCase() + "image" + new Date().getTime();

        for(int i = 0; i < algo.getCases().size(); i++) {
            try {
                String fileName = fileManager.getFileName(language, i+1);
                String sourceCodeExecutable = mainClassGenerateManager.execute(algo.getCases().get(i), String.valueOf(i+1), algo.getFuncName(), sourceCode, language);
                logger.info(folderName);
                fileManager.convertToFileAndUpload(sourceCodeExecutable, folderName + "/" + fileName);
                logger.info("Source code file has been uploaded");
            } catch (IOException e) {
                logger.info(e.getMessage());
                throw new FileUploadException();
            }
        }

        int status = 1;
        try {
            logger.info("Building the docker image");

            status = dockerManager.buildImage(folderName, imageName).waitFor();

            if(status == 0)
                logger.info("Docker image has been successfully built");
            else
                logger.info("Error while building image");
        } catch (InterruptedException | IOException e) {
            throw new DockerBuildException();
        }

        var caseExecutionOutputs = new LinkedList<CaseExecutionOutput>();

        for(int i = 0; i < algo.getCases().size(); i++) {
            var errorOutput = "";
            var successOutput = "";
            var isTestSuccessful = false;
            if(status == 0) {
                try {
                    logger.info("Running the container");
                    var process = dockerManager.runImage(imageName, String.valueOf(i+1));

                    status = process.waitFor();
                    logger.info("End of the execution of the container");

                    errorOutput = fileManager.getOutputAsString(process.getErrorStream());
                    logger.info("status: " + status);

                    if(!errorOutput.isEmpty())
                        throw new CompilationException(errorOutput);

                    successOutput = fileManager.getOutputAsString(process.getInputStream());

                    isTestSuccessful = verifyOutput(algo.getCases().get(i), successOutput);

                    fileManager.deleteFile(folderName, fileManager.getFileName(language, i+1));
                    fileManager.deleteFile(folderName, "entrypoint.sh");
                    logger.info("All files have been deleted");

                } catch (InterruptedException | IOException e) {
                    throw new ContainerExecutionException();
                }
            }

            caseExecutionOutputs.add(new CaseExecutionOutput(status, algo.getCases().get(i).getName(), successOutput, algo.getCases().get(i).getExpectedOutput(), isTestSuccessful));
        }

        return caseExecutionOutputs;
    }



    public boolean verifyOutput(AlgorithmCase runCase, String output) {
        output = output.replace("\r", "");
        output = output.replace("\n", "");
        return output.equals(runCase.getExpectedOutput());
    }
}

