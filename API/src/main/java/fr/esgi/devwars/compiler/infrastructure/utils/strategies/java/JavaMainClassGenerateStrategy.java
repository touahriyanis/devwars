package fr.esgi.devwars.compiler.infrastructure.utils.strategies.java;

import fr.esgi.devwars.algorithm_case.domain.model.AlgorithmCase;
import fr.esgi.devwars.compiler.domain.strategies.MainClassGenerateStrategy;
import fr.esgi.devwars.compiler.infrastructure.utils.constants.MainClasses;

public class JavaMainClassGenerateStrategy implements MainClassGenerateStrategy {

    @Override
    public String generateMainClass(AlgorithmCase runCase, String mainFileIndex, String funcName, String sourceCode) {
        return String.format(MainClasses.JAVA, mainFileIndex, generateCasesFunctions(runCase, funcName), sourceCode);
    }

    private String generateCasesFunctions(AlgorithmCase runCase, String funcName) {
        String result = "";
        int nb = 1;

        StringBuilder line = new StringBuilder(runCase.getOutputType() + " case" + nb + " = " + funcName + "(");
        for (int i = 0; i < runCase.getInput().size(); i++) {
            if (i == 0) {
                line.append(runCase.getInput().get(i).getValue());
            } else {
                line.append(",").append(runCase.getInput().get(i).getValue());
            }
        }
        line.append(");\n\t\t");
        line.append("System.out.print(case" + nb + ");\n\t\t");
        nb++;
        result += line.toString();
        return result;
    }
}
