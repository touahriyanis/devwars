package fr.esgi.devwars.compiler.infrastructure.utils;

import fr.esgi.devwars.compiler.domain.model.Language;
import fr.esgi.devwars.compiler.infrastructure.utils.constants.ExecutionScripts;
import fr.esgi.devwars.compiler.infrastructure.utils.interfaces.ExecutableScript;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

@Service
public class ExecutionScriptGenerateManager implements ExecutableScript {

    public void generateExecutionScript(Language language, int timeLimit, int memoryLimit) {
        String content = String.format(getExecutionScript(language), memoryLimit, timeLimit);
        OutputStream os = null;
        try {
            os = new FileOutputStream(new File(getPath(language)));
            os.write(content.getBytes(), 0, content.length());
        } catch (IOException e) {
            e.printStackTrace();
        } finally{
            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String getExecutionScript(Language language) {
        switch (language) {
            case Java:
                return ExecutionScripts.JAVA;
            case Python:
                return ExecutionScripts.PYTHON;
            default:
                return ExecutionScripts.CPP;
        }
    }

    public String getPath(Language language) {
        switch (language) {
            case Java:
                return "execution_environments/env_java/entrypoint.sh";
            case Python:
                return "execution_environments/env_python/entrypoint.sh";
            default:
                return "execution_environments/env_cpp/entrypoint.sh";
        }
    }
}
