package fr.esgi.devwars.compiler.domain.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
public class CaseExecutionOutput {
    private Status status;

    private String name;

    private String log;

    private String expectedOutput;

    private boolean isSuccessful;

    public CaseExecutionOutput(int status, String name, String log, String expectedOutput, boolean isSuccessful) {
        this.status = statusResponse(status);
        this.name = name;
        this.log = log;
        this.expectedOutput = expectedOutput;
        this.isSuccessful = isSuccessful;
    }

    private Status statusResponse(int status) {
        Status resultStatus;

        switch(status) {
            case 0:
                resultStatus = Status.Success;
                break;
            case 1:
                resultStatus = Status.RuntimeError;
                break;
            case 2:
                resultStatus = Status.CompilationError;
                break;
            case 139:
                resultStatus = Status.OutOfMemory;
                break;
            default:
                resultStatus = Status.TimeLimitExcess;
                break;
        }

        return resultStatus;
    }
}
