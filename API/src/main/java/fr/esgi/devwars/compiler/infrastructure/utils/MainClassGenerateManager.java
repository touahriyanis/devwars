package fr.esgi.devwars.compiler.infrastructure.utils;

import fr.esgi.devwars.algorithm_case.domain.model.AlgorithmCase;
import fr.esgi.devwars.compiler.domain.model.Language;
import fr.esgi.devwars.compiler.domain.strategies.MainClassGenerateStrategy;
import fr.esgi.devwars.compiler.infrastructure.utils.strategies.cpp.CppMainClassGenerateStrategy;
import fr.esgi.devwars.compiler.infrastructure.utils.strategies.java.JavaMainClassGenerateStrategy;
import fr.esgi.devwars.compiler.infrastructure.utils.strategies.python.PythonMainClassGenerateStrategy;
import org.springframework.stereotype.Service;

@Service
public class MainClassGenerateManager {
    private MainClassGenerateStrategy strategy = new JavaMainClassGenerateStrategy();

    public String execute(AlgorithmCase runCase, String fileName, String funcName, String sourceCode, Language language) {
        setStrategy(language);
        return strategy.generateMainClass(runCase, fileName, funcName, sourceCode);
    }

    public void setStrategy(Language language) {
        switch (language) {
            case Java:
                strategy = new JavaMainClassGenerateStrategy();
                break;
            case Python:
                strategy = new PythonMainClassGenerateStrategy();
                break;
            case Cpp:
                strategy = new CppMainClassGenerateStrategy();
                break;
        }
    }

    public MainClassGenerateStrategy getStrategy() {
        return strategy;
    }
}
