package fr.esgi.devwars.compiler.domain.strategies;

import fr.esgi.devwars.algorithm_case.domain.model.AlgorithmCase;

public interface MainClassGenerateStrategy {
    String generateMainClass(AlgorithmCase runCase, String mainFileIndex, String funcName, String sourceCode);
}
