package fr.esgi.devwars.compiler.infrastructure.utils.constants;

public final class ExecutionScripts {
    public static final String JAVA = "#!/usr/bin/env bash\n" +
            "mv main$1.java Main$1.java\n" +
            "javac Main$1.java\n" +
            "ret=$?\n" +
            "if [ $ret -ne 0 ]\n" +
            "then\n" +
            "  exit 2\n" +
            "fi\n" +
            "ulimit -s %s\n" +
            "timeout --signal=SIGTERM %s java Main$1\n" +
            "exit $?\n";

    public static final String CPP = "#!/usr/bin/env bash\n" +
            "g++ main$1.cpp" + " -o exec" + "\n" +
            "ret=$?\n" +
            "if [ $ret -ne 0 ]\n" +
            "then\n" +
            "  exit 2\n" +
            "fi\n" +
            "ulimit -s %s\n" +
            "timeout --signal=SIGTERM %s ./exec " + "\n" +
            "exit $?\n";

    public static final String PYTHON = "#!/usr/bin/env bash\n" +
            "ulimit -s %s\n" +
            "timeout --signal=SIGTERM %s python3 main$1.py" + "\n" +
            "exit $?\n";
}
