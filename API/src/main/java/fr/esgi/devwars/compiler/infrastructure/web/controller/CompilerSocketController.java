package fr.esgi.devwars.compiler.infrastructure.web.controller;

import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class CompilerSocketController {

    @MessageMapping("/resume")
    @SendTo("/start/initial")
    public String chat(String msg) {
        System.out.println(msg);
        return msg;
    }

    @MessageMapping("/fleet/{userId}")
    @SendTo("/start/{userId}")
    public String simple(@DestinationVariable int userId) {
        System.out.println(userId);
        return "heeey "+userId;
    }
}
