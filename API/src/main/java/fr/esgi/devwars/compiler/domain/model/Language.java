package fr.esgi.devwars.compiler.domain.model;

public enum Language {
    Java,
    Python,
    Cpp
}