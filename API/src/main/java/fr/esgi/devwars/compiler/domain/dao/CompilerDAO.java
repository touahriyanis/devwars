package fr.esgi.devwars.compiler.domain.dao;

import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.compiler.domain.model.ExecutionOutput;
import fr.esgi.devwars.compiler.domain.model.Language;

import java.io.IOException;

public interface CompilerDAO {
    ExecutionOutput compile(Long algoId, String sourceCode, Language language) throws IOException, InterruptedException;
}
