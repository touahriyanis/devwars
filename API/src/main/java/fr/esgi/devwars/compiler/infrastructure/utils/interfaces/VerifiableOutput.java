package fr.esgi.devwars.compiler.infrastructure.utils.interfaces;

import fr.esgi.devwars.algorithm_case.domain.model.AlgorithmCase;

public interface VerifiableOutput {
    boolean verifyOutput(AlgorithmCase runCase, String output);
}
