package fr.esgi.devwars.compiler.domain.exception;

public class CompilationException extends RuntimeException{
    public CompilationException(String message) {
        super(message);
    }
}
