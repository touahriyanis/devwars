package fr.esgi.devwars.compiler.infrastructure.utils.interfaces;

import fr.esgi.devwars.compiler.domain.model.Language;

import java.io.IOException;
import java.io.InputStream;

public interface ManageableFile {
    void convertToFileAndUpload(String code, String name) throws IOException;
    boolean deleteFile(String folder, String file);
    String getFolderName(Language language) throws IOException;
    String getFileName(Language language, int index);
    String getOutputAsString(InputStream stream) throws IOException;
}
