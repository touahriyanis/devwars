package fr.esgi.devwars.player.domain.exception;

public class TooManyPlayersInGameException extends RuntimeException {
    public TooManyPlayersInGameException(String message) {
        super(message);
    }
}
