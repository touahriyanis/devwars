package fr.esgi.devwars.player.infrastructure.web.response;

import fr.esgi.devwars.game.domain.model.Game;
import fr.esgi.devwars.user.domain.model.User;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Accessors(chain = true)
@Data
@RequiredArgsConstructor
public class PlayerResponse implements Serializable {
    private Game game;
    private User user;
    private Integer remainingHealthPoints;
    private Boolean won;
}
