package fr.esgi.devwars.player.infrastructure.dataprovider.entity;

import java.io.Serializable;

public class PlayerId implements Serializable {
    private Long gameId;
    private Long userId;
}
