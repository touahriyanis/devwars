package fr.esgi.devwars.player.use_case;

import fr.esgi.devwars.player.domain.dao.PlayerDAO;
import fr.esgi.devwars.game.domain.dao.GameDAO;
import fr.esgi.devwars.game.domain.model.Game;
import fr.esgi.devwars.player.domain.exception.GameOverException;
import fr.esgi.devwars.player.domain.model.Player;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import fr.esgi.devwars.user.domain.dao.UserDAO;
import fr.esgi.devwars.user.domain.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RemovePlayer {

    private final PlayerDAO playerDAO;
    private final GameDAO gameDAO;
    private final UserDAO userDAO;

    public void execute(Long gameId, Long userId) {
        checkIfGameExistsAndIsOpen(gameId);
        checkIfUserExists(userId);
        checkIfPlayerExists(gameId, userId);
        playerDAO.delete(gameId, userId);
    }

    private void checkIfGameExistsAndIsOpen(Long id) {
        Game game = gameDAO.findById(id);
        if (game == null) {
            throw new NotFoundException(String.format("Game with id %s not found", id));
        }
        if (game.isOver()) {
            throw new GameOverException(String.format("Game with id %s is closed", id));
        }
    }

    private void checkIfUserExists(Long id) {
        User exist = userDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("User with id %s not found", id));
        }
    }

    private void checkIfPlayerExists(Long gameId, Long userId) {
        Player exist = playerDAO.findByIds(gameId, userId);
        if (exist == null) {
            throw new NotFoundException(String.format("No relation between game %s and user %s", gameId, userId));
        }
    }
}
