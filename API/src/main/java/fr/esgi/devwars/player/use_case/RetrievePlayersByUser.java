package fr.esgi.devwars.player.use_case;

import fr.esgi.devwars.player.domain.dao.PlayerDAO;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import fr.esgi.devwars.player.domain.model.Player;
import fr.esgi.devwars.user.domain.dao.UserDAO;
import fr.esgi.devwars.user.domain.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RetrievePlayersByUser {

    private final PlayerDAO playerDAO;
    private final UserDAO userDAO;

    public List<Player> execute(Long userId) {
        checkIfUserExists(userId);
        return playerDAO.findAllByUser(userId);
    }

    private void checkIfUserExists(Long id) {
        User exist = userDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("User with id %s not found", id));
        }
    }

}
