package fr.esgi.devwars.player.use_case;

import fr.esgi.devwars.player.domain.dao.PlayerDAO;
import fr.esgi.devwars.player.domain.model.Player;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RetrievePlayersByGames {

    private final PlayerDAO playerDAO;

    public List<Player> execute(List<Long> gameIds) {
        return playerDAO.findAllByAllGames(gameIds);
    }
}
