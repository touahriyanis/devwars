package fr.esgi.devwars.player.domain.exception;

public class TooManyPlayersWonException extends RuntimeException {
    public TooManyPlayersWonException(String message) {
        super(message);
    }
}
