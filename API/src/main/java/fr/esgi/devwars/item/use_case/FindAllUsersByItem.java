package fr.esgi.devwars.item.use_case;

import fr.esgi.devwars.user.domain.model.User;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import fr.esgi.devwars.item.domain.dao.ItemDAO;
import fr.esgi.devwars.item.domain.model.Item;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FindAllUsersByItem {

    private final ItemDAO itemDAO;

    public List<User> execute(Long itemId) {
        final var item = checkIfItemExists(itemId);
        return itemDAO.findItemPossessors(item);
    }

    private Item checkIfItemExists(Long id) {
        Item exist = itemDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("Item with id %s not found", id));
        }
        return exist;
    }

}
