package fr.esgi.devwars.item.domain.exception;

public class NoGameSpecifiedException extends RuntimeException {
    public NoGameSpecifiedException(String message) {
        super(message);
    }
}
