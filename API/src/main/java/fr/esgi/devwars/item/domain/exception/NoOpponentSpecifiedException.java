package fr.esgi.devwars.item.domain.exception;

public class NoOpponentSpecifiedException extends RuntimeException {
    public NoOpponentSpecifiedException(String message) {
        super(message);
    }
}
