package fr.esgi.devwars.item.infrastructure.web.adapter;

import fr.esgi.devwars.item.domain.model.Item;
import fr.esgi.devwars.item.infrastructure.web.response.ItemResponse;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ItemAdapter {
    public ItemResponse toResponse(Item item) {
        return new ItemResponse()
                .setId(item.getId())
                .setDamageAmount(item.getDamageAmount())
                .setHealAmount(item.getHealAmount())
                .setName(item.getName())
                .setPower(item.getPower());
    }

    public List<ItemResponse> toResponses(List<Item> items) {
        return items
                .stream()
                .map(this::toResponse)
                .collect(Collectors.toList());
    }


}
