package fr.esgi.devwars.item.use_case;

import fr.esgi.devwars.common.domain.exception.NotFoundException;
import fr.esgi.devwars.game.domain.dao.GameDAO;
import fr.esgi.devwars.game.domain.exception.IncorrectPlayerNumberException;
import fr.esgi.devwars.game.domain.model.Game;
import fr.esgi.devwars.item.domain.dao.ItemDAO;
import fr.esgi.devwars.item.domain.exception.*;
import fr.esgi.devwars.item.domain.model.Item;
import fr.esgi.devwars.player.domain.dao.PlayerDAO;
import fr.esgi.devwars.player.domain.exception.GameOverException;
import fr.esgi.devwars.player.domain.model.Player;
import fr.esgi.devwars.user.domain.dao.UserDAO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UseItem {

    private final PlayerDAO playerDAO;
    private final ItemDAO itemDAO;
    private final GameDAO gameDAO;
    private final UserDAO userDAO;

    public void execute(Long itemId, Long gameId, Long userId, Integer errorNumber) {
        final var item = getItem(itemId);
        final var game = findGame(gameId);
        assertGameIsNotOver(game);
        final var players = findPlayers(gameId);

        final var currentPlayer = getCurrentPlayer(players, userId);
        assertCurrentPlayerCanUseItem(currentPlayer, item);

        if (item.getHealAmount() > 0) {
            healCurrentPlayer(item, currentPlayer, errorNumber);
        }

        if (item.getDamageAmount() > 0) {
            final var opponent = getOpponent(players, currentPlayer);
            attackOpponent(item, opponent, errorNumber);
        }
    }

    private void assertGameIsNotOver(Game game) {
        if (game.isOver()) {
            throw new GameOverException(String.format("Game %s is over", game.getId()));
        }
    }

    private void assertCurrentPlayerCanUseItem(Player currentPlayer, Item item) {
        var userItems = userDAO.findItemsByUser(currentPlayer.getUser());

        if (userItems.stream().noneMatch(userItem -> userItem.equals(item))) {

            throw new CannotUseItemException(String.format("User %s cannot use item %s",
                    currentPlayer.getUser().getId(),
                    item.getId()));
        }
    }

    private Player getOpponent(List<Player> players, Player currentPlayer) {
        final var opponent = players.stream()
                .filter(player -> !player.equals(currentPlayer))
                .findFirst();

        if (opponent.isEmpty()) {
            throw new NoOpponentSpecifiedException("No opponent to use item");
        }
        return opponent.get();
    }

    private List<Player> findPlayers(Long gameId) {
        final var players = playerDAO.findAllByGame(gameId);
        if (players.size() != 2) {
            throw new IncorrectPlayerNumberException("There should be 2 players in a game");
        }
        return players;
    }

    private Player getCurrentPlayer(List<Player> players, Long userId) {
        final var currentPlayer = players.stream()
                .filter(player -> player.getUser().getId().equals(userId))
                .findFirst();

        if (currentPlayer.isEmpty()) {
            throw new NoCurrentPlayerException("No performer to use item");
        }

        return currentPlayer.get();
    }

    private Game findGame(Long gameId) {
        if (gameId == null) {
            throw new NoGameSpecifiedException("No game specified to use the item");
        }
        final var game = gameDAO.findById(gameId);
        if (game == null) {
            throw new NotFoundException(String.format("Game with id %s not found", gameId));
        }
        return game;
    }

    private Item getItem(Long itemId) {
        if (itemId == null) {
            throw new NoItemToUseException("No item to use specified");
        }
        final var exists = itemDAO.findById(itemId);
        if (exists == null) {
            throw new NotFoundException(String.format("Item with id %s not found", itemId));
        }
        return exists;
    }

    private void healCurrentPlayer(Item item, Player player, Integer errorNumber) {
        item.healPlayer(player, calculateHealAmount(item, errorNumber));
        playerDAO.update(player);
    }

    private void attackOpponent(Item item, Player target, Integer errorNumber) {
        item.attackPlayer(target, calculateDamageAmount(item, errorNumber));
        playerDAO.update(target);
    }

    public Integer calculateDamageAmount(Item item, Integer errorNumber) {
        final var damageAmount = item.getDamageAmount();
        final var percentage = (double) errorNumber * 10 / 100;
        final var valueToRetire = damageAmount * percentage;
        return Math.max(1, damageAmount - (int) valueToRetire);
    }

    public Integer calculateHealAmount(Item item, Integer errorNumber) {
        final var healAmount = item.getHealAmount();
        final var percentage = (double) errorNumber * 10 / 100;
        final var valueToRetire = healAmount * percentage;
        return Math.max(1, healAmount - (int) valueToRetire);
    }

}
