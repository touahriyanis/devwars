package fr.esgi.devwars.item.domain.exception;

public class NoItemToUseException extends RuntimeException {
    public NoItemToUseException(String message) {
        super(message);
    }
}
