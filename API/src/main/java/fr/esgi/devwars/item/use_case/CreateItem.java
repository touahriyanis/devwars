package fr.esgi.devwars.item.use_case;

import fr.esgi.devwars.item.domain.dao.ItemDAO;
import fr.esgi.devwars.item.domain.model.Item;
import fr.esgi.devwars.common.domain.exception.AlreadyExistsException;
import fr.esgi.devwars.item.infrastructure.web.request.ItemRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CreateItem {

    private final ItemDAO itemDAO;

    public Item execute(ItemRequest request) {
        checkIfNameAlreadyExists(request.getName());

        Item item = new Item()
                .setName(request.getName())
                .setDamageAmount(request.getDamageAmount())
                .setHealAmount(request.getHealAmount())
                .setPower(request.getPower());

        return itemDAO.create(item);
    }

    private void checkIfNameAlreadyExists(String name) {
        Item exist = itemDAO.findByName(name);
        if (exist != null) {
            throw new AlreadyExistsException(String.format("Item with name %s already exists", name));
        }
    }
}
