package fr.esgi.devwars.item.infrastructure.dataprovider.mapper;

import fr.esgi.devwars.item.domain.model.Item;
import fr.esgi.devwars.item.infrastructure.dataprovider.entity.ItemEntity;
import org.springframework.stereotype.Component;

@Component
public class ItemMapper {
    public Item toDomain(ItemEntity entity) {
        return new Item()
                .setId(entity.getId())
                .setName(entity.getName())
                .setDamageAmount(entity.getDamageAmount())
                .setHealAmount(entity.getHealAmount())
                .setPower(entity.getPower());
    }

    public ItemEntity toEntity(Item item) {
        return new ItemEntity()
                .setName(item.getName())
                .setDamageAmount(item.getDamageAmount())
                .setHealAmount(item.getHealAmount())
                .setPower(item.getPower());
    }
}
