package fr.esgi.devwars.item.infrastructure.web.controller;

import fr.esgi.devwars.item.domain.model.Item;
import fr.esgi.devwars.item.infrastructure.web.adapter.ItemAdapter;
import fr.esgi.devwars.item.infrastructure.web.request.ItemRequest;
import fr.esgi.devwars.item.infrastructure.web.response.ItemResponse;
import fr.esgi.devwars.item.use_case.*;
import fr.esgi.devwars.user.domain.model.User;
import fr.esgi.devwars.user.infrastructure.web.adapter.UserAdapter;
import fr.esgi.devwars.user.infrastructure.web.response.UserResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.net.URI;
import java.util.List;

@RestController
@Validated
@RequestMapping("/api/items")
@RequiredArgsConstructor
public class ItemController {

    private final ItemAdapter adapter;
    private final CreateItem createItem;
    private final UpdateItem updateItem;
    private final FindItemById findItemById;
    private final FindAllItems findAllItems;
    private final DeleteItemById deleteItemById;
    private final UserAdapter userAdapter;
    private final FindAllUsersByItem findAllUsersByItem;
    private final AddItemPossessor addItemPossessor;
    private final DeleteItemPossessor deleteItemPossessor;

    @PostMapping
    public ResponseEntity<ItemResponse> create(@Valid @RequestBody ItemRequest request) {

        Item item = createItem.execute(request);

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(item.getId())
                .toUri();

        return ResponseEntity
                .created(uri)
                .body(adapter.toResponse(item));
    }

    @PutMapping("/{id}")
    public ResponseEntity<ItemResponse> update(
            @PathVariable("id") @NotNull @PositiveOrZero Long id,
            @Valid @RequestBody ItemRequest request) {

        Item item = updateItem.execute(id, request);

        return ResponseEntity
                .ok()
                .body(adapter.toResponse(item));
    }

    @GetMapping
    public ResponseEntity<List<ItemResponse>> findAll() {
        List<Item> items = findAllItems.execute();
        return ResponseEntity.ok(adapter.toResponses(items));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ItemResponse> findById(@PathVariable("id") @NotNull @PositiveOrZero Long id) {
        Item item = findItemById.execute(id);
        return ResponseEntity.ok(adapter.toResponse(item));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ItemResponse> deleteById(@PathVariable("id") @NotNull @PositiveOrZero Long id) {
        deleteItemById.execute(id);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{itemId}/users/{userId}")
    public ResponseEntity<UserResponse> deleteUserFromItem(
            @PathVariable("userId") @NotNull @Positive Long userId,
            @PathVariable("itemId") @NotNull @Positive Long itemId) {

        deleteItemPossessor.execute(userId, itemId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{itemId}/users")
    public ResponseEntity<List<UserResponse>> findAllUsersByItem(
            @PathVariable("itemId") @NotNull @Positive Long itemId) {

        List<User> users = findAllUsersByItem.execute(itemId);
        return ResponseEntity.ok(userAdapter.toResponses(users));
    }

    @PutMapping("/{itemId}/users/{userId}")
    public ResponseEntity<UserResponse> addUserToItem(
            @PathVariable("userId") @NotNull @Positive Long userId,
            @PathVariable("itemId") @NotNull @Positive Long itemId) {

        User user = addItemPossessor.execute(userId, itemId);

        return ResponseEntity
                .ok()
                .body(userAdapter.toResponse(user));
    }

}
