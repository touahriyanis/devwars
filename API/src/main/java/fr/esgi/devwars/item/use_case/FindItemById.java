package fr.esgi.devwars.item.use_case;

import fr.esgi.devwars.item.domain.dao.ItemDAO;
import fr.esgi.devwars.item.domain.model.Item;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FindItemById {

    private final ItemDAO itemDAO;

    public Item execute(Long id) {
        var item = itemDAO.findById(id);
        if (item == null) {
            throw new NotFoundException(String.format("Item with id %s not found", id));
        }
        return item;
    }
}
