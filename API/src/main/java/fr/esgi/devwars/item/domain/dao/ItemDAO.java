package fr.esgi.devwars.item.domain.dao;

import fr.esgi.devwars.item.domain.model.Item;
import fr.esgi.devwars.user.domain.model.User;

import java.util.List;

public interface ItemDAO {
    Item create(Item item);

    Item findById(Long id);

    Item findByName(String name);

    List<Item> findAll();

    Item update(Item item);

    void delete(Long id);

    List<User> findItemPossessors(Item item);

    void addPossessor(Item item, User possessor);

    void deletePossessor(Item item, User possessor);
}
