package fr.esgi.devwars.item.infrastructure.web.request;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

@Data
@Accessors(chain = true)
public class ItemRequest {
    @NotBlank
    private String name;

    @NotNull
    @Positive(message = "Power doit être un nombre positif")
    private Integer power;

    @NotNull
    @PositiveOrZero
    private Integer damageAmount;

    @NotNull
    @PositiveOrZero
    private Integer healAmount;
}
