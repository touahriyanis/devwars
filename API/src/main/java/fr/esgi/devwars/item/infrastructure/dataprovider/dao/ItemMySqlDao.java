package fr.esgi.devwars.item.infrastructure.dataprovider.dao;

import fr.esgi.devwars.item.domain.dao.ItemDAO;
import fr.esgi.devwars.item.domain.model.Item;
import fr.esgi.devwars.item.infrastructure.dataprovider.entity.ItemEntity;
import fr.esgi.devwars.item.infrastructure.dataprovider.mapper.ItemMapper;
import fr.esgi.devwars.item.infrastructure.dataprovider.repository.ItemRepository;
import fr.esgi.devwars.user.domain.model.User;
import fr.esgi.devwars.user.infrastructure.dataprovider.entity.UserItemEntity;
import fr.esgi.devwars.user.infrastructure.dataprovider.mapper.UserItemMapper;
import fr.esgi.devwars.user.infrastructure.dataprovider.mapper.UserMapper;
import fr.esgi.devwars.user.infrastructure.dataprovider.repository.UserItemRepository;
import fr.esgi.devwars.user.infrastructure.dataprovider.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ItemMySqlDao implements ItemDAO {

    private final ItemRepository repository;
    private final ItemMapper mapper;
    private final UserItemRepository userItemRepository;
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final UserItemMapper userItemMapper;

    @Override
    public Item create(Item item) {
        ItemEntity entity = mapper.toEntity(item);
        entity = repository.save(entity);
        return mapper.toDomain(entity);
    }

    @Override
    public Item findById(Long id) {
        return repository.findById(id)
                .map(mapper::toDomain)
                .orElse(null);
    }

    @Override
    public Item findByName(String name) {
        return Optional.ofNullable(repository.findByName(name))
                .map(mapper::toDomain)
                .orElse(null);
    }

    @Override
    public List<Item> findAll() {
        return repository.findAll()
                .stream()
                .map(mapper::toDomain)
                .collect(Collectors.toList());
    }

    @Override
    public Item update(Item item) {
        ItemEntity itemEntity = repository.getOne(item.getId());
        itemEntity.setName(item.getName());
        itemEntity.setDamageAmount(item.getDamageAmount());
        itemEntity.setHealAmount(item.getHealAmount());
        itemEntity.setPower(item.getPower());
        return mapper.toDomain(repository.save(itemEntity));
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }

    @Override
    public List<User> findItemPossessors(Item item) {
        List<Long> ids = userItemRepository.findByItemId(item.getId())
                .stream()
                .map(UserItemEntity::getUserId)
                .collect(Collectors.toList());

        return userRepository.findAllById(ids)
                .stream().map(userMapper::toDomain)
                .collect(Collectors.toList());
    }

    @Override
    public void addPossessor(Item item, User possessor) {
        UserItemEntity entity = userItemMapper.toEntity(possessor, item);
        userItemRepository.save(entity);
    }

    @Override
    public void deletePossessor(Item item, User possessor) {
        userItemRepository.findByUserIdAndItemId(possessor.getId(), item.getId())
                .ifPresent(userItemRepository::delete);
    }
}
