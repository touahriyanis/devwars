package fr.esgi.devwars.item.infrastructure.dataprovider.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Entity(name = "item")
@Accessors(chain = true)
@Data
public class ItemEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private Integer power;

    @Column(name = "damage_amount")
    private Integer damageAmount;

    @Column(name = "heal_amount")
    private Integer healAmount;
}
