package fr.esgi.devwars.item.infrastructure.dataprovider.repository;

import fr.esgi.devwars.item.infrastructure.dataprovider.entity.ItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemRepository extends JpaRepository<ItemEntity, Long> {
    ItemEntity findByName(String name);
}
