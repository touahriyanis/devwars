package fr.esgi.devwars.item.use_case;

import fr.esgi.devwars.item.domain.dao.ItemDAO;
import fr.esgi.devwars.item.domain.model.Item;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FindAllItems {

    private final ItemDAO itemDAO;

    public List<Item> execute() {
        return itemDAO.findAll();
    }
}
