package fr.esgi.devwars.item.domain.exception;

public class CannotUseItemException extends RuntimeException {
    public CannotUseItemException(String message) {
        super(message);
    }
}
