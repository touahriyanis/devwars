package fr.esgi.devwars.item.domain.exception;

public class NoCurrentPlayerException extends RuntimeException {
    public NoCurrentPlayerException(String message) {
        super(message);
    }
}
