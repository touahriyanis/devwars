package fr.esgi.devwars.item.use_case;

import fr.esgi.devwars.item.domain.dao.ItemDAO;
import fr.esgi.devwars.item.domain.model.Item;
import fr.esgi.devwars.common.domain.exception.AlreadyExistsException;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import fr.esgi.devwars.item.infrastructure.web.request.ItemRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UpdateItem {

    private final ItemDAO itemDAO;

    public Item execute(Long id, ItemRequest request) {
        checkIfExists(id);
        checkIfNameAlreadyExists(request.getName(), id);

        Item item = new Item()
                .setId(id)
                .setName(request.getName())
                .setDamageAmount(request.getDamageAmount())
                .setHealAmount(request.getHealAmount())
                .setPower(request.getPower());

        return itemDAO.update(item);
    }

    private void checkIfExists(Long id) {
        var item = itemDAO.findById(id);
        if (item == null) {
            throw new NotFoundException(String.format("Item with id %s not found", id));
        }
    }

    private void checkIfNameAlreadyExists(String name, Long id) {
        Item exist = itemDAO.findByName(name);
        if (exist != null && !exist.getId().equals(id)) {
            throw new AlreadyExistsException(String.format("Item with name %s already exists", name));
        }
    }
}
