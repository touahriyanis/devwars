package fr.esgi.devwars.item.domain.exception;

public class AlreadyHaveItemException extends RuntimeException {
    public AlreadyHaveItemException(String message) {
        super(message);
    }
}
