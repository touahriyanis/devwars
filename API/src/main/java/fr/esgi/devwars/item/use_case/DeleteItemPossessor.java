package fr.esgi.devwars.item.use_case;


import fr.esgi.devwars.common.domain.exception.NotFoundException;
import fr.esgi.devwars.item.domain.dao.ItemDAO;
import fr.esgi.devwars.item.domain.model.Item;
import fr.esgi.devwars.user.domain.dao.UserDAO;
import fr.esgi.devwars.user.domain.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DeleteItemPossessor {

    private final UserDAO userDAO;
    private final ItemDAO itemDAO;

    public void execute(Long userId, Long itemId) {
        final var possessor = checkIfUserExists(userId);
        final var item = checkIfItemExists(itemId);
        checkIfItemHasUser(item, possessor.getId());

        itemDAO.deletePossessor(item, possessor);
    }

    private User checkIfUserExists(Long id) {
        User exist = userDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("User with id %s not found", id));
        }
        return exist;
    }

    private Item checkIfItemExists(Long id) {
        Item exist = itemDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("Item with id %s not found", id));
        }
        return exist;
    }

    private void checkIfItemHasUser(Item item, Long userId) {
        List<User> possessors = itemDAO.findItemPossessors(item);
        if (possessors.stream().noneMatch(possessor -> possessor.getId().equals(userId))) {
            throw new NotFoundException(String.format("No relation between user %s and item %s", userId, item.getId()));
        }
    }
}
