package fr.esgi.devwars.item.domain.model;

import fr.esgi.devwars.player.domain.model.Player;
import fr.esgi.devwars.user.domain.model.User;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;


@Accessors(chain = true)
@Data
public class Item {
    private Long id;
    private String name;
    private Integer power;
    private Integer damageAmount;
    private Integer healAmount;
    private List<User> possessors;

    public void healPlayer(Player target, Integer healAmount) {
        final var remainingHealthPoints = target.getRemainingHealthPoints();
        final var newHealthPoints = calculateNewHealthPointsWithLimit(remainingHealthPoints, healAmount);
        target.setRemainingHealthPoints(newHealthPoints);
    }

    public void attackPlayer(Player target, Integer damageAmount) {
        final var remainingHealthPoints = target.getRemainingHealthPoints();
        target.setRemainingHealthPoints(remainingHealthPoints - damageAmount);
    }

    private Integer calculateNewHealthPointsWithLimit(Integer remainingHealthPoints, Integer healAmount) {
        final var maxHealthPoints = Player.getMaxHealthPoints();
        return Math.min(remainingHealthPoints + healAmount, maxHealthPoints);
    }
}
