package fr.esgi.devwars.user.infrastructure.dataprovider.repository;


import fr.esgi.devwars.user.infrastructure.dataprovider.entity.UserActionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserActionRepository extends JpaRepository<UserActionEntity, Long> {
    Optional<UserActionEntity> findByUserIdAndActionId(Long userId, Long actionId);

    List<UserActionEntity> findByActionId(Long actionId);

    List<UserActionEntity> findByUserId(Long userId);
}
