package fr.esgi.devwars.user.use_case;

import fr.esgi.devwars.algorithm.domain.dao.AlgorithmDAO;
import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import fr.esgi.devwars.game.domain.dao.GameDAO;
import fr.esgi.devwars.game.domain.model.Game;
import fr.esgi.devwars.player.domain.dao.PlayerDAO;
import fr.esgi.devwars.player.domain.model.Player;
import fr.esgi.devwars.resolution.domain.dao.ResolutionDAO;
import fr.esgi.devwars.resolution.domain.model.Resolution;
import fr.esgi.devwars.user.domain.dao.UserDAO;
import fr.esgi.devwars.user.domain.model.User;
import fr.esgi.devwars.user.infrastructure.web.response.StatisticsResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class GetStatisticsOfOneUser {

    private final UserDAO userDAO;
    private final PlayerDAO playerDAO;
    private final ResolutionDAO resolutionDAO;
    private final AlgorithmDAO algorithmDAO;

    public StatisticsResponse execute(Long userId) {
        final var statistics = new StatisticsResponse();
        checkIfUserExists(userId);
        final var players = playerDAO.findAllByUser(userId);
        final var closedGames = getClosedGames(players);
        final var opponents = findOpponents(closedGames, userId);

        statistics.setNumberOfGamesWon(getTotalWinGames(players));
        statistics.setNumberOfGamesLost(getTotalLostGames(players));

        statistics.setTotalHealthPointsLost(getTotalLostHP(players));
        statistics.setTotalDamageInflicted(getTotalLostHP(opponents));

        final var resolutions = resolutionDAO.findResolutionsByUser(userId);
        statistics.setNumberOfAlgorithmsSolved(getTotalAlgoSolved(resolutions));
        statistics.setNumberOfUnresolvedAlgorithms(getTotalAlgoUnresolved(resolutions));
        statistics.setAverageComplexitySolved(getAverageComplexityResolved(resolutions));
        return statistics;
    }

    private List<Game> getClosedGames(List<Player> players) {
        return players.stream()
                .map(Player::getGame)
                .filter(Game::isOver)
                .collect(Collectors.toList());
    }

    private List<Player> findOpponents(List<Game> games, Long userId) {
        List<Player> opponents = new ArrayList<>();

        for (Game game : games) {
            final var players = playerDAO.findAllByGame(game.getId());

            for (Player player : players) {
                if (!player.getUser().getId().equals(userId)) {
                    opponents.add(player);
                }
            }
        }

        return opponents;
    }

    private void checkIfUserExists(Long userId) {
        User user = userDAO.findById(userId);
        if (user == null) throw new NotFoundException(
                String.format("User with id '%d' does not exist", userId));
    }

    private Long getTotalWinGames(List<Player> players) {
        return players.stream()
                .filter(Player::hasWon).count();
    }

    private Long getTotalLostGames(List<Player> players) {
        return players.stream()
                .filter(player -> !player.hasWon()).count();
    }

    private Long getTotalAlgoSolved(List<Resolution> resolutions) {
        return resolutions.stream()
                .filter(Resolution::isSolved).count();
    }

    private Long getTotalAlgoUnresolved(List<Resolution> resolutions) {
        return resolutions.stream()
                .filter(resolution -> !resolution.isSolved()).count();
    }

    private Long getTotalLostHP(List<Player> players) {
        return players.stream()
                .mapToLong(player -> Player.MAX_HEALTH_POINTS - player.getRemainingHealthPoints())
                .sum();
    }

    private Double getAverageComplexityResolved(List<Resolution> resolutions) {
        final var algoSolvedIds = resolutions.stream()
                .filter(Resolution::isSolved)
                .map(resolution -> resolution.getAlgorithm().getId())
                .collect(Collectors.toList());

        final var algos = algorithmDAO.findAllByIds(algoSolvedIds);
        return algos.stream()
                .mapToDouble(Algorithm::getComplexity)
                .average().orElse(0);
    }


}
