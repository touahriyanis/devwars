package fr.esgi.devwars.user.infrastructure.dataprovider.mapper;

import fr.esgi.devwars.action.domain.model.Action;
import fr.esgi.devwars.user.domain.model.User;
import fr.esgi.devwars.user.infrastructure.dataprovider.entity.UserActionEntity;
import org.springframework.stereotype.Component;

@Component
public class UserActionMapper {
    public UserActionEntity toEntity(User user, Action action) {
        return new UserActionEntity()
                .setUserId(user.getId())
                .setActionId(action.getId());
    }
}
