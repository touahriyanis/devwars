package fr.esgi.devwars.user.use_case;

import fr.esgi.devwars.common.domain.exception.AlreadyExistsException;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import fr.esgi.devwars.item.domain.dao.ItemDAO;
import fr.esgi.devwars.item.domain.model.Item;
import fr.esgi.devwars.user.domain.dao.UserDAO;
import fr.esgi.devwars.user.domain.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AddItemToUser {

    private final UserDAO userDAO;
    private final ItemDAO itemDAO;

    public Item execute(Long userId, Long itemId) {
        User user = checkIfUserExists(userId);
        Item item = checkIfItemExists(itemId);
        checkIfUserHasItem(user, itemId);

        userDAO.addItem(user, item);
        return item;
    }

    private User checkIfUserExists(Long id) {
        User exist = userDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("User with id %s not found", id));
        }
        return exist;
    }

    private Item checkIfItemExists(Long id) {
        Item exist = itemDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("Item with id %s not found", id));
        }
        return exist;
    }

    private void checkIfUserHasItem(User user, Long itemId) {
        List<Item> items = userDAO.findItemsByUser(user);
        if (items.stream().anyMatch(item -> item.getId().equals(itemId))) {
            throw new AlreadyExistsException(String.format("Relation already exists between user %s and item %s", user.getId(), itemId));
        }
    }
}
