package fr.esgi.devwars.user.infrastructure.dataprovider.mapper;

import fr.esgi.devwars.user.domain.model.User;
import fr.esgi.devwars.user.infrastructure.dataprovider.entity.UserItemEntity;
import fr.esgi.devwars.item.domain.model.Item;
import org.springframework.stereotype.Component;

@Component
public class UserItemMapper {
    public UserItemEntity toEntity(User user, Item item) {
        return new UserItemEntity()
                .setUserId(user.getId())
                .setItemId(item.getId());
    }

}
