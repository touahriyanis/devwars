package fr.esgi.devwars.user.infrastructure.dataprovider.entity;

import java.io.Serializable;

public class UserActionEntityId implements Serializable {
    private Long userId;
    private Long actionId;
}
