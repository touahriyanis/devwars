package fr.esgi.devwars.user.use_case;

import fr.esgi.devwars.user.domain.exception.InvalidCredentialsException;
import fr.esgi.devwars.user.infrastructure.security.CustomUserDetails;
import fr.esgi.devwars.user.infrastructure.security.CustomUserDetailsService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SignIn {

    private final AuthenticationManager authenticationManager;
    private final CustomUserDetailsService userDetailsService;

    public CustomUserDetails execute(String username, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (Exception e) {
            throw new InvalidCredentialsException("Wrong credentials");
        }

        return (CustomUserDetails) userDetailsService
                .loadUserByUsername(username);
    }
}
