package fr.esgi.devwars.user.infrastructure.dataprovider.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.io.Serializable;

@Entity(name = "user_item")
@Accessors(chain = true)
@Data
@IdClass(UserItemEntity.class)
public class UserItemEntity implements Serializable {
    @Id
    @Column(name = "user_id")
    private Long userId;

    @Id
    @Column(name = "item_id")
    private Long itemId;
}
