package fr.esgi.devwars.user.domain.dao;

import fr.esgi.devwars.action.domain.model.Action;
import fr.esgi.devwars.item.domain.model.Item;
import fr.esgi.devwars.user.domain.model.User;

import java.util.List;

public interface UserDAO {
    Long registerUser(User user);

    User findById(Long userId);

    User findByName(String name);

    User findByEmail(String email);

    List<Item> findItemsByUser(User user);

    List<Item> findUsedItemsByUser(User user);

    List<Action> findActionsByUser(User user);

    void deleteItem(User user, Item item);

    void addItem(User user, Item item);

    void deleteAction(User user, Action action);

    void addAction(User user, Action action);
}
