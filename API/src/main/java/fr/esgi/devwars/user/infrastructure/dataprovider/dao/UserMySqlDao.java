package fr.esgi.devwars.user.infrastructure.dataprovider.dao;

import fr.esgi.devwars.action.domain.model.Action;
import fr.esgi.devwars.action.infrastructure.dataprovider.mapper.ActionMapper;
import fr.esgi.devwars.action.infrastructure.dataprovider.repository.ActionRepository;
import fr.esgi.devwars.item.domain.model.Item;
import fr.esgi.devwars.item.infrastructure.dataprovider.mapper.ItemMapper;
import fr.esgi.devwars.item.infrastructure.dataprovider.repository.ItemRepository;
import fr.esgi.devwars.user.domain.dao.UserDAO;
import fr.esgi.devwars.user.domain.model.User;
import fr.esgi.devwars.user.infrastructure.dataprovider.entity.UserActionEntity;
import fr.esgi.devwars.user.infrastructure.dataprovider.entity.UserEntity;
import fr.esgi.devwars.user.infrastructure.dataprovider.entity.UserItemEntity;
import fr.esgi.devwars.user.infrastructure.dataprovider.mapper.UserActionMapper;
import fr.esgi.devwars.user.infrastructure.dataprovider.mapper.UserItemMapper;
import fr.esgi.devwars.user.infrastructure.dataprovider.mapper.UserMapper;
import fr.esgi.devwars.user.infrastructure.dataprovider.repository.UserActionRepository;
import fr.esgi.devwars.user.infrastructure.dataprovider.repository.UserItemRepository;
import fr.esgi.devwars.user.infrastructure.dataprovider.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserMySqlDao implements UserDAO {

    private final UserMapper userMapper;
    private final UserRepository userRepository;
    private final UserItemMapper userItemMapper;
    private final UserActionMapper userActionMapper;
    private final UserItemRepository userItemRepository;
    private final UserActionRepository userActionRepository;
    private final ItemRepository itemRepository;
    private final ActionRepository actionRepository;
    private final ItemMapper itemMapper;
    private final ActionMapper actionMapper;

    @Override
    public Long registerUser(User user) {
        UserEntity newUser = userMapper.toEntity(user);
        return userRepository.save(newUser).getId();
    }

    @Override
    public User findById(Long userId) {
        return userRepository.findById(userId)
                .map(userMapper::toDomain)
                .orElse(null);
    }

    @Override
    public User findByName(String name) {
        return Optional.ofNullable(userRepository.findByName(name))
                .map(userMapper::toDomain)
                .orElse(null);
    }

    @Override
    public User findByEmail(String email) {
        return Optional.ofNullable(userRepository.findByEmail(email))
                .map(userMapper::toDomain)
                .orElse(null);
    }

    @Override
    public List<Item> findItemsByUser(User user) {
        List<Long> ids = userItemRepository.findByUserId(user.getId())
                .stream()
                .map(UserItemEntity::getItemId)
                .collect(Collectors.toList());

        return itemRepository
                .findAllById(ids)
                .stream().map(itemMapper::toDomain)
                .collect(Collectors.toList());
    }

    @Override
    public List<Item> findUsedItemsByUser(User user) {
        final var allItems = itemRepository.findAll();
        final List<Long> userItemsIds = userItemRepository
                .findByUserId(user.getId())
                .stream().map(UserItemEntity::getItemId).collect(Collectors.toList());

        final var userItems = itemRepository.findAllById(userItemsIds);

        return allItems.stream()
                .filter(item -> !userItems.contains(item))
                .map(itemMapper::toDomain)
                .collect(Collectors.toList());
    }

    @Override
    public List<Action> findActionsByUser(User user) {
        List<Long> ids = userActionRepository.findByUserId(user.getId())
                .stream()
                .map(UserActionEntity::getActionId)
                .collect(Collectors.toList());

        return actionRepository
                .findAllById(ids)
                .stream().map(actionMapper::toDomain)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteItem(User user, Item item) {
        userItemRepository.findByUserIdAndItemId(user.getId(), item.getId())
                .ifPresent(userItemRepository::delete);
    }

    @Override
    public void addItem(User user, Item item) {
        UserItemEntity entity = userItemMapper.toEntity(user, item);
        userItemRepository.save(entity);
    }

    @Override
    public void deleteAction(User user, Action action) {
        userActionRepository.findByUserIdAndActionId(user.getId(), action.getId())
                .ifPresent(userActionRepository::delete);
    }

    @Override
    public void addAction(User user, Action action) {
        UserActionEntity entity = userActionMapper.toEntity(user, action);
        userActionRepository.save(entity);
    }
}
