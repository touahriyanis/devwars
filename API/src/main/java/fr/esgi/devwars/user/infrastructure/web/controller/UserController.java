package fr.esgi.devwars.user.infrastructure.web.controller;

import fr.esgi.devwars.action.domain.model.Action;
import fr.esgi.devwars.action.infrastructure.web.adapter.ActionAdapter;
import fr.esgi.devwars.action.infrastructure.web.response.ActionResponse;
import fr.esgi.devwars.item.domain.model.Item;
import fr.esgi.devwars.item.infrastructure.web.adapter.ItemAdapter;
import fr.esgi.devwars.item.infrastructure.web.response.ItemResponse;
import fr.esgi.devwars.user.domain.model.User;
import fr.esgi.devwars.user.infrastructure.web.adapter.UserAdapter;
import fr.esgi.devwars.user.infrastructure.web.request.SignInRequest;
import fr.esgi.devwars.user.infrastructure.web.request.SignUpRequest;
import fr.esgi.devwars.user.infrastructure.web.response.JWTResponse;
import fr.esgi.devwars.user.infrastructure.web.response.StatisticsResponse;
import fr.esgi.devwars.user.infrastructure.web.response.UserResponse;
import fr.esgi.devwars.user.infrastructure.security.JWTTokenUtil;
import fr.esgi.devwars.user.use_case.*;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.net.URI;
import java.util.List;

@RestController
@Validated
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class UserController {
    //TODO Faudra check que le user id est bien celui du mec connecté...
    private final UserAdapter userAdapter;
    private final SignUp signUp;
    private final SignIn signIn;
    private final FindOneUser findOneUser;
    private final JWTTokenUtil jwtTokenUtil;
    private final ItemAdapter itemAdapter;
    private final ActionAdapter actionAdapter;
    private final AddItemToUser addItemToUser;
    private final AddActionToUser addActionToUser;
    private final DeleteItemFromUser deleteItemFromUser;
    private final DeleteActionFromUser deleteActionFromUser;
    private final FindAllItemsByUser findAllItemsByUser;
    private final FindAllActionsByUser findAllActionsByUser;
    private final FindAllUsedItemsByUser findAllUsedItemsByUser;
    private final GetStatisticsOfOneUser getStatisticsOfOneUser;

    @PostMapping
    public ResponseEntity<?> register(@Valid @RequestBody SignUpRequest request) {
        Long userId = signUp.execute(request.getName(), request.getEmail(),
                request.getPassword());

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(userId)
                .toUri();

        return ResponseEntity.created(uri).build();
    }

    @PostMapping("/signin")
    public ResponseEntity<JWTResponse> login(@Valid @RequestBody SignInRequest request) {

        final var user = signIn.execute(request.getName(), request.getPassword());
        final var token = jwtTokenUtil.generateToken(user);
        return ResponseEntity.ok(new JWTResponse(token).setEmail(user.getEmail())
                .setName(user.getUsername()).setId(user.getId()));

    }

    @GetMapping("/{id}")
    public ResponseEntity<UserResponse> findById(@PathVariable("id") Long userId) {
        User user = findOneUser.execute(userId);
        return ResponseEntity.ok(userAdapter.toResponse(user));
    }

    @PutMapping("/{userId}/items/{itemId}")
    public ResponseEntity<ItemResponse> addItemToUser(
            @PathVariable("userId") @NotNull @Positive Long userId,
            @PathVariable("itemId") @NotNull @Positive Long itemId) {

        Item item = addItemToUser.execute(userId, itemId);

        return ResponseEntity
                .ok()
                .body(itemAdapter.toResponse(item));
    }

    @DeleteMapping("/{userId}/items/{itemId}")
    public ResponseEntity<ItemResponse> deleteItemFromUser(
            @PathVariable("userId") @NotNull @Positive Long userId,
            @PathVariable("itemId") @NotNull @Positive Long itemId) {

        deleteItemFromUser.execute(userId, itemId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{userId}/items")
    public ResponseEntity<List<ItemResponse>> findAllItemsByUser(
            @PathVariable("userId") @NotNull @Positive Long userId) {

        List<Item> items = findAllItemsByUser.execute(userId);
        return ResponseEntity.ok(itemAdapter.toResponses(items));
    }

    @GetMapping("/{userId}/items/used")
    public ResponseEntity<List<ItemResponse>> findAllUsedItemsByUser(
            @PathVariable("userId") @NotNull @Positive Long userId) {

        List<Item> items = findAllUsedItemsByUser.execute(userId);
        return ResponseEntity.ok(itemAdapter.toResponses(items));
    }

    @PutMapping("/{userId}/actions/{actionId}")
    public ResponseEntity<ActionResponse> addActionToUser(
            @PathVariable("userId") @NotNull @Positive Long userId,
            @PathVariable("actionId") @NotNull @Positive Long actionId) {

        Action action = addActionToUser.execute(userId, actionId);

        return ResponseEntity
                .ok()
                .body(actionAdapter.toResponse(action));
    }

    @DeleteMapping("/{userId}/actions/{actionId}")
    public ResponseEntity<ActionResponse> deleteActionFromUser(
            @PathVariable("userId") @NotNull @Positive Long userId,
            @PathVariable("actionId") @NotNull @Positive Long actionId) {

        deleteActionFromUser.execute(userId, actionId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{userId}/actions")
    public ResponseEntity<List<ActionResponse>> findAllActionsByUser(
            @PathVariable("userId") @NotNull @Positive Long userId) {

        List<Action> actions = findAllActionsByUser.execute(userId);
        return ResponseEntity.ok(actionAdapter.toResponses(actions));
    }

    @GetMapping("/{userId}/statistics")
    public ResponseEntity<StatisticsResponse> findStatisticsOfUser(
            @PathVariable("userId") @NotNull @Positive Long userId) {

        final StatisticsResponse stats = getStatisticsOfOneUser.execute(userId);
        return ResponseEntity.ok(stats);
    }
}
