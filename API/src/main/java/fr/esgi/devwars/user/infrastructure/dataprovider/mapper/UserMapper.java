package fr.esgi.devwars.user.infrastructure.dataprovider.mapper;

import fr.esgi.devwars.user.domain.model.User;
import fr.esgi.devwars.user.infrastructure.dataprovider.entity.UserEntity;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    public User toDomain(UserEntity entity) {
        return new User()
                .setId(entity.getId())
                .setName(entity.getName())
                .setEmail(entity.getEmail())
                .setPassword(entity.getPassword());
    }

    public UserEntity toEntity(User user) {
        return new UserEntity()
                .setName(user.getName())
                .setEmail(user.getEmail())
                .setPassword(user.getPassword());
    }
}
