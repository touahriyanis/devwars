package fr.esgi.devwars.user.infrastructure.web.response;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Accessors(chain = true)
@Data
@RequiredArgsConstructor
public class StatisticsResponse implements Serializable {
    private Long numberOfGamesWon;
    private Long numberOfGamesLost;
    private Long numberOfAlgorithmsSolved;
    private Long numberOfUnresolvedAlgorithms;
    private Double averageComplexitySolved;
    private Long totalHealthPointsLost;
    private Long totalDamageInflicted;
}
