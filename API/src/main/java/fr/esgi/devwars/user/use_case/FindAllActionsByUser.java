package fr.esgi.devwars.user.use_case;

import fr.esgi.devwars.common.domain.exception.NotFoundException;
import fr.esgi.devwars.action.domain.model.Action;
import fr.esgi.devwars.user.domain.dao.UserDAO;
import fr.esgi.devwars.user.domain.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FindAllActionsByUser {

    private final UserDAO userDAO;

    public List<Action> execute(Long userId) {
        final var user = checkIfUserExists(userId);
        return userDAO.findActionsByUser(user);
    }

    private User checkIfUserExists(Long id) {
        User exist = userDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("User with id %s not found", id));
        }
        return exist;
    }
}
