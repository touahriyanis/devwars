package fr.esgi.devwars.user.infrastructure.dataprovider.repository;


import fr.esgi.devwars.user.infrastructure.dataprovider.entity.UserItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserItemRepository extends JpaRepository<UserItemEntity, Long> {
    Optional<UserItemEntity> findByUserIdAndItemId(Long userId, Long itemId);

    List<UserItemEntity> findByItemId(Long itemId);

    List<UserItemEntity> findByUserId(Long userId);
}
