package fr.esgi.devwars.user.infrastructure.dataprovider.entity;

import java.io.Serializable;

public class UserItemEntityId implements Serializable {
    private Long userId;
    private Long itemId;
}
