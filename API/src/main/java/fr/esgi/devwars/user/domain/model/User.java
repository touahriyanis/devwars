package fr.esgi.devwars.user.domain.model;

import fr.esgi.devwars.action.domain.model.Action;
import fr.esgi.devwars.item.domain.model.Item;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
@RequiredArgsConstructor
public class User {
    private Long id;
    private String name;
    private String password;
    private String email;
    private List<Item> items;
    private List<Action> actions;
}
