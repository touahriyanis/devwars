package fr.esgi.devwars.algorithm_item.domain.dao;

import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.algorithm_item.domain.model.AlgorithmItem;
import fr.esgi.devwars.item.domain.model.Item;

import java.util.List;

public interface AlgorithmItemDAO {
    void create(AlgorithmItem algorithmItem);

    void delete(Long algoId, Long itemId);

    AlgorithmItem findByIds(Long algoId, Long itemId);

    List<Item> findItemsByAlgo(Long algoId);

    List<Algorithm> findAlgosByItem(Long itemId);
}
