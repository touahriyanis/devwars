package fr.esgi.devwars.algorithm_item.infrastructure.web.adapter;

import fr.esgi.devwars.algorithm_item.domain.model.AlgorithmItem;
import fr.esgi.devwars.algorithm_item.infrastructure.web.response.AlgorithmItemResponse;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class AlgorithmItemAdapter {
    public AlgorithmItemResponse toResponse(AlgorithmItem algorithmItem) {
        return new AlgorithmItemResponse()
                .setAlgorithm(algorithmItem.getAlgorithm())
                .setItem(algorithmItem.getItem());
    }

    public List<AlgorithmItemResponse> toResponses(List<AlgorithmItem> algorithmItems) {
        return algorithmItems
                .stream()
                .map(this::toResponse)
                .collect(Collectors.toList());
    }


}
