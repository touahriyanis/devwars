package fr.esgi.devwars.algorithm_item.infrastructure.dataprovider.entity;

import java.io.Serializable;

public class AlgorithmItemId implements Serializable {
    private Long algorithmId;
    private Long itemId;
}
