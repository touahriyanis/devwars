package fr.esgi.devwars.algorithm_item.use_case;

import fr.esgi.devwars.algorithm_item.domain.dao.AlgorithmItemDAO;
import fr.esgi.devwars.algorithm_item.domain.model.AlgorithmItem;
import fr.esgi.devwars.algorithm.domain.dao.AlgorithmDAO;
import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import fr.esgi.devwars.item.domain.dao.ItemDAO;
import fr.esgi.devwars.item.domain.model.Item;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FindAllAlgorithmsByItem {

    private final AlgorithmItemDAO algorithmItemDAO;
    private final ItemDAO itemDAO;

    public List<Algorithm> execute(Long itemId) {
        checkIfItemExists(itemId);
        return algorithmItemDAO.findAlgosByItem(itemId);
    }

    private void checkIfItemExists(Long id) {
        Item exist = itemDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("Item with id %s not found", id));
        }
    }

}
