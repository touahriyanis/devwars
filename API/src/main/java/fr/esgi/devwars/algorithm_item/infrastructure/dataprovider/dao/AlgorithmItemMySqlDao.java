package fr.esgi.devwars.algorithm_item.infrastructure.dataprovider.dao;


import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.algorithm.infrastructure.dataprovider.mapper.AlgorithmMapper;
import fr.esgi.devwars.algorithm.infrastructure.dataprovider.repository.AlgorithmRepository;
import fr.esgi.devwars.algorithm_item.domain.dao.AlgorithmItemDAO;
import fr.esgi.devwars.algorithm_item.domain.model.AlgorithmItem;
import fr.esgi.devwars.algorithm_item.infrastructure.dataprovider.entity.AlgorithmItemEntity;
import fr.esgi.devwars.algorithm_item.infrastructure.dataprovider.mapper.AlgorithmItemMapper;
import fr.esgi.devwars.algorithm_item.infrastructure.dataprovider.repository.AlgorithmItemRepository;
import fr.esgi.devwars.item.domain.model.Item;
import fr.esgi.devwars.item.infrastructure.dataprovider.mapper.ItemMapper;
import fr.esgi.devwars.item.infrastructure.dataprovider.repository.ItemRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AlgorithmItemMySqlDao implements AlgorithmItemDAO {

    private final AlgorithmItemRepository algorithmItemRepository;
    private final AlgorithmRepository algorithmRepository;
    private final ItemRepository itemRepository;
    private final AlgorithmItemMapper algorithmItemMapper;
    private final AlgorithmMapper algorithmMapper;
    private final ItemMapper itemMapper;

    @Override
    public void create(AlgorithmItem algorithmItem) {
        AlgorithmItemEntity entity = algorithmItemMapper.toEntity(algorithmItem);
        algorithmItemRepository.save(entity);
    }

    @Override
    public void delete(Long algoId, Long itemId) {
        algorithmItemRepository.findByAlgorithmIdAndItemId(algoId, itemId)
                .ifPresent(algorithmItemRepository::delete);
    }

    @Override
    public AlgorithmItem findByIds(Long algoId, Long itemId) {
        return algorithmItemRepository.findByAlgorithmIdAndItemId(algoId, itemId)
                .map(algorithmItemMapper::toDomain)
                .orElse(null);
    }

    @Override
    public List<Item> findItemsByAlgo(Long algoId) {
        var itemIds = algorithmItemRepository.findByAlgorithmId(algoId)
                .stream().map(AlgorithmItemEntity::getItemId)
                .collect(Collectors.toList());
        return itemRepository.findAllById(itemIds)
                .stream()
                .map(itemMapper::toDomain)
                .collect(Collectors.toList());
    }

    @Override
    public List<Algorithm> findAlgosByItem(Long itemId) {
        var algoIds = algorithmItemRepository.findByItemId(itemId)
                .stream().map(AlgorithmItemEntity::getAlgorithmId)
                .collect(Collectors.toList());
        return algorithmRepository.findAllById(algoIds)
                .stream()
                .map(algorithmMapper::toDomain)
                .collect(Collectors.toList());
    }
}
