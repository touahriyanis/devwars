package fr.esgi.devwars.algorithm_item.use_case;

import fr.esgi.devwars.algorithm.domain.dao.AlgorithmDAO;
import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.algorithm_item.domain.dao.AlgorithmItemDAO;
import fr.esgi.devwars.algorithm_item.domain.model.AlgorithmItem;
import fr.esgi.devwars.common.domain.exception.AlreadyExistsException;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import fr.esgi.devwars.item.domain.dao.ItemDAO;
import fr.esgi.devwars.item.domain.model.Item;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AddAlgorithmItem {

    private final AlgorithmItemDAO algorithmItemDAO;
    private final AlgorithmDAO algorithmDAO;
    private final ItemDAO itemDAO;

    public AlgorithmItem execute(Long algoId, Long itemId) {
        Algorithm algorithm = checkIfAlgoExists(algoId);
        Item item = checkIfItemExists(itemId);
        checkIfAlgoItemAlreadyExists(algoId, itemId);

        var algorithmItem = new AlgorithmItem()
                .setAlgorithm(algorithm)
                .setItem(item);

        algorithmItemDAO.create(algorithmItem);
        return algorithmItem;
    }

    private Algorithm checkIfAlgoExists(Long id) {
        Algorithm exist = algorithmDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("Algo with id %s not found", id));
        }
        return exist;
    }

    private Item checkIfItemExists(Long id) {
        Item exist = itemDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("Item with id %s not found", id));
        }
        return exist;
    }

    private void checkIfAlgoItemAlreadyExists(Long algoId, Long itemId) {
        AlgorithmItem exist = algorithmItemDAO.findByIds(algoId, itemId);
        if (exist != null) {
            throw new AlreadyExistsException(String.format("Relation already exists between algorithm %s and item %s", algoId, itemId));
        }
    }
}
