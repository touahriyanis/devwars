package fr.esgi.devwars.algorithm_item.infrastructure.dataprovider.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.io.Serializable;

@Entity(name = "algo_item")
@Accessors(chain = true)
@Data
@IdClass(AlgorithmItemId.class)
public class AlgorithmItemEntity implements Serializable {
    @Id
    @Column(name = "algo_id")
    private Long algorithmId;

    @Id
    @Column(name = "item_id")
    private Long itemId;
}
