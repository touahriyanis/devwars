package fr.esgi.devwars.algorithm_item.infrastructure.dataprovider.mapper;

import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.algorithm_item.domain.model.AlgorithmItem;
import fr.esgi.devwars.algorithm_item.infrastructure.dataprovider.entity.AlgorithmItemEntity;
import fr.esgi.devwars.item.domain.model.Item;
import org.springframework.stereotype.Component;

@Component
public class AlgorithmItemMapper {
    public AlgorithmItemEntity toEntity(AlgorithmItem algorithmItem) {
        return new AlgorithmItemEntity()
                .setAlgorithmId(algorithmItem.getAlgorithm().getId())
                .setItemId(algorithmItem.getItem().getId());
    }

    public AlgorithmItem toDomain(AlgorithmItemEntity entity) {
        return new AlgorithmItem()
                .setAlgorithm(new Algorithm().setId(entity.getAlgorithmId()))
                .setItem(new Item().setId(entity.getItemId()));
    }
}
