package fr.esgi.devwars.algorithm_item.use_case;

import fr.esgi.devwars.algorithm.domain.dao.AlgorithmDAO;
import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.algorithm_item.domain.dao.AlgorithmItemDAO;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import fr.esgi.devwars.item.domain.model.Item;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FindAllItemsByAlgorithm {

    private final AlgorithmItemDAO algorithmItemDAO;
    private final AlgorithmDAO algorithmDAO;

    public List<Item> execute(Long algoId) {
        checkIfAlgoExists(algoId);
        return algorithmItemDAO.findItemsByAlgo(algoId);
    }

    private void checkIfAlgoExists(Long id) {
        Algorithm exist = algorithmDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("Algo with id %s not found", id));
        }
    }
}
