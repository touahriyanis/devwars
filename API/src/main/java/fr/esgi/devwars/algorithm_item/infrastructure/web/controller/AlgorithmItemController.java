package fr.esgi.devwars.algorithm_item.infrastructure.web.controller;

import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.algorithm.infrastructure.web.adapter.AlgorithmAdapter;
import fr.esgi.devwars.algorithm.infrastructure.web.response.AlgorithmResponse;
import fr.esgi.devwars.algorithm_item.domain.model.AlgorithmItem;
import fr.esgi.devwars.algorithm_item.infrastructure.web.adapter.AlgorithmItemAdapter;
import fr.esgi.devwars.algorithm_item.infrastructure.web.response.AlgorithmItemResponse;
import fr.esgi.devwars.algorithm_item.use_case.*;
import fr.esgi.devwars.item.domain.model.Item;
import fr.esgi.devwars.item.infrastructure.web.adapter.ItemAdapter;
import fr.esgi.devwars.item.infrastructure.web.response.ItemResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;

@RestController
@Validated
@RequestMapping("/api")
@RequiredArgsConstructor
public class AlgorithmItemController {

    private final AlgorithmItemAdapter algorithmItemAdapter;
    private final AlgorithmAdapter algorithmAdapter;
    private final ItemAdapter itemAdapter;
    private final AddAlgorithmItem addAlgorithmItem;
    private final DeleteAlgorithmItem deleteAlgorithmItem;
    private final FindAllAlgorithmsByItem findAllAlgorithmsByItem;
    private final FindAllItemsByAlgorithm findAllItemsByAlgorithm;

    @PutMapping("/algorithms/{algoId}/items/{itemId}")
    public ResponseEntity<AlgorithmItemResponse> addItemToAlgo(
            @PathVariable("algoId") @NotNull @Positive Long algoId,
            @PathVariable("itemId") @NotNull @Positive Long itemId) {

        AlgorithmItem algorithmItem = addAlgorithmItem.execute(algoId, itemId);

        return ResponseEntity
                .ok()
                .body(algorithmItemAdapter.toResponse(algorithmItem));
    }

    @PutMapping("/items/{itemId}/algorithms/{algoId}")
    public ResponseEntity<AlgorithmItemResponse> addAlgoToItem(
            @PathVariable("algoId") @NotNull @Positive Long algoId,
            @PathVariable("itemId") @NotNull @Positive Long itemId) {

        AlgorithmItem algorithmItem = addAlgorithmItem.execute(algoId, itemId);

        return ResponseEntity
                .ok()
                .body(algorithmItemAdapter.toResponse(algorithmItem));
    }

    @DeleteMapping("/algorithms/{algoId}/items/{itemId}")
    public ResponseEntity<AlgorithmItemResponse> deleteItemFromAlgo(
            @PathVariable("algoId") @NotNull @Positive Long algoId,
            @PathVariable("itemId") @NotNull @Positive Long itemId) {

        deleteAlgorithmItem.execute(algoId, itemId);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/items/{itemId}/algorithms/{algoId}")
    public ResponseEntity<AlgorithmItemResponse> deleteAlgoFromItem(
            @PathVariable("algoId") @NotNull @Positive Long algoId,
            @PathVariable("itemId") @NotNull @Positive Long itemId) {

        deleteAlgorithmItem.execute(algoId, itemId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/items/{itemId}/algorithms")
    public ResponseEntity<List<AlgorithmResponse>> findAllAlgosByItem(
            @PathVariable("itemId") @NotNull @Positive Long itemId) {

        List<Algorithm> algorithms = findAllAlgorithmsByItem.execute(itemId);
        return ResponseEntity.ok(algorithmAdapter.toResponses(algorithms));
    }

    @GetMapping("/algorithms/{algoId}/items")
    public ResponseEntity<List<ItemResponse>> findAllItemsByAlgo(
            @PathVariable("algoId") @NotNull @Positive Long algoId) {

        List<Item> items = findAllItemsByAlgorithm.execute(algoId);
        return ResponseEntity.ok(itemAdapter.toResponses(items));
    }


}
