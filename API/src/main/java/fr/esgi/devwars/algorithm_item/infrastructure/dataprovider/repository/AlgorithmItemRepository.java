package fr.esgi.devwars.algorithm_item.infrastructure.dataprovider.repository;


import fr.esgi.devwars.algorithm_item.infrastructure.dataprovider.entity.AlgorithmItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AlgorithmItemRepository extends JpaRepository<AlgorithmItemEntity, Long> {
    Optional<AlgorithmItemEntity> findByAlgorithmIdAndItemId(Long algorithmId, Long itemId);

    List<AlgorithmItemEntity> findByItemId(Long itemId);

    List<AlgorithmItemEntity> findByAlgorithmId(Long algorithmId);
}
