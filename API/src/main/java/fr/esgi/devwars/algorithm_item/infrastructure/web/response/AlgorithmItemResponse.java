package fr.esgi.devwars.algorithm_item.infrastructure.web.response;

import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.item.domain.model.Item;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Accessors(chain = true)
@Data
@RequiredArgsConstructor
public class AlgorithmItemResponse implements Serializable {
    private Algorithm algorithm;
    private Item item;
}
