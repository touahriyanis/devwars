package fr.esgi.devwars.algorithm_item.domain.model;

import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.item.domain.model.Item;
import lombok.Data;
import lombok.experimental.Accessors;


@Accessors(chain = true)
@Data
public class AlgorithmItem {
    private Algorithm algorithm;
    private Item item;
}
