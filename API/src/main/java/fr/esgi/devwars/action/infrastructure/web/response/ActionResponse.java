package fr.esgi.devwars.action.infrastructure.web.response;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Accessors(chain = true)
@Data
@RequiredArgsConstructor
public class ActionResponse implements Serializable {
    private Long id;
    private String name;
    private Integer power;
}
