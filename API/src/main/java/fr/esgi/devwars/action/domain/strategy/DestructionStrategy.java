package fr.esgi.devwars.action.domain.strategy;


import fr.esgi.devwars.action.domain.exception.NoTargetSpecifiedException;
import fr.esgi.devwars.action.domain.model.ActionName;
import fr.esgi.devwars.item.domain.exception.CannotUseItemException;
import fr.esgi.devwars.item.domain.model.Item;
import fr.esgi.devwars.player.domain.model.Player;
import fr.esgi.devwars.user.domain.dao.UserDAO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;


@Component
@RequiredArgsConstructor
public class DestructionStrategy implements ActionStrategy {

    private final UserDAO userDAO;

    @Override
    public void performAction(Player performer, Player opponent, Item item) {

        final var possessor = opponent.getUser();
        if (possessor == null) {
            throw new NoTargetSpecifiedException("No target to use destruction");
        }

        final var possessorItems = userDAO.findItemsByUser(possessor);

        if (!possessorItems.contains(item)) {
            throw new CannotUseItemException(
                    String.format("User with id %s can not use item %s",
                            possessor.getId(),
                            item.getId()
                    ));
        }
        userDAO.deleteItem(possessor, item);
    }


    @Override
    public ActionName getStrategyName() {
        return ActionName.DESTRUCTION;
    }
}
