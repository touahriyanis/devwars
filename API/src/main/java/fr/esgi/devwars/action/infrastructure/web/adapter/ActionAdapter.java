package fr.esgi.devwars.action.infrastructure.web.adapter;

import fr.esgi.devwars.action.domain.model.Action;
import fr.esgi.devwars.action.infrastructure.web.response.ActionResponse;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ActionAdapter {
    public ActionResponse toResponse(Action action) {
        return new ActionResponse()
                .setId(action.getId())
                .setName(String.valueOf(action.getName()))
                .setPower(action.getPower());
    }

    public List<ActionResponse> toResponses(List<Action> actions) {
        return actions
                .stream()
                .map(this::toResponse)
                .collect(Collectors.toList());
    }


}
