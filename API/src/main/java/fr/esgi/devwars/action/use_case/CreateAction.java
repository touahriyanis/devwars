package fr.esgi.devwars.action.use_case;

import fr.esgi.devwars.action.domain.dao.ActionDAO;
import fr.esgi.devwars.action.domain.exception.InvalidActionNameException;
import fr.esgi.devwars.action.domain.model.Action;
import fr.esgi.devwars.action.domain.model.ActionName;
import fr.esgi.devwars.common.domain.exception.AlreadyExistsException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
@RequiredArgsConstructor
public class CreateAction {

    private final ActionDAO actionDAO;

    public Action execute(String name, Integer power) {
        checkIfNameAlreadyExists(name);

        Action action = new Action()
                .setName(getActionName(name))
                .setPower(power);

        return actionDAO.create(action);
    }

    private void checkIfNameAlreadyExists(String name) {
        Action exist = actionDAO.findByName(name);
        if (exist != null) {
            throw new AlreadyExistsException(String.format("Action with name %s already exists", name));
        }
    }

    private ActionName getActionName(String name) {
        final var allActionNames = ActionName.values();
        final var findActionName = Arrays.stream(allActionNames)
                .filter(actionName -> actionName.name().equalsIgnoreCase(name))
                .findFirst();

        if (findActionName.isEmpty()) {
            throw new InvalidActionNameException(String.format("Action with name %s is invalid", name));
        }
        return findActionName.get();
    }
}
