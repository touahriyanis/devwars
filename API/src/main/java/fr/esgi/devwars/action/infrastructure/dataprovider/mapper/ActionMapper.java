package fr.esgi.devwars.action.infrastructure.dataprovider.mapper;

import fr.esgi.devwars.action.domain.model.Action;
import fr.esgi.devwars.action.domain.model.ActionName;
import fr.esgi.devwars.action.infrastructure.dataprovider.entity.ActionEntity;
import org.springframework.stereotype.Component;

@Component
public class ActionMapper {
    public Action toDomain(ActionEntity entity) {
        return new Action()
                .setId(entity.getId())
                .setName(ActionName.valueOf(entity.getName()))
                .setPower(entity.getPower());
    }

    public ActionEntity toEntity(Action action) {
        return new ActionEntity()
                .setName(String.valueOf(action.getName()))
                .setPower(action.getPower());
    }
}
