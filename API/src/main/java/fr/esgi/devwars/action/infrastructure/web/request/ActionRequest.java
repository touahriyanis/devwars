package fr.esgi.devwars.action.infrastructure.web.request;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@Accessors(chain = true)
public class ActionRequest {
    @NotBlank
    private String name;

    @NotNull
    @Positive(message = "Power doit être un nombre positif")
    private Integer power;
}
