package fr.esgi.devwars.action.domain.strategy;

import fr.esgi.devwars.action.domain.model.ActionName;
import fr.esgi.devwars.item.domain.model.Item;
import fr.esgi.devwars.player.domain.model.Player;

public interface ActionStrategy {
    void performAction(Player performer, Player target, Item item);

    ActionName getStrategyName();
}
