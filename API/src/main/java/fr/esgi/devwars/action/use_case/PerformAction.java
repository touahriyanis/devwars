package fr.esgi.devwars.action.use_case;

import fr.esgi.devwars.action.domain.dao.ActionDAO;
import fr.esgi.devwars.action.domain.exception.*;
import fr.esgi.devwars.action.domain.factory.ActionStrategyFactory;
import fr.esgi.devwars.action.domain.model.Action;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import fr.esgi.devwars.game.domain.dao.GameDAO;
import fr.esgi.devwars.game.domain.exception.IncorrectPlayerNumberException;
import fr.esgi.devwars.game.domain.model.Game;
import fr.esgi.devwars.item.domain.dao.ItemDAO;
import fr.esgi.devwars.item.domain.exception.NoGameSpecifiedException;
import fr.esgi.devwars.item.domain.model.Item;
import fr.esgi.devwars.player.domain.dao.PlayerDAO;
import fr.esgi.devwars.player.domain.exception.GameOverException;
import fr.esgi.devwars.player.domain.model.Player;
import fr.esgi.devwars.user.domain.dao.UserDAO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PerformAction {

    private final ActionStrategyFactory actionStrategyFactory;
    private final UserDAO userDAO;
    private final ActionDAO actionDAO;
    private final PlayerDAO playerDAO;
    private final GameDAO gameDAO;
    private final ItemDAO itemDAO;

    private Action getAction(Long actionId) {
        if (actionId == null) {
            throw new NoActionToPerformException("No action to use specified");
        }
        final var exists = actionDAO.findById(actionId);
        if (exists == null) {
            throw new NotFoundException(String.format("Action with id %s not found", actionId));
        }
        return exists;
    }

    public void execute(Long performerId, Long gameId, Long actionId, Long itemId) {
        final var action = getAction(actionId);
        final var game = findGame(gameId);
        assertGameIsNotOver(game);

        final var players = findPlayers(gameId);
        final var performer = getPerformer(players, performerId);
        assertPerformerCanPerformTheAction(performer, action);

        final var target = getTarget(players, performer);
        final var item = getItem(itemId);

        final var actionStrategy = actionStrategyFactory.getStrategy(action.getName());
        actionStrategy.performAction(performer, target, item);
    }

    private Item getItem(Long itemId) {
        if (itemId == null) {
            throw new NoItemSpecifiedException("No item specified for the action");
        }
        final var item = itemDAO.findById(itemId);
        if (item == null) {
            throw new NotFoundException(String.format("Item with id %s not found", itemId));
        }
        return item;
    }

    private Player getTarget(List<Player> players, Player performer) {
        final var target = players.stream()
                .filter(player -> !player.equals(performer))
                .findFirst();

        if (target.isEmpty()) {
            throw new NoTargetSpecifiedException("No target specified for the action");
        }
        return target.get();
    }

    private List<Player> findPlayers(Long gameId) {
        final var players = playerDAO.findAllByGame(gameId);
        if (players.size() != 2) {
            throw new IncorrectPlayerNumberException("There should be 2 players in a game");
        }
        return players;
    }

    private Player getPerformer(List<Player> players, Long performerId) {
        final var performer = players.stream()
                .filter(player -> player.getUser().getId().equals(performerId))
                .findFirst();

        if (performer.isEmpty()) {
            throw new NoPerformerException("No performer specified to do the action");
        }
        return performer.get();
    }

    private void assertPerformerCanPerformTheAction(Player performer, Action action) {
        final var userActions = userDAO.findActionsByUser(performer.getUser());

        if (userActions.stream().noneMatch(userAction -> userAction.equals(action))) {
            throw new CannotUseActionException(String.format("User %s cannot use action %s",
                    performer.getUser().getId(), action.getId()));
        }
    }


    private Game findGame(Long gameId) {
        if (gameId == null) {
            throw new NoGameSpecifiedException("No game specified to use the item");
        }
        final var game = gameDAO.findById(gameId);
        if (game == null) {
            throw new NotFoundException(String.format("Game with id %s not found", gameId));
        }
        return game;
    }

    private void assertGameIsNotOver(Game game) {
        if (game.isOver()) {
            throw new GameOverException(String.format("Game %s is over", game.getId()));
        }
    }
}
