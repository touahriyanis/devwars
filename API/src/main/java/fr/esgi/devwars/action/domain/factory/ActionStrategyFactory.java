package fr.esgi.devwars.action.domain.factory;

import fr.esgi.devwars.action.domain.model.ActionName;
import fr.esgi.devwars.action.domain.strategy.ActionStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Component
public class ActionStrategyFactory {

    private Map<ActionName, ActionStrategy> strategies;

    @Autowired
    public ActionStrategyFactory(Set<ActionStrategy> strategies) {
        instanciateStrategies(strategies);
    }

    private void instanciateStrategies(Set<ActionStrategy> strategiesToInstanciate) {
        strategies = new HashMap<>();
        strategiesToInstanciate.forEach(strategie ->
                strategies.put(strategie.getStrategyName(), strategie));
    }

    public ActionStrategy getStrategy(ActionName strategyName) {
        return strategies.get(strategyName);
    }
}
