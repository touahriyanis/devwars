package fr.esgi.devwars.action.use_case;

import fr.esgi.devwars.action.domain.dao.ActionDAO;
import fr.esgi.devwars.action.domain.model.Action;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FindActionById {

    private final ActionDAO actionDAO;

    public Action execute(Long id) {
        var action = actionDAO.findById(id);
        if (action == null) {
            throw new NotFoundException(String.format("Action with id %s not found", id));
        }
        return action;
    }
}
