package fr.esgi.devwars.action.use_case;

import fr.esgi.devwars.action.domain.dao.ActionDAO;
import fr.esgi.devwars.action.domain.model.Action;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FindAllActions {

    private final ActionDAO actionDAO;

    public List<Action> execute() {
        return actionDAO.findAll();
    }
}
