package fr.esgi.devwars.action.domain.exception;

public class InvalidActionNameException extends RuntimeException {
    public InvalidActionNameException(String message) {
        super(message);
    }
}
