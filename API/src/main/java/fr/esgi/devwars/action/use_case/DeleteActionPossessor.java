package fr.esgi.devwars.action.use_case;


import fr.esgi.devwars.common.domain.exception.NotFoundException;
import fr.esgi.devwars.action.domain.dao.ActionDAO;
import fr.esgi.devwars.action.domain.model.Action;
import fr.esgi.devwars.user.domain.dao.UserDAO;
import fr.esgi.devwars.user.domain.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DeleteActionPossessor {

    private final UserDAO userDAO;
    private final ActionDAO actionDAO;

    public void execute(Long userId, Long actionId) {
        final var possessor = checkIfUserExists(userId);
        final var action = checkIfActionExists(actionId);
        checkIfActionHasUser(action, possessor.getId());

        actionDAO.deletePossessor(action, possessor);
    }

    private User checkIfUserExists(Long id) {
        User exist = userDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("User with id %s not found", id));
        }
        return exist;
    }

    private Action checkIfActionExists(Long id) {
        Action exist = actionDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("Action with id %s not found", id));
        }
        return exist;
    }

    private void checkIfActionHasUser(Action action, Long userId) {
        List<User> possessors = actionDAO.findActionPossessors(action);
        if (possessors.stream().noneMatch(possessor -> possessor.getId().equals(userId))) {
            throw new NotFoundException(String.format("No relation between user %s and action %s", userId, action.getId()));
        }
    }
}
