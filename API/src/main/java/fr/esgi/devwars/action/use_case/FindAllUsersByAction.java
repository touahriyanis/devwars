package fr.esgi.devwars.action.use_case;

import fr.esgi.devwars.common.domain.exception.NotFoundException;
import fr.esgi.devwars.action.domain.dao.ActionDAO;
import fr.esgi.devwars.action.domain.model.Action;
import fr.esgi.devwars.user.domain.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FindAllUsersByAction {

    private final ActionDAO actionDAO;

    public List<User> execute(Long actionId) {
        final var action = checkIfActionExists(actionId);
        return actionDAO.findActionPossessors(action);
    }

    private Action checkIfActionExists(Long id) {
        Action exist = actionDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("Action with id %s not found", id));
        }
        return exist;
    }

}
