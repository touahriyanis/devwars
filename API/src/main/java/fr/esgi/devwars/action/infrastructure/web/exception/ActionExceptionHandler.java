package fr.esgi.devwars.action.infrastructure.web.exception;

import fr.esgi.devwars.action.domain.exception.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@RestControllerAdvice
public class ActionExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(NoPerformerException.class)
    public ResponseEntity<String> on(NoPerformerException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NoActionToPerformException.class)
    public ResponseEntity<String> on(NoActionToPerformException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NoTargetSpecifiedException.class)
    public ResponseEntity<String> on(NoTargetSpecifiedException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(CannotUseActionException.class)
    public ResponseEntity<String> on(CannotUseActionException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(NoItemSpecifiedException.class)
    public ResponseEntity<String> on(NoItemSpecifiedException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InvalidActionNameException.class)
    public ResponseEntity<String> on(InvalidActionNameException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
