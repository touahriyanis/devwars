package fr.esgi.devwars.action.domain.model;

public enum ActionName {
    DESTRUCTION,
    RESURRECTION
}
