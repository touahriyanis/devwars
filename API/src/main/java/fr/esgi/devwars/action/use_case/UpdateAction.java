package fr.esgi.devwars.action.use_case;

import fr.esgi.devwars.action.domain.dao.ActionDAO;
import fr.esgi.devwars.action.domain.exception.InvalidActionNameException;
import fr.esgi.devwars.action.domain.model.Action;
import fr.esgi.devwars.action.domain.model.ActionName;
import fr.esgi.devwars.common.domain.exception.AlreadyExistsException;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
@RequiredArgsConstructor
public class UpdateAction {

    private final ActionDAO actionDAO;

    public Action execute(Long id, String name, Integer power) {
        checkIfExists(id);
        checkIfNameAlreadyExists(name, id);

        Action action = new Action()
                .setId(id)
                .setName(getActionName(name))
                .setPower(power);

        return actionDAO.update(action);
    }

    private void checkIfExists(Long id) {
        var action = actionDAO.findById(id);
        if (action == null) {
            throw new NotFoundException(String.format("Action with id %s not found", id));
        }
    }

    private void checkIfNameAlreadyExists(String name, Long id) {
        Action exist = actionDAO.findByName(name);
        if (exist != null && !exist.getId().equals(id)) {
            throw new AlreadyExistsException(String.format("Action with name %s already exists", name));
        }
    }

    private ActionName getActionName(String name) {
        final var allActionNames = ActionName.values();
        final var findActionName = Arrays.stream(allActionNames)
                .filter(actionName -> actionName.name().equalsIgnoreCase(name))
                .findFirst();

        if (findActionName.isEmpty()) {
            throw new InvalidActionNameException(String.format("Action with name %s is invalid", name));
        }
        return findActionName.get();
    }
}
