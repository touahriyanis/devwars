package fr.esgi.devwars.action.infrastructure.web.controller;

import fr.esgi.devwars.action.domain.model.Action;
import fr.esgi.devwars.action.infrastructure.web.adapter.ActionAdapter;
import fr.esgi.devwars.action.infrastructure.web.request.ActionRequest;
import fr.esgi.devwars.action.infrastructure.web.response.ActionResponse;
import fr.esgi.devwars.action.use_case.*;
import fr.esgi.devwars.user.domain.model.User;
import fr.esgi.devwars.user.infrastructure.web.adapter.UserAdapter;
import fr.esgi.devwars.user.infrastructure.web.response.UserResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.net.URI;
import java.util.List;

@RestController
@Validated
@RequestMapping("/api/actions")
@RequiredArgsConstructor
public class ActionController {

    private final ActionAdapter adapter;
    private final UserAdapter userAdapter;
    private final CreateAction createAction;
    private final UpdateAction updateAction;
    private final FindActionById findActionById;
    private final FindAllActions findAllActions;
    private final DeleteActionById deleteActionById;
    private final DeleteActionPossessor deleteActionPossessor;
    private final FindAllUsersByAction findAllUsersByAction;
    private final AddActionPossessor addActionPossessor;

    @PostMapping
    public ResponseEntity<ActionResponse> create(@Valid @RequestBody ActionRequest request) {

        Action action = createAction.execute(request.getName(), request.getPower());

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(action.getId())
                .toUri();

        return ResponseEntity
                .created(uri)
                .body(adapter.toResponse(action));
    }

    @PutMapping("/{id}")
    public ResponseEntity<ActionResponse> update(
            @PathVariable("id") @NotNull @PositiveOrZero Long id,
            @Valid @RequestBody ActionRequest request) {

        Action action = updateAction.execute(id, request.getName(), request.getPower());

        return ResponseEntity
                .ok()
                .body(adapter.toResponse(action));
    }

    @GetMapping
    public ResponseEntity<List<ActionResponse>> findAll() {
        List<Action> actions = findAllActions.execute();
        return ResponseEntity.ok(adapter.toResponses(actions));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ActionResponse> findById(@PathVariable("id") @NotNull @PositiveOrZero Long id) {
        Action action = findActionById.execute(id);
        return ResponseEntity.ok(adapter.toResponse(action));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ActionResponse> deleteById(@PathVariable("id") @NotNull @PositiveOrZero Long id) {
        deleteActionById.execute(id);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{actionId}/users/{userId}")
    public ResponseEntity<UserResponse> deleteUserFromAction(
            @PathVariable("userId") @NotNull @Positive Long userId,
            @PathVariable("actionId") @NotNull @Positive Long actionId) {

        deleteActionPossessor.execute(userId, actionId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{actionId}/users")
    public ResponseEntity<List<UserResponse>> findAllUsersByAction(
            @PathVariable("actionId") @NotNull @Positive Long actionId) {

        List<User> users = findAllUsersByAction.execute(actionId);
        return ResponseEntity.ok(userAdapter.toResponses(users));
    }

    @PutMapping("/{actionId}/users/{userId}")
    public ResponseEntity<UserResponse> addUserToAction(
            @PathVariable("userId") @NotNull @Positive Long userId,
            @PathVariable("actionId") @NotNull @Positive Long actionId) {

        User user = addActionPossessor.execute(userId, actionId);

        return ResponseEntity
                .ok()
                .body(userAdapter.toResponse(user));
    }

}
