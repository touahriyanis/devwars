package fr.esgi.devwars.action.domain.dao;

import fr.esgi.devwars.action.domain.model.Action;
import fr.esgi.devwars.user.domain.model.User;

import java.util.List;

public interface ActionDAO {
    Action create(Action action);

    Action findById(Long id);

    Action findByName(String name);

    List<Action> findAll();

    Action update(Action action);

    void delete(Long id);

    List<User> findActionPossessors(Action action);

    void addPossessor(Action action, User possessor);

    void deletePossessor(Action action, User possessor);
}
