package fr.esgi.devwars.action.domain.exception;

public class NoTargetSpecifiedException extends RuntimeException {
    public NoTargetSpecifiedException(String message) {
        super(message);
    }
}
