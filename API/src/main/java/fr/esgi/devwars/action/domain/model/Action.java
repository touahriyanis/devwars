package fr.esgi.devwars.action.domain.model;

import fr.esgi.devwars.user.domain.model.User;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Accessors(chain = true)
@Data
public class Action {
    private Long id;
    private ActionName name;
    private Integer power;
    private List<User> possessors;
}
