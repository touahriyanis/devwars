package fr.esgi.devwars.action.domain.exception;

public class NoActionToPerformException extends RuntimeException {
    public NoActionToPerformException(String message) {
        super(message);
    }
}
