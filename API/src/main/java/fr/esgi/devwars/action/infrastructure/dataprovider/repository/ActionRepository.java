package fr.esgi.devwars.action.infrastructure.dataprovider.repository;

import fr.esgi.devwars.action.infrastructure.dataprovider.entity.ActionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActionRepository extends JpaRepository<ActionEntity, Long> {
    ActionEntity findByName(String name);
}
