package fr.esgi.devwars.action.domain.exception;

public class NoItemSpecifiedException extends RuntimeException {
    public NoItemSpecifiedException(String message) {
        super(message);
    }
}
