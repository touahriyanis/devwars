package fr.esgi.devwars.action.domain.strategy;

import fr.esgi.devwars.action.domain.exception.NoItemSpecifiedException;
import fr.esgi.devwars.action.domain.exception.NoTargetSpecifiedException;
import fr.esgi.devwars.action.domain.model.ActionName;
import fr.esgi.devwars.item.domain.exception.AlreadyHaveItemException;
import fr.esgi.devwars.item.domain.model.Item;
import fr.esgi.devwars.player.domain.model.Player;
import fr.esgi.devwars.user.domain.dao.UserDAO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ResurrectionStrategy implements ActionStrategy {

    private final UserDAO userDAO;

    @Override
    public void performAction(Player performer, Player opponent, Item lostItem) {

        final var possessor = performer.getUser();
        if (possessor == null) {
            throw new NoTargetSpecifiedException("No target specified to use resurrection");
        }

        final var items = userDAO.findItemsByUser(possessor);
        if (items == null) {
            throw new NoItemSpecifiedException("No item specified to use resurrection");
        }
        if (items.contains(lostItem)) {
            throw new AlreadyHaveItemException(String.format("User %s already have item %s",
                    possessor.getId(),
                    lostItem.getId()
            ));
        }
        userDAO.addItem(possessor, lostItem);
    }

    @Override
    public ActionName getStrategyName() {
        return ActionName.RESURRECTION;
    }
}
