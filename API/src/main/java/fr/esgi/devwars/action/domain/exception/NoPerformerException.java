package fr.esgi.devwars.action.domain.exception;

public class NoPerformerException extends RuntimeException {
    public NoPerformerException(String message) {
        super(message);
    }
}
