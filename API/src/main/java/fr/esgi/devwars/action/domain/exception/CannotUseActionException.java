package fr.esgi.devwars.action.domain.exception;

public class CannotUseActionException extends RuntimeException {
    public CannotUseActionException(String message) {
        super(message);
    }
}
