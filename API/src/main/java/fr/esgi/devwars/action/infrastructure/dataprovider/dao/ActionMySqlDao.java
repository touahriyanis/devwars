package fr.esgi.devwars.action.infrastructure.dataprovider.dao;

import fr.esgi.devwars.action.domain.dao.ActionDAO;
import fr.esgi.devwars.action.domain.model.Action;
import fr.esgi.devwars.action.infrastructure.dataprovider.entity.ActionEntity;
import fr.esgi.devwars.action.infrastructure.dataprovider.mapper.ActionMapper;
import fr.esgi.devwars.action.infrastructure.dataprovider.repository.ActionRepository;
import fr.esgi.devwars.user.domain.model.User;
import fr.esgi.devwars.user.infrastructure.dataprovider.entity.UserActionEntity;
import fr.esgi.devwars.user.infrastructure.dataprovider.mapper.UserActionMapper;
import fr.esgi.devwars.user.infrastructure.dataprovider.mapper.UserMapper;
import fr.esgi.devwars.user.infrastructure.dataprovider.repository.UserActionRepository;
import fr.esgi.devwars.user.infrastructure.dataprovider.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ActionMySqlDao implements ActionDAO {

    private final ActionRepository repository;
    private final ActionMapper mapper;
    private final UserActionRepository userActionRepository;
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final UserActionMapper userActionMapper;

    @Override
    public Action create(Action action) {
        ActionEntity entity = mapper.toEntity(action);
        entity = repository.save(entity);
        return mapper.toDomain(entity);
    }

    @Override
    public Action findById(Long id) {
        return repository.findById(id)
                .map(mapper::toDomain)
                .orElse(null);
    }

    @Override
    public Action findByName(String name) {
        return Optional.ofNullable(repository.findByName(name))
                .map(mapper::toDomain)
                .orElse(null);
    }

    @Override
    public List<Action> findAll() {
        return repository.findAll()
                .stream()
                .map(mapper::toDomain)
                .collect(Collectors.toList());
    }

    @Override
    public Action update(Action action) {
        ActionEntity actionEntity = repository.getOne(action.getId());
        actionEntity.setName(String.valueOf(action.getName()));
        actionEntity.setPower(action.getPower());
        return mapper.toDomain(repository.save(actionEntity));
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }

    @Override
    public List<User> findActionPossessors(Action action) {
        List<Long> ids = userActionRepository.findByActionId(action.getId())
                .stream()
                .map(UserActionEntity::getUserId)
                .collect(Collectors.toList());

        return userRepository.findAllById(ids)
                .stream().map(userMapper::toDomain)
                .collect(Collectors.toList());
    }

    @Override
    public void addPossessor(Action action, User possessor) {
        UserActionEntity entity = userActionMapper.toEntity(possessor, action);
        userActionRepository.save(entity);
    }

    @Override
    public void deletePossessor(Action action, User possessor) {
        userActionRepository.findByUserIdAndActionId(possessor.getId(), action.getId())
                .ifPresent(userActionRepository::delete);
    }
}
