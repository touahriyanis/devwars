package fr.esgi.devwars.resolution.use_case;

import fr.esgi.devwars.action.domain.dao.ActionDAO;
import fr.esgi.devwars.algorithm.domain.dao.AlgorithmDAO;
import fr.esgi.devwars.item.domain.dao.ItemDAO;
import fr.esgi.devwars.resolution.domain.model.Resolution;
import fr.esgi.devwars.user.domain.dao.UserDAO;
import fr.esgi.devwars.user.domain.model.User;
import fr.esgi.devwars.resolution.domain.dao.ResolutionDAO;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FindAllResolutionsByUser {

    private final ResolutionDAO resolutionDAO;
    private final UserDAO userDAO;
    private final AlgorithmDAO algorithmDAO;
    private final ItemDAO itemDAO;
    private final ActionDAO actionDAO;

    public List<Resolution> execute(Long userId) {
        checkIfUserExists(userId);
        final var resolutions = resolutionDAO.findResolutionsByUser(userId);

        for (Resolution resolution : resolutions) {
            final var algo = algorithmDAO.findById(resolution.getAlgorithm().getId());
            resolution.setAlgorithm(algo);

            final var item = itemDAO.findById(resolution.getItem().getId());
            resolution.setItem(item);

            final var action = resolution.getAction();
            if (action != null) {
                resolution.setAction(actionDAO.findById(action.getId()));
            }
        }

        return resolutions;
    }

    private void checkIfUserExists(Long id) {
        User exist = userDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("User with id %s not found", id));
        }
    }
}
