package fr.esgi.devwars.resolution.infrastructure.web.adapter;

import fr.esgi.devwars.resolution.domain.model.Resolution;
import fr.esgi.devwars.resolution.infrastructure.web.response.ResolutionsByAlgoResponse;
import fr.esgi.devwars.resolution.infrastructure.web.response.ResolutionsByUserResponse;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ResolutionsByAlgoAdapter {
    public ResolutionsByAlgoResponse toResponse(Resolution resolution) {
        var response = new ResolutionsByAlgoResponse()
                .setUserId(resolution.getUser().getId())
                .setGameId(resolution.getGame().getId())
                .setResolutionTime(resolution.getResolutionTime())
                .setSolved(resolution.getSolved())
                .setLinterErrors(resolution.getLinterErrors());

        return setItemOrAction(response, resolution);
    }

    public List<ResolutionsByAlgoResponse> toResponses(List<Resolution> resolutions) {
        return resolutions
                .stream()
                .map(this::toResponse)
                .collect(Collectors.toList());
    }

    private ResolutionsByAlgoResponse setItemOrAction(ResolutionsByAlgoResponse response, Resolution resolution) {
        if (resolution.getAction() != null) {
            response.setActionId(resolution.getAction().getId());
        }
        if (resolution.getItem() != null) {
            response.setItemId(resolution.getItem().getId());
        }
        return response;
    }


}
