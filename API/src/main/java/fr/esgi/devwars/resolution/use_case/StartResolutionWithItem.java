package fr.esgi.devwars.resolution.use_case;

import fr.esgi.devwars.algorithm.domain.dao.AlgorithmDAO;
import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.common.domain.exception.AlreadyExistsException;
import fr.esgi.devwars.common.domain.exception.ForbiddenException;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import fr.esgi.devwars.game.domain.dao.GameDAO;
import fr.esgi.devwars.game.domain.exception.IncorrectPlayerNumberException;
import fr.esgi.devwars.game.domain.model.Game;
import fr.esgi.devwars.item.domain.dao.ItemDAO;
import fr.esgi.devwars.item.domain.exception.CannotUseItemException;
import fr.esgi.devwars.item.domain.model.Item;
import fr.esgi.devwars.player.domain.dao.PlayerDAO;
import fr.esgi.devwars.player.domain.exception.GameOverException;
import fr.esgi.devwars.player.domain.model.Player;
import fr.esgi.devwars.resolution.domain.dao.ResolutionDAO;
import fr.esgi.devwars.resolution.domain.exception.ComplexitiesDoNotMatchException;
import fr.esgi.devwars.resolution.domain.model.Resolution;
import fr.esgi.devwars.user.domain.dao.UserDAO;
import fr.esgi.devwars.user.domain.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class StartResolutionWithItem {

    private final ResolutionDAO resolutionDAO;
    private final AlgorithmDAO algorithmDAO;
    private final GameDAO gameDAO;
    private final UserDAO userDAO;
    private final PlayerDAO playerDAO;
    private final ItemDAO itemDAO;

    public Resolution execute(Long userId,
                              Long algorithmId,
                              Long gameId,
                              Long itemId) {

        final var user = getUser(userId);
        final var algorithm = getAlgorithm(algorithmId);
        final var game = getGame(gameId);

        checkResolutionAlreadyExists(userId, algorithmId);
        assertGameIsNotOver(game);
        assertPlayersAreTwo(game);

        final var item = findItem(itemId);
        assertAlgoComplexityMatchItemPower(algorithm, item);
        assertCurrentPlayerCanUseItem(user, item);

        final var newResolution =
                buildResolution(user, algorithm, game, item);

        return resolutionDAO.startResolution(newResolution);
    }

    private void assertAlgoComplexityMatchItemPower(Algorithm algorithm, Item item) {
        if (!algorithm.getComplexity().equals(item.getPower())) {
            throw new ComplexitiesDoNotMatchException(
                    String.format("Item power %s does not match algo complexity %s",
                            item.getPower(),
                            algorithm.getComplexity())
            );
        }
    }

    private Resolution buildResolution(User user, Algorithm algorithm,
                                       Game game, Item item) {
        return new Resolution()
                .setUser(user)
                .setAlgorithm(algorithm)
                .setGame(game)
                .setItem(item)
                .setStartedTime(LocalDateTime.now());
    }

    private Game getGame(Long id) {
        Game game = gameDAO.findById(id);
        if (game == null) {
            throw new NotFoundException(String.format("Game with id %s not found", id));
        }
        return game;
    }

    private void assertCurrentPlayerCanUseItem(User currentPlayer, Item item) {
        var userItems = userDAO.findItemsByUser(currentPlayer);

        if (userItems.stream().noneMatch(userItem -> userItem.equals(item))) {

            throw new CannotUseItemException(String.format("User %s cannot use item %s",
                    currentPlayer.getId(),
                    item.getId()));
        }
    }

    private void assertGameIsNotOver(Game game) {
        if (game.isOver()) {
            throw new GameOverException(String.format("Game with id %s is closed", game.getId()));
        }
    }

    private Algorithm getAlgorithm(Long id) {
        Algorithm exist = algorithmDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("Algorithm with id %s not found", id));
        }
        return exist;
    }

    private void checkResolutionAlreadyExists(Long userId, Long algorithmId) {
        final var exists = resolutionDAO.findByIds(userId, algorithmId);
        if (exists != null) {
            throw new AlreadyExistsException(String.format("User %s already resolved algorithm %s", userId, algorithmId));
        }
    }

    private void assertPlayersAreTwo(Game game) {
        if (playerDAO.findAllByGame(game.getId()).size() != 2) {
            throw new IncorrectPlayerNumberException("There should be two different players in the game");
        }
    }

    private Item findItem(Long itemId) {
        final var exists = itemDAO.findById(itemId);
        if (exists == null) {
            throw new NotFoundException(String.format("Item with id %s not found", itemId));
        }
        return exists;
    }

    private User getUser(Long id) {
        User exist = userDAO.findById(id);
        if (exist == null) {
            throw new ForbiddenException(String.format("User %s not allowed", id));
        }
        return exist;
    }
}
