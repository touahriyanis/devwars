package fr.esgi.devwars.resolution.infrastructure.web.controller;

import fr.esgi.devwars.action.use_case.PerformAction;
import fr.esgi.devwars.compiler.domain.model.LinterError;
import fr.esgi.devwars.game.use_case.CheckGameStatus;
import fr.esgi.devwars.item.use_case.UseItem;
import fr.esgi.devwars.player.infrastructure.web.adapter.PlayerAdapter;
import fr.esgi.devwars.player.use_case.RetrievePlayersByGame;
import fr.esgi.devwars.player.use_case.RetrievePlayersByGames;
import fr.esgi.devwars.resolution.infrastructure.web.adapter.ResolutionByGameAdapter;
import fr.esgi.devwars.resolution.infrastructure.web.adapter.ResolutionsByAlgoAdapter;
import fr.esgi.devwars.resolution.infrastructure.web.adapter.ResolutionsByUserAdapter;
import fr.esgi.devwars.resolution.infrastructure.web.request.*;
import fr.esgi.devwars.resolution.infrastructure.web.response.*;
import fr.esgi.devwars.resolution.domain.model.Resolution;
import fr.esgi.devwars.resolution.infrastructure.web.adapter.ResolutionAdapter;
import fr.esgi.devwars.resolution.use_case.*;
import fr.esgi.devwars.user.infrastructure.security.CustomUserDetails;
import fr.esgi.devwars.user.use_case.DeleteActionFromUser;
import fr.esgi.devwars.user.use_case.DeleteItemFromUser;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@Validated
@RequestMapping("/api")
@RequiredArgsConstructor
public class ResolutionController {

    private final ResolutionAdapter resolutionAdapter;
    private final ResolutionsByAlgoAdapter resolutionsByAlgoAdapter;
    private final ResolutionsByUserAdapter resolutionsByUserAdapter;
    private final ResolutionByGameAdapter resolutionByGameAdapter;
    private final SaveResolutionWithAction saveResolutionWithAction;
    private final SaveResolutionWithItem saveResolutionWithItem;
    private final DeleteResolution deleteResolution;
    private final FindAllResolutionsByAlgo findAllResolutionsByAlgorithm;
    private final FindAllResolutionsByUser findAllResolutionsByUser;
    private final FindAllResolutionsByGame findAllResolutionsByGame;
    private final FindOneResolution findOneResolution;
    private final UseItem useItem;
    private final DeleteItemFromUser deleteItemFromUser;
    private final DeleteActionFromUser deleteActionFromUser;
    private final CheckGameStatus checkGameStatus;
    private final PerformAction performAction;
    private final StartResolutionWithItem startResolutionWithItem;
    private final StartResolutionWithAction startResolutionWithAction;
    private final RetrievePlayersByGame retrievePlayersByGame;
    private final PlayerAdapter playerAdapter;
    private final RetrievePlayersByGames retrievePlayersByGames;

    @PostMapping("/resolutions/start/item")
    public ResponseEntity<ResolutionResponse> addStartedResolutionWithItem(
            @Valid @RequestBody StartedResolutionWithItemRequest request) {

        final var loggedUser = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        final var resolution = startResolutionWithItem.execute(loggedUser.getId(),
                request.getAlgorithmId(), request.getGameId(), request.getItemId());

        return ResponseEntity
                .ok()
                .body(resolutionAdapter.toResponse(resolution));
    }

    @PostMapping("/resolutions/start/action")
    public ResponseEntity<ResolutionResponse> addStartedResolutionWithAction(
            @Valid @RequestBody StartedResolutionWithActionRequest request) {

        final var loggedUser = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        final var resolution = startResolutionWithAction.execute(loggedUser.getId(),
                request.getAlgorithmId(), request.getActionId(), request.getItemId(),
                request.getGameId());

        return ResponseEntity
                .ok()
                .body(resolutionAdapter.toResponse(resolution));
    }

    @PutMapping("/resolutions")
    public ResponseEntity<ResolutionResponse> addResolution(
            @Valid @RequestBody ResolutionRequest request) {

        final var loggedUser = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        final var userId = loggedUser.getId();

        final var resolution = findOneResolution.execute(userId, request.getAlgorithmId());
        final var resolutionTime = resolution.getStartedTime().until(LocalDateTime.now(), ChronoUnit.SECONDS);

        if (resolution.getAction() != null) {
            return saveWithAction(userId,
                    request.getAlgorithmId(),
                    resolution.getAction().getId(),
                    resolution.getItem().getId(),
                    resolution.getGame().getId(),
                    resolutionTime,
                    request.getSolved(),
                    request.getLinterErrors());
        }

        return saveWithItem(userId,
                request.getAlgorithmId(),
                resolution.getItem().getId(),
                resolution.getGame().getId(),
                resolutionTime,
                request.getSolved(),
                request.getLinterErrors());
    }

    private ResponseEntity<ResolutionResponse> saveWithAction(Long userId, Long algorithmId, Long actionId,
                                                              Long itemId, Long gameId, Long resolutionTime,
                                                              Boolean solved, List<LinterError> linterErrors) {
        final var resolution = saveResolutionWithAction.execute(userId,
                algorithmId,
                actionId,
                itemId,
                gameId,
                resolutionTime,
                solved,
                linterErrors
        );

        if (Boolean.TRUE.equals(resolution.isSolved())) {
            performAction.execute(userId, gameId, actionId, itemId);
        }

        deleteActionFromUser.execute(userId, actionId);
        checkGameStatus.execute(gameId);

        var player = retrievePlayersByGame.execute(gameId).stream().filter(p -> p.getUser().getId() == userId)
                .map(playerAdapter::toResponse)
                .findAny()
                .orElse(null);

        return ResponseEntity
                .ok()
                .body(resolutionAdapter.toResponse(resolution).setPlayer(player));
    }

    private ResponseEntity<ResolutionResponse> saveWithItem(Long userId, Long algorithmId,
                                                            Long itemId, Long gameId, Long resolutionTime,
                                                            Boolean solved, List<LinterError> linterErrors) {

        final var resolution = saveResolutionWithItem.execute(userId,
                algorithmId,
                itemId,
                gameId,
                resolutionTime,
                solved,
                linterErrors);

        final var errorNumber = linterErrors.stream()
                .mapToInt(LinterError::getErrorNumber)
                .sum();

        if (Boolean.TRUE.equals(resolution.isSolved())) {
            useItem.execute(itemId, gameId, userId, errorNumber);
        }

        deleteItemFromUser.execute(userId, itemId);
        checkGameStatus.execute(gameId);

        var player = retrievePlayersByGame.execute(gameId).stream().filter(p -> p.getUser().getId().equals(userId))
                .map(playerAdapter::toResponse)
                .findAny()
                .orElse(null);

        return ResponseEntity
                .ok()
                .body(resolutionAdapter.toResponse(resolution).setPlayer(player));
    }

    @DeleteMapping("/users/{userId}/algorithms/{algorithmId}")
    public ResponseEntity<ResolutionResponse> deleteAlgorithmFromUser(
            @PathVariable("userId") @NotNull @Positive Long userId,
            @PathVariable("algorithmId") @NotNull @Positive Long algorithmId) {

        deleteResolution.execute(userId, algorithmId);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/algorithms/{algorithmId}/users/{userId}")
    public ResponseEntity<ResolutionResponse> deleteUserFromAlgorithm(
            @PathVariable("userId") @NotNull @Positive Long userId,
            @PathVariable("algorithmId") @NotNull @Positive Long algorithmId) {

        deleteResolution.execute(userId, algorithmId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/algorithms/{algorithmId}/users")
    public ResponseEntity<List<ResolutionsByAlgoResponse>> findAllUsersByAlgorithm(
            @PathVariable("algorithmId") @NotNull @Positive Long algorithmId) {

        List<Resolution> resolutions = findAllResolutionsByAlgorithm.execute(algorithmId);
        return ResponseEntity.ok(resolutionsByAlgoAdapter.toResponses(resolutions));
    }

    @GetMapping("/users/{userId}/algorithms")
    public ResponseEntity<List<ResolutionsByUserResponse>> findAllAlgorithmsByUser(
            @PathVariable("userId") @NotNull @Positive Long userId) {

        List<Resolution> resolutions = findAllResolutionsByUser.execute(userId);
        return ResponseEntity.ok(resolutionsByUserAdapter.toResponses(resolutions));
    }

    @GetMapping("/games/{gameId}/algorithms")
    public ResponseEntity<List<ResolutionsByGameResponse>> findAllAlgorithmsByGame(
            @PathVariable("gameId") @NotNull @Positive Long gameId) {

        List<Resolution> resolutions = findAllResolutionsByGame.execute(gameId);
        return ResponseEntity.ok(resolutionByGameAdapter.toResponses(resolutions));
    }

    @GetMapping("/resolutions/users/{userId}")
    public ResponseEntity<List<ResolutionDetailsResponse>> findDetailsByUser(
            @PathVariable("userId") @NotNull @Positive Long userId) {

        final var resolutions = findAllResolutionsByUser.execute(userId);
        final var gameIds = resolutions.stream()
                .map(resolution -> resolution.getGame().getId())
                .collect(Collectors.toList());
        final var players = retrievePlayersByGames.execute(gameIds);


        return ResponseEntity.ok(resolutionAdapter.toDetailResponses(resolutions, players));
    }
}
