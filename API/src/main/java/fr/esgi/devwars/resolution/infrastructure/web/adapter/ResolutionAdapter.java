package fr.esgi.devwars.resolution.infrastructure.web.adapter;

import fr.esgi.devwars.compiler.domain.model.LinterError;
import fr.esgi.devwars.player.domain.model.Player;
import fr.esgi.devwars.resolution.domain.model.Resolution;
import fr.esgi.devwars.resolution.infrastructure.web.response.ResolutionDetailsResponse;
import fr.esgi.devwars.resolution.infrastructure.web.response.ResolutionResponse;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ResolutionAdapter {
    public ResolutionResponse toResponse(Resolution resolution) {
        var response = new ResolutionResponse()
                .setGameId(resolution.getGame().getId())
                .setResolutionTime(resolution.getResolutionTime())
                .setSolved(resolution.getSolved())
                .setStartedTime(resolution.getStartedTime())
                .setLinterErrors(resolution.getLinterErrors());
        setItemOrAction(response, resolution);
        return response;
    }

    public List<ResolutionResponse> toResponses(List<Resolution> resolutions) {
        return resolutions
                .stream()
                .map(this::toResponse)
                .collect(Collectors.toList());
    }

    private void setItemOrAction(ResolutionResponse response, Resolution resolution) {
        if (resolution.getAction() != null) {
            response.setActionId(resolution.getAction().getId());
        }
        if (resolution.getItem() != null) {
            response.setItemId(resolution.getItem().getId());
        }
    }


    public List<ResolutionDetailsResponse> toDetailResponses(List<Resolution> resolutions, List<Player> players) {
        final var responses = new ArrayList<ResolutionDetailsResponse>();

        for (Resolution resolution : resolutions) {
            final var response = new ResolutionDetailsResponse();

            if (resolution.getAction() != null) {
                response.setActionName(resolution.getAction().getName().toString());
            }

            response.setAlgorithmName(resolution.getAlgorithm().getWording());
            response.setItemName(resolution.getItem().getName());

            response.setResolutionTime(resolution.getResolutionTime());
            response.setSolved(resolution.isSolved());
            response.setErrorNumber(resolution.getLinterErrors().stream().mapToLong(LinterError::getErrorNumber).sum());

            final var playersInGame = players.stream()
                    .filter(player -> player.getGame().getId().equals(resolution.getGame().getId()))
                    .collect(Collectors.toList());
            response.setPlayers(playersInGame);

            responses.add(response);
        }

        return responses;
    }
}
