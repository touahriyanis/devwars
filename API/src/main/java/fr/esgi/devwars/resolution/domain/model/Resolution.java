package fr.esgi.devwars.resolution.domain.model;

import fr.esgi.devwars.action.domain.model.Action;
import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.compiler.domain.model.LinterError;
import fr.esgi.devwars.game.domain.model.Game;
import fr.esgi.devwars.item.domain.model.Item;
import fr.esgi.devwars.user.domain.model.User;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.util.List;

@Accessors(chain = true)
@Data
public class Resolution {
    private User user;
    private Algorithm algorithm;
    private Game game;
    private Item item;
    private Action action;
    private Long resolutionTime;
    private Boolean solved;
    private List<LinterError> linterErrors;
    private LocalDateTime startedTime;

    public Boolean isSolved() {
        return Boolean.TRUE.equals(solved);
    }
}
