package fr.esgi.devwars.resolution.infrastructure.web.request;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

@Data
@Accessors(chain = true)
public class StartedResolutionWithItemRequest {
    @NotNull
    @Positive
    private Long gameId;

    @NotNull
    @Positive
    private Long algorithmId;

    @NotNull
    @Positive
    private Long itemId;
}
