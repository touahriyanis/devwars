package fr.esgi.devwars.resolution.domain.exception;

public class NoneActionOrItemSpecifiedException extends RuntimeException {
    public NoneActionOrItemSpecifiedException(String message) {
        super(message);
    }
}
