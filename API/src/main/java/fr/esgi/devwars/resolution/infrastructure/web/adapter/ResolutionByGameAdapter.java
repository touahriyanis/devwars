package fr.esgi.devwars.resolution.infrastructure.web.adapter;

import fr.esgi.devwars.resolution.domain.model.Resolution;
import fr.esgi.devwars.resolution.infrastructure.web.response.ResolutionsByGameResponse;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ResolutionByGameAdapter {
    public ResolutionsByGameResponse toResponse(Resolution resolution) {
        var response = new ResolutionsByGameResponse()
                .setAlgoId(resolution.getAlgorithm().getId())
                .setUserId(resolution.getUser().getId())
                .setResolutionTime(resolution.getResolutionTime())
                .setSolved(resolution.getSolved())
                .setLinterErrors(resolution.getLinterErrors());

        return setItemOrAction(response, resolution);
    }

    public List<ResolutionsByGameResponse> toResponses(List<Resolution> resolutions) {
        return resolutions
                .stream()
                .map(this::toResponse)
                .collect(Collectors.toList());
    }

    private ResolutionsByGameResponse setItemOrAction(ResolutionsByGameResponse response, Resolution resolution) {
        if (resolution.getAction() != null) {
            response.setActionId(resolution.getAction().getId());
        }
        if (resolution.getItem() != null) {
            response.setItemId(resolution.getItem().getId());
        }
        return response;
    }

}
