package fr.esgi.devwars.resolution.infrastructure.dataprovider.repository;


import fr.esgi.devwars.resolution.infrastructure.dataprovider.entity.ResolutionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ResolutionRepository extends JpaRepository<ResolutionEntity, Long> {
    Optional<ResolutionEntity> findByUserIdAndAlgoId(Long userId, Long algoId);

    List<ResolutionEntity> findByAlgoId(Long algoId);

    //    @Query("SELECT r, a, i, action, g FROM resolution r " +
//            "INNER JOIN algorithm a ON r.algoId = a.id " +
//            "INNER JOIN item i ON i.id = r.itemId " +
//            "INNER JOIN action action ON action.id = r.actionId " +
//            "INNER JOIN game g ON g.id = r.gameId ")
    List<ResolutionEntity> findByUserId(Long userId);

    List<ResolutionEntity> findByGameId(Long gameId);
}
