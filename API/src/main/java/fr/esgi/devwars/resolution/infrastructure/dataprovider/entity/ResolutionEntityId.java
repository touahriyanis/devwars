package fr.esgi.devwars.resolution.infrastructure.dataprovider.entity;

import java.io.Serializable;

public class ResolutionEntityId implements Serializable {
    private Long userId;
    private Long algoId;
}
