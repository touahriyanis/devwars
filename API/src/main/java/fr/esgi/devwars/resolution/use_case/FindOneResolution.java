package fr.esgi.devwars.resolution.use_case;

import fr.esgi.devwars.common.domain.exception.NotFoundException;
import fr.esgi.devwars.resolution.domain.dao.ResolutionDAO;
import fr.esgi.devwars.resolution.domain.model.Resolution;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FindOneResolution {

    private final ResolutionDAO resolutionDAO;

    public Resolution execute(Long userId, Long algorithmId) {
        return findResolution(userId, algorithmId);
    }

    private Resolution findResolution(Long userId, Long algorithmId) {
        final var resolution = resolutionDAO.findByIds(userId, algorithmId);
        if (resolution == null) {
            throw new NotFoundException(String.format("Resolution with user %s and algo %s not found",
                    userId, algorithmId));
        }
        return resolution;
    }

}
