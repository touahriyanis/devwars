package fr.esgi.devwars.resolution.infrastructure.web.response;

import fr.esgi.devwars.compiler.domain.model.LinterError;
import fr.esgi.devwars.player.infrastructure.web.response.PlayerResponse;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Accessors(chain = true)
@Data
@RequiredArgsConstructor
public class ResolutionResponse implements Serializable {
    private Long gameId;
    private Long itemId;
    private Long actionId;
    private Long resolutionTime;
    private LocalDateTime startedTime;
    private Boolean solved;
    private PlayerResponse player;
    private List<LinterError> linterErrors;
}
