package fr.esgi.devwars.resolution.infrastructure.web.request;

import fr.esgi.devwars.compiler.domain.model.LinterError;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.util.List;

@Data
@Accessors(chain = true)
public class ResolutionRequest {
    @NotNull
    @Positive
    private Long algorithmId;

    @NotNull
    private Boolean solved;

    @NotNull
    private List<LinterError> linterErrors;
}
