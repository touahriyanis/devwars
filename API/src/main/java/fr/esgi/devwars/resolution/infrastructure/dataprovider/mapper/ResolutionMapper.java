package fr.esgi.devwars.resolution.infrastructure.dataprovider.mapper;

import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.compiler.domain.model.LinterError;
import fr.esgi.devwars.game.domain.model.Game;
import fr.esgi.devwars.item.domain.model.Item;
import fr.esgi.devwars.user.domain.model.User;
import fr.esgi.devwars.resolution.domain.model.Resolution;
import fr.esgi.devwars.resolution.infrastructure.dataprovider.entity.ResolutionEntity;
import fr.esgi.devwars.action.domain.model.Action;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collections;

@Component
public class ResolutionMapper {
    public ResolutionEntity toEntity(Resolution resolution) {
        var entity = new ResolutionEntity()
                .setUserId(resolution.getUser().getId())
                .setAlgoId(resolution.getAlgorithm().getId())
                .setGameId(resolution.getGame().getId())
                .setResolutionTime(resolution.getResolutionTime())
                .setSolved(resolution.getSolved())
                .setStartedTime(resolution.getStartedTime())
                .setLinterErrors(resolution.getLinterErrors().stream()
                        .mapToInt(LinterError::getErrorNumber)
                        .sum());
        return setItemOrActionForEntity(entity, resolution);
    }

    public ResolutionEntity toStartedResolutionEntity(Resolution resolution) {
        var entity = new ResolutionEntity()
                .setUserId(resolution.getUser().getId())
                .setAlgoId(resolution.getAlgorithm().getId())
                .setGameId(resolution.getGame().getId())
                .setStartedTime(resolution.getStartedTime());
        return setItemOrActionForEntity(entity, resolution);
    }

    public Resolution toDomain(ResolutionEntity entity) {
        var resolution = new Resolution()
                .setUser(new User().setId(entity.getUserId()))
                .setAlgorithm(new Algorithm().setId(entity.getAlgoId()))
                .setGame(new Game().setId(entity.getGameId()))
                .setResolutionTime(entity.getResolutionTime())
                .setStartedTime(entity.getStartedTime())
                .setLinterErrors(Collections.singletonList(new LinterError().setErrorNumber(entity.getLinterErrors())))
                .setSolved(entity.getSolved());
        return setItemOrActionForDomain(entity, resolution);
    }

    private Resolution setItemOrActionForDomain(ResolutionEntity entity, Resolution resolution) {
        if (entity.getActionId() != null) {
            resolution.setAction(new Action().setId(entity.getActionId()));
        }
        if (entity.getItemId() != null) {
            resolution.setItem(new Item().setId(entity.getItemId()));
        }
        return resolution;
    }

    private ResolutionEntity setItemOrActionForEntity(ResolutionEntity entity, Resolution resolution) {

        if (resolution.getItem() != null) {
            entity.setItemId(resolution.getItem().getId());
        }
        if (resolution.getAction() != null) {
            entity.setActionId(resolution.getAction().getId());
        }
        return entity;
    }
}
