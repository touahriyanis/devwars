package fr.esgi.devwars.resolution.use_case;

import fr.esgi.devwars.common.domain.exception.NotFoundException;
import fr.esgi.devwars.game.domain.dao.GameDAO;
import fr.esgi.devwars.game.domain.model.Game;
import fr.esgi.devwars.resolution.domain.dao.ResolutionDAO;
import fr.esgi.devwars.resolution.domain.model.Resolution;
import fr.esgi.devwars.user.domain.dao.UserDAO;
import fr.esgi.devwars.user.domain.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FindAllResolutionsByGame {
    private final ResolutionDAO resolutionDAO;
    private final GameDAO gameDAO;

    public List<Resolution> execute(Long gameId) {
        checkIfGameExists(gameId);
        return resolutionDAO.findResolutionsByGame(gameId);
    }


    private void checkIfGameExists(Long id) {
        Game exist = gameDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("Game with id %s not found", id));
        }
    }
}
