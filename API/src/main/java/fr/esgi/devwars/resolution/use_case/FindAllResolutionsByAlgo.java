package fr.esgi.devwars.resolution.use_case;

import fr.esgi.devwars.resolution.domain.dao.ResolutionDAO;
import fr.esgi.devwars.resolution.domain.model.Resolution;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import fr.esgi.devwars.algorithm.domain.dao.AlgorithmDAO;
import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FindAllResolutionsByAlgo {

    private final ResolutionDAO resolutionDAO;
    private final AlgorithmDAO algorithmDAO;

    public List<Resolution> execute(Long algorithmId) {
        checkIfAlgorithmExists(algorithmId);
        return resolutionDAO.findResolutionsByAlgo(algorithmId);
    }

    private void checkIfAlgorithmExists(Long id) {
        Algorithm exist = algorithmDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("Algorithm with id %s not found", id));
        }
    }

}
