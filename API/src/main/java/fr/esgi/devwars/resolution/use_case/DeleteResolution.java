package fr.esgi.devwars.resolution.use_case;

import fr.esgi.devwars.algorithm.domain.dao.AlgorithmDAO;
import fr.esgi.devwars.game.domain.dao.GameDAO;
import fr.esgi.devwars.game.domain.model.Game;
import fr.esgi.devwars.player.domain.exception.GameOverException;
import fr.esgi.devwars.resolution.domain.dao.ResolutionDAO;
import fr.esgi.devwars.user.domain.dao.UserDAO;
import fr.esgi.devwars.user.domain.model.User;
import fr.esgi.devwars.resolution.domain.model.Resolution;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DeleteResolution {

    private final ResolutionDAO resolutionDAO;
    private final UserDAO userDAO;
    private final AlgorithmDAO algorithmDAO;
    private final GameDAO gameDAO;

    public void execute(Long userId, Long algoId) {
        checkIfUserExists(userId);
        checkIfAlgoExists(algoId);
        var resolution = checkIfResolutionExists(userId, algoId);
        checkIfGameIsOpen(resolution.getGame().getId());
        resolutionDAO.delete(userId, algoId);
    }

    private void checkIfGameIsOpen(Long id) {
        Game game = gameDAO.findById(id);
        if (game == null) {
            throw new NotFoundException(String.format("Game with id %s not found", id));
        }
        if (game.isOver()) {
            throw new GameOverException(String.format("Game with id %s is closed", id));
        }
    }

    private void checkIfUserExists(Long id) {
        User exist = userDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("User with id %s not found", id));
        }
    }

    private void checkIfAlgoExists(Long id) {
        var exist = algorithmDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("Algorithm with id %s not found", id));
        }
    }

    private Resolution checkIfResolutionExists(Long userId, Long actionId) {
        Resolution exist = resolutionDAO.findByIds(userId, actionId);
        if (exist == null) {
            throw new NotFoundException(String.format("No relation between user %s and action %s", userId, actionId));
        }
        return exist;
    }
}
