package fr.esgi.devwars.resolution.infrastructure.web.response;

import fr.esgi.devwars.compiler.domain.model.LinterError;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

@Accessors(chain = true)
@Data
@RequiredArgsConstructor
public class ResolutionsByUserResponse implements Serializable {
    private Long algoId;
    private Long gameId;
    private Long itemId;
    private Long actionId;
    private Long resolutionTime;
    private Boolean solved;
    private List<LinterError> linterErrors;
}
