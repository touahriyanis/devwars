package fr.esgi.devwars.resolution.use_case;

import fr.esgi.devwars.action.domain.dao.ActionDAO;
import fr.esgi.devwars.action.domain.exception.CannotUseActionException;
import fr.esgi.devwars.action.domain.model.Action;
import fr.esgi.devwars.algorithm.domain.dao.AlgorithmDAO;
import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.common.domain.exception.AlreadyExistsException;
import fr.esgi.devwars.common.domain.exception.ForbiddenException;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import fr.esgi.devwars.game.domain.dao.GameDAO;
import fr.esgi.devwars.game.domain.exception.IncorrectPlayerNumberException;
import fr.esgi.devwars.game.domain.model.Game;
import fr.esgi.devwars.item.domain.dao.ItemDAO;
import fr.esgi.devwars.item.domain.model.Item;
import fr.esgi.devwars.player.domain.dao.PlayerDAO;
import fr.esgi.devwars.player.domain.exception.GameOverException;
import fr.esgi.devwars.player.domain.model.Player;
import fr.esgi.devwars.resolution.domain.dao.ResolutionDAO;
import fr.esgi.devwars.resolution.domain.exception.ComplexitiesDoNotMatchException;
import fr.esgi.devwars.resolution.domain.model.Resolution;
import fr.esgi.devwars.user.domain.dao.UserDAO;
import fr.esgi.devwars.user.domain.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class StartResolutionWithAction {

    private final ResolutionDAO resolutionDAO;
    private final UserDAO userDAO;
    private final AlgorithmDAO algorithmDAO;
    private final GameDAO gameDAO;
    private final PlayerDAO playerDAO;
    private final ActionDAO actionDAO;
    private final ItemDAO itemDAO;

    public Resolution execute(Long userId,
                              Long algorithmId,
                              Long actionId,
                              Long itemId,
                              Long gameId) {


        final var user = getUser(userId);
        final var algorithm = getAlgorithm(algorithmId);
        final var game = getGame(gameId);

        checkResolutionAlreadyExists(userId, algorithmId);
        assertGameIsNotOver(game);
        assertPlayersAreTwo(game);

        final var action = findAction(actionId);
        final var item = findItem(itemId);

        assertAlgoComplexityMatchActionPower(algorithm, action);
        assertPerformerCanPerformTheAction(user, action);

        final var newResolution =
                buildResolution(user, algorithm, game, action, item);
        return resolutionDAO.startResolution(newResolution);
    }

    private void assertAlgoComplexityMatchActionPower(Algorithm algorithm, Action action) {
        if (!algorithm.getComplexity().equals(action.getPower())) {
            throw new ComplexitiesDoNotMatchException(
                    String.format("Action power %s does not match algo complexity %s",
                            action.getPower(),
                            algorithm.getComplexity())
            );
        }
    }

    private void assertPerformerCanPerformTheAction(User performer, Action action) {
        final var userActions = userDAO.findActionsByUser(performer);

        if (userActions.stream().noneMatch(userAction -> userAction.equals(action))) {
            throw new CannotUseActionException(String.format("User %s cannot use action %s",
                    performer.getId(), action.getId()));
        }
    }

    private Resolution buildResolution(User user, Algorithm algorithm,
                                       Game game, Action action,
                                       Item item) {
        return new Resolution()
                .setUser(user)
                .setAlgorithm(algorithm)
                .setGame(game)
                .setAction(action)
                .setItem(item)
                .setStartedTime(LocalDateTime.now());
    }

    private Game getGame(Long id) {
        Game game = gameDAO.findById(id);
        if (game == null) {
            throw new NotFoundException(String.format("Game with id %s not found", id));
        }
        return game;
    }

    private void assertGameIsNotOver(Game game) {
        if (game.isOver()) {
            throw new GameOverException(String.format("Game with id %s is closed", game.getId()));
        }
    }

    private User getUser(Long id) {
        User exist = userDAO.findById(id);
        if (exist == null) {
            throw new ForbiddenException(String.format("User %s not allowed", id));
        }
        return exist;
    }

    private Algorithm getAlgorithm(Long id) {
        Algorithm exist = algorithmDAO.findById(id);
        if (exist == null) {
            throw new NotFoundException(String.format("Algorithm with id %s not found", id));
        }
        return exist;
    }

    private void checkResolutionAlreadyExists(Long userId, Long algorithmId) {
        final var exists = resolutionDAO.findByIds(userId, algorithmId);
        if (exists != null) {
            throw new AlreadyExistsException(String.format("User %s already resolved algorithm %s", userId, algorithmId));
        }
    }

    private void assertPlayersAreTwo(Game game) {
        if (playerDAO.findAllByGame(game.getId()).size() != 2) {
            throw new IncorrectPlayerNumberException("There should be two different players in the game");
        }
    }

    private Action findAction(Long actionId) {
        final var exists = actionDAO.findById(actionId);
        if (exists == null) {
            throw new NotFoundException(String.format("Action with id %s not found", actionId));
        }
        return exists;
    }

    private Item findItem(Long itemId) {
        final var exists = itemDAO.findById(itemId);
        if (exists == null) {
            throw new NotFoundException(String.format("Item with id %s not found", itemId));
        }
        return exists;
    }
}
