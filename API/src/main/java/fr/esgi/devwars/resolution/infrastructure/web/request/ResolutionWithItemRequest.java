package fr.esgi.devwars.resolution.infrastructure.web.request;

import fr.esgi.devwars.compiler.domain.model.LinterError;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.util.List;

@Data
@Accessors(chain = true)
public class ResolutionWithItemRequest {
    @NotNull
    @Positive
    private Long algorithmId;

    @NotNull
    @Positive
    private Long itemId;

    @NotNull
    @Positive
    private Long gameId;

    @NotNull
    @DecimalMin("0.0")
    private Double resolutionTime;

    @NotNull
    private Boolean solved;

    @NotNull
    @PositiveOrZero
    private List<LinterError> linterErrors;
}
