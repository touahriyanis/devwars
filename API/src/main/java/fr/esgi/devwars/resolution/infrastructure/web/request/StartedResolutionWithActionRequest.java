package fr.esgi.devwars.resolution.infrastructure.web.request;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@Accessors(chain = true)
public class StartedResolutionWithActionRequest {
    @NotNull
    @Positive
    private Long gameId;

    @NotNull
    @Positive
    private Long algorithmId;

    @NotNull
    @Positive
    private Long itemId;

    @NotNull
    @Positive
    private Long actionId;
}
