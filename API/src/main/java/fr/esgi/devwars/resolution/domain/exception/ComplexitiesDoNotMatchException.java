package fr.esgi.devwars.resolution.domain.exception;

public class ComplexitiesDoNotMatchException extends RuntimeException {
    public ComplexitiesDoNotMatchException(String message) {
        super(message);
    }
}
