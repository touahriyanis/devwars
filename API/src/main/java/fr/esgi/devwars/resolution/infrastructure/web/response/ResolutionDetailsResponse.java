package fr.esgi.devwars.resolution.infrastructure.web.response;

import fr.esgi.devwars.player.domain.model.Player;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

@Accessors(chain = true)
@Data
@RequiredArgsConstructor
public class ResolutionDetailsResponse implements Serializable {
    private String algorithmName;
    private String itemName;
    private String actionName;
    private List<Player> players;
    private Long errorNumber;
    private Long resolutionTime;
    private Boolean solved;
}
