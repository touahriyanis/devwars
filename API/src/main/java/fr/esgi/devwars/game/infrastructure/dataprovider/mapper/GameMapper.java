package fr.esgi.devwars.game.infrastructure.dataprovider.mapper;

import fr.esgi.devwars.game.domain.model.Game;
import fr.esgi.devwars.game.infrastructure.dataprovider.entity.GameEntity;
import org.springframework.stereotype.Component;

@Component
public class GameMapper {
    public Game toDomain(GameEntity entity) {
        return new Game()
                .setId(entity.getId())
                .setDate(entity.getDate())
                .setOver(entity.getOver());
    }

    public GameEntity toEntity(Game game) {
        return new GameEntity()
                .setDate(game.getDate())
                .setOver(game.getOver());
    }
}
