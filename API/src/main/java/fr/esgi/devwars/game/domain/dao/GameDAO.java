package fr.esgi.devwars.game.domain.dao;

import fr.esgi.devwars.game.domain.model.Game;

import java.util.List;

public interface GameDAO {
    Game create(Game game);

    Game findById(Long id);

    List<Game> findAll();

    List<Game> findAllUnfinishedGames();

    Game update(Game game);

    void delete(Long id);
}
