package fr.esgi.devwars.game.use_case;

import fr.esgi.devwars.game.domain.dao.GameDAO;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DeleteGameById {

    private final GameDAO gameDAO;

    public void execute(Long id) {
        var game = gameDAO.findById(id);
        if (game == null) {
            throw new NotFoundException(String.format("Game with id %s not found", id));
        }
        gameDAO.delete(id);
    }
}
