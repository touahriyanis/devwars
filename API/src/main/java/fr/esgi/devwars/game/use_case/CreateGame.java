package fr.esgi.devwars.game.use_case;

import fr.esgi.devwars.game.domain.dao.GameDAO;
import fr.esgi.devwars.game.domain.model.Game;
import fr.esgi.devwars.common.domain.exception.AlreadyExistsException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class CreateGame {

    private final GameDAO gameDAO;

    public Game execute(LocalDateTime date, Boolean over) {
        Game game = new Game()
                .setDate(date)
                .setOver(over);

        return gameDAO.create(game);
    }
}
