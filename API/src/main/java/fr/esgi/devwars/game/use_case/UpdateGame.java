package fr.esgi.devwars.game.use_case;

import fr.esgi.devwars.game.domain.dao.GameDAO;
import fr.esgi.devwars.game.domain.model.Game;
import fr.esgi.devwars.common.domain.exception.AlreadyExistsException;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UpdateGame {

    private final GameDAO gameDAO;

    public Game execute(Long id, Boolean over) {
        checkIfExists(id);

        Game game = new Game()
                .setId(id)
                .setOver(over);

        return gameDAO.update(game);
    }

    private void checkIfExists(Long id) {
        var game = gameDAO.findById(id);
        if (game == null) {
            throw new NotFoundException(String.format("Game with id %s not found", id));
        }
    }

}
