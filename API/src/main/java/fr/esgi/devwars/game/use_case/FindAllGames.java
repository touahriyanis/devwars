package fr.esgi.devwars.game.use_case;

import fr.esgi.devwars.game.domain.dao.GameDAO;
import fr.esgi.devwars.game.domain.model.Game;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FindAllGames {

    private final GameDAO gameDAO;

    public List<Game> execute() {
        return gameDAO.findAll();
    }
}
