package fr.esgi.devwars.game.infrastructure.dataprovider.repository;

import fr.esgi.devwars.game.infrastructure.dataprovider.entity.GameEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GameRepository extends JpaRepository<GameEntity, Long> {
    List<GameEntity> findByOver(Boolean over);
}
