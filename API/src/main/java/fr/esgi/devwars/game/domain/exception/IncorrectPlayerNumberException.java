package fr.esgi.devwars.game.domain.exception;

public class IncorrectPlayerNumberException extends RuntimeException {
    public IncorrectPlayerNumberException(String message) {
        super(message);
    }
}
