package fr.esgi.devwars.algorithm.use_case;

import fr.esgi.devwars.algorithm.domain.dao.AlgorithmDAO;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DeleteAlgorithmById {

    private final AlgorithmDAO algorithmDAO;

    public void execute(Long id) {
        var algorithm = algorithmDAO.findById(id);
        if (algorithm == null) {
            throw new NotFoundException(String.format("Algorithm with id %s not found", id));
        }
        algorithmDAO.delete(id);
    }
}
