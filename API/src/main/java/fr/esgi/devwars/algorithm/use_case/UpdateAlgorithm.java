package fr.esgi.devwars.algorithm.use_case;

import fr.esgi.devwars.algorithm.domain.dao.AlgorithmDAO;
import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.algorithm.infrastructure.web.request.AlgorithmRequest;
import fr.esgi.devwars.common.domain.exception.AlreadyExistsException;
import fr.esgi.devwars.common.domain.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UpdateAlgorithm {

    private final AlgorithmDAO algorithmDAO;

    public Algorithm execute(Long id, AlgorithmRequest request) {
        checkIfExists(id);
        checkIfWordingAlreadyExists(request.getWording(), id);

        Algorithm algorithm = new Algorithm()
                .setId(id)
                .setWording(request.getWording())
                .setFuncName(request.getFuncName())
                .setDescription(request.getDescription())
                .setShortDescription(request.getShortDescription())
                .setCppInitialCode(request.getCppInitialCode())
                .setJavaInitialCode(request.getJavaInitialCode())
                .setPythonInitialCode(request.getPythonInitialCode())
                .setTimeToSolve(request.getTimeToSolve())
                .setTimeLimit(request.getTimeLimit())
                .setComplexity(request.getComplexity())
                .setMemoryLimit(request.getMemoryLimit());

        return algorithmDAO.update(algorithm);
    }

    private void checkIfExists(Long id) {
        var algorithm = algorithmDAO.findById(id);
        if (algorithm == null) {
            throw new NotFoundException(String.format("Algorithm with id %s not found", id));
        }
    }

    private void checkIfWordingAlreadyExists(String wording, Long id) {
        Algorithm exist = algorithmDAO.findByWording(wording);
        if (exist != null && !exist.getId().equals(id)) {
            throw new AlreadyExistsException(String.format("Algorithm with wording %s already exists", wording));
        }
    }
}
