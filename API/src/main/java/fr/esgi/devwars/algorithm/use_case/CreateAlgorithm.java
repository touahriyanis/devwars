package fr.esgi.devwars.algorithm.use_case;

import fr.esgi.devwars.algorithm.domain.dao.AlgorithmDAO;
import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import fr.esgi.devwars.algorithm.infrastructure.web.request.AlgorithmRequest;
import fr.esgi.devwars.common.domain.exception.AlreadyExistsException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CreateAlgorithm {

    private final AlgorithmDAO algorithmDAO;

    public Algorithm execute(AlgorithmRequest request) {
        checkIfWordingAlreadyExists(request.getWording());

        Algorithm algorithm = new Algorithm()
                .setWording(request.getWording())
                .setFuncName(request.getFuncName())
                .setPythonInitialCode(request.getPythonInitialCode())
                .setJavaInitialCode(request.getJavaInitialCode())
                .setCppInitialCode(request.getCppInitialCode())
                .setDescription(request.getDescription())
                .setShortDescription(request.getShortDescription())
                .setTimeToSolve(request.getTimeToSolve())
                .setTimeLimit(request.getTimeLimit())
                .setComplexity(request.getComplexity())
                .setMemoryLimit(request.getMemoryLimit());

        return algorithmDAO.create(algorithm);
    }

    private void checkIfWordingAlreadyExists(String wording) {
        Algorithm exist = algorithmDAO.findByWording(wording);
        if (exist != null) {
            throw new AlreadyExistsException(String.format("Algorithm with wording %s already exists", wording));
        }
    }
}
