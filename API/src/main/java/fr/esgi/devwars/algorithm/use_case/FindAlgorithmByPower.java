package fr.esgi.devwars.algorithm.use_case;

import fr.esgi.devwars.algorithm.domain.dao.AlgorithmDAO;
import fr.esgi.devwars.algorithm.domain.model.Algorithm;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FindAlgorithmByPower {
    private final AlgorithmDAO algorithmDAO;

    public List<Algorithm> execute(Integer power, Long userId) {
        return algorithmDAO.findByPower(power, userId);
    }
}
