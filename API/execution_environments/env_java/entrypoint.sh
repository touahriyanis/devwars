#!/usr/bin/env bash
mv main$1.java Main$1.java
javac Main$1.java
ret=$?
if [ $ret -ne 0 ]
then
  exit 2
fi
ulimit -s 400
timeout --signal=SIGTERM 180 java Main$1
exit $?
