#!/usr/bin/env bash
g++ main$1.cpp -o exec
ret=$?
if [ $ret -ne 0 ]
then
  exit 2
fi
ulimit -s 400
timeout --signal=SIGTERM 500 ./exec 
exit $?
