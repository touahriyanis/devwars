#!/usr/bin/env bash
ulimit -s 400
timeout --signal=SIGTERM 500 python3 main$1.py
exit $?
